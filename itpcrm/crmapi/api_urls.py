from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from crmapi import api_views
from crmapi import provisioner_views as prov_views

urlpatterns = [
    url(r'^orders/order-items/bw/order-id/(?P<oid>[\w\-]+)', api_views.API_BandwidthOrderSearch.as_view()),
    url(r'^orders/order-items/did/order-id/(?P<did>[\w\-]+)', api_views.API_BandwidthCallbackDIDLookup.as_view()),
    url(r'^order-items', api_views.API_OrderItems.as_view()),
    #url(r'^orders/order-items', api_views.API_OrderItems.as_view()),
    url(r'^orders/account-services/(?P<pk>[\w\-]+)', prov_views.API_AccountServicesEnabled.as_view()),
    url(r'^orders/account-services', prov_views.API_AccountServices.as_view()),
    url(r'^orders', api_views.API_Orders.as_view()),

    #url(r'^services/(?P<api_id>[\w\-]+)/itpvoice/did', api_views.API_OrderDID.as_view()),
    # IC-266 - ^^ replaced with this one below....
    url(r'^service-addons', api_views.API_ServiceAddons.as_view()),

    # url(r'^services/api-id/(?P<api_id>[\w\-]+)', api_views.API_ServiceByApiId.as_view()),
    # url(r'^callbacks/bandwidth', views.API_Services.as_view()),
    url(r'^services', api_views.API_Services.as_view()),
    url(r'^callbacks/docusign', api_views.API_DocusignCallbacks.as_view()),

]


