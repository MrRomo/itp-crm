from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from crmapi import views
#from crmapi import portal_views

urlpatterns = [
    # ACCOUNTS URLS
    url(r'^accounts', views.API_Accounts.as_view()),

    url(r'^orders/(?P<pk>[0-9]+)', views.API_GetOrdersByID.as_view()),
    url(r'^orders', views.API_Orders.as_view()),
    
    
    url(r'^quotes/(?P<pk>[0-9]+)', views.API_GetQuoteByID.as_view()),
    url(r'^quotes', views.API_Quote.as_view()),

    # DEVICES URL
    url(r'^services/itpvoice/devices', views.API_Devices.as_view()),

    # SERVICES URL
    url(r'^services', views.API_Services.as_view()),

    #  other utils
    url(r'^misc/locations/states', views.API_CitySearch.as_view()),
    url(r'^misc/locations/cities', views.API_CitySearch.as_view())
]


