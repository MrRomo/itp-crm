# stuffs with DIDs
from crmapi.crm_utils import itpvoice_pbx_did


# item_type name / action method
item_type_mapper = {
    'itpvoice_pbx_did': 
        {
            "service-addon": itpvoice_pbx_did.order_did,
            "disconnect": itpvoice_pbx_did.disconnect_did,
            # "upgrade": itpvoice_pbx_did.some_func
            # "downgrade": itpvoice_pbx_did.some_func
            # "new-order": itpvoice_pbx_did.some_func
        }
}


class ItemTypeLoader(object):
    """
    ServiceAddons

    Determine product / producttype
    call respective action function

    Auto create / approve quote process and
    create quick orders
    """

    extra_data = {
        'api_id': None,
        'parent_account_service': None,

    }
    product = None

    def __init__(self, *args, **kwargs):
        # populate extra data fields except product
        req_fields = ['item_type', 'product']

        for k, v in kwargs.items():
            if not k in req_fields:
                self.extra_data[k] = v

        self.product = kwargs.get('product')


    def create_order(self, purchase_type=None):
        # disconnect, new, upgrade, downgrade
        product = self.product
        item_type = product.product_key.name
        item_default_metadata = product.product_key.default_metadata
        # determine by item_type
        return item_type_mapper.get(item_type).get(purchase_type)(
            product, extra_data=self.extra_data)


