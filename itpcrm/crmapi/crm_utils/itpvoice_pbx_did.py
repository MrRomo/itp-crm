# custom functions for item_type = 'itpvoice_pbx_did'
from crm.models import (
    QuoteItems,
    QUOTE_STATUSES,
    Quotes,
    Orders
)


def disconnect_did(p, extra_data=None, request_data=None):
    data = extra_data.get('request_data')
    api_id = extra_data.get('api_id')
    account_service = extra_data.get('parent_account_service')

    if not account_service:
        raise Exception("No parent_account_service - cannot continue")
    account = account_service.account

    if not api_id:
        # well check inside request dat
        api_id = data.get('api_id')
    if not api_id:
        raise Exception("No api_id in payload")

    q = Quotes()
    q.quote_status = QUOTE_STATUSES.get('DRAFT')
    q.account = account
    q.save()

    product = p
    for qitem in data.get('numbers'):
        qi_metadata = product.metadata.copy()
        for key_check in ['state', 'city', 'did']:
            if not qi_metadata.get(key_check):
                qi_metadata[key_check] = None

        qi_metadata['did'] = qitem.get('did')
        qi_metadata['api_id'] = api_id

        qi = QuoteItems(
            quote=q,
            account=q.account,
            product=product,
            metadata=qi_metadata,
            price_per_unit=product.price,
            price=product.price,
            auto_provision=product.auto_provision,
            purchase_type='disconnect',
            upgrade_service=account_service,
            quantity=1, # numbers always have quantity=1 for orders.
            item_price_total=(product.price * 1))
        qi.save()

    # set the quote to customer approved
    q.quote_status = QUOTE_STATUSES.get('CUSTOMER_APPROVED')
    q.save()
    try:
        o = Orders.objects.get(quote=q)
        #for oi in OrderItems.objects.filter(order=o):
        #    oi.auto_provision = True
        #    oi.provision_status = 'pending-provision'
        #    oi.save()
        return {"result": "ok",
                         "order_number": o.order_number,
                         "api_id": api_id,
                         "errors": False,
                         "code": 200}
    except Orders.DoesNotExist:
        o = None
        return {"result": "nope..Order was not created",
                         "api_id": api_id,
                         "order_number": None,
                         "errors": True, "code": 400}






def order_did(p, extra_data=None, request_data=None):
    # data should be the json REQUEST data
    data = extra_data.get('request_data')
    api_id = extra_data.get('api_id')
    account_service = extra_data.get('parent_account_service')

    if not account_service:
        raise Exception("No parent_account_service - cannot continue")
    account = account_service.account

    if not api_id:
        # well check inside request dat
        api_id = data.get('api_id')
    if not api_id:
        raise Exception("No api_id in payload")

    product = p

    q = Quotes()
    q.quote_status = QUOTE_STATUSES.get('DRAFT')
    q.account = account
    q.save()

    for qitem in data.get('numbers'):
        qi_metadata = product.metadata.copy()
        for key_check in ['state', 'city', 'did']:
            if not qi_metadata.get(key_check):
                qi_metadata[key_check] = None


        qi_metadata['did'] = qitem.get('did')
        qi_metadata['api_id'] = api_id

        qi = QuoteItems(
            quote=q,
            account=q.account,
            product=product,
            metadata=qi_metadata,
            price_per_unit=product.price,
            price=product.price,
            auto_provision=product.auto_provision,
            purchase_type='service-addon',
            upgrade_service=account_service,
            quantity=1, # numbers always have quantity=1 for orders.
            item_price_total=(product.price * 1))
        qi.save()

    # set the quote to customer approved
    q.quote_status = QUOTE_STATUSES.get('CUSTOMER_APPROVED')
    q.save()
    try:
        o = Orders.objects.get(quote=q)
        #for oi in OrderItems.objects.filter(order=o):
        #    oi.auto_provision = True
        #    oi.provision_status = 'pending-provision'
        #    oi.save()
        return {"result": "ok",
                         "order_number": o.order_number,
                         "api_id": api_id,
                         "errors": False,
                         "code": 200}
    except Orders.DoesNotExist:
        o = None
        return {"result": "nope..Order was not created",
                         "api_id": api_id,
                         "order_number": None,
                         "errors": True, "code": 400}

