# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import requests
from django.shortcuts import render
from crm.models import (
    Accounts,
    Contacts,
    Orders,
    OrderItems,
    AccountServices,
    US_Cities,
    US_States)
from crmapi.serializers import (
    AccountsSerializer,
    AccountServicesSerializer,
    US_StatesSerializer,
    US_CitiesSerializer,
    OrdersSerializer,
    OrderItemsSerializer,
)
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.db import connection
from django.conf import settings

from crmapi.auth import do_auth


##############

# PORTAL URLS 

##############

def get_account_id(uid):
    try:
        return Contacts.objects.get(auth_api_id=uid)
    except Contacts.DoesNotExist:
        return None

class API_Quote(APIView):
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        acc = get_account_id(uid)
        #accounts = Accounts.objects.filter(pk=acc.pk)
        account = acc.account
        account_pk = account.pk
        results = []
        quotes = Quotes.objects.filter(account=account)
        for i in quotes:
            o = QuotesSerializer(i, many=False)
            results.append(o.data)
        return Response({"result": results, "errors": False, "message": "ok"}, 200)

class API_GetQuoteByID(APIView):
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        acc = get_account_id(uid)
        #accounts = Accounts.objects.filter(pk=acc.pk)
        account = acc.account
        account_pk = account.pk
        try:
            quotes = Quotes.objects.get(pk=pk, account=account)
        except Quotes.DoesNotExist:
            quotes = None

        if quotes:
            ser = QuotesSerializer(quotes, many=False)
            quote_data = ser.data
            quote_data['quote_items'] = []
            for qi in quotes.get_parent_quote_items():
                qiser = QuoteItemsSerializer(qi, many=False)
                qiser_data = qiser.data
                #qiser_data['related_addons'] = []
                quote_data['quote_items'].append(qiser.data)

            for qi in quote_data['quote_items']:
                qis = QuoteItems.objects.filter(parent_quote_item=QuoteItems.objects.get(pk=qi.get('pk')))
                for quote_orderitems_related in qis:
                    quote_orderitems_related_ser = QuoteItemsSerializer(quote_orderitems_related, many=False)
                    if not qi.get('related_addons'):
                        qi['related_addons'] = []
                    qi['related_addons'].append(quote_orderitems_related_ser.data)
            d = dict(result=quote_data, message="ok", errors=False)
            return Response(d)
        else:
            return Response(dict(result="errors", errors=True,
                                message="Agent unauthorized quote access"), 401)


class API_Orders(APIView):
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        acc = get_account_id(uid)
        if not acc:
            return Response({'results': "Unauthorized"},
                            status=401)
        #accounts = Accounts.objects.filter(pk=acc.pk)
        account = acc.account
        account_pk = account.pk
        results = []
        orders = Orders.objects.filter(account=account)
        for i in orders:
            o = OrdersSerializer(i, many=False)
            results.append(o.data)
        return Response({"result": results, "errors": False, "message": "ok"}, 200)
        
class API_GetOrdersByID(APIView):
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        acc = get_account_id(uid)
        #accounts = Accounts.objects.filter(pk=acc.pk)
        account = acc.account
        account_pk = account.pk
        try:
            # this does the filtering to make sure its part of the client poirtal users ACCOUNT....IC-216
            orders = Orders.objects.get(account=account, pk=pk)
        except Orders.DoesNotExist:
            return Response({"result": "error", "errors": True, "message": "Order does not exist for this agent"}, status=400)

        d = {'order': None, 'orderitems': []}
        ser = OrdersSerializer(orders, many=False)
        d['order'] = ser.data
        #oi = OrderItems.objects.filter(order=orders)
        #for i in oi:
        #    ois = OrderItemsSerializer(i, many=False)
        #    d['orderitems'].append(ois.data)
        if orders:
            ser = OrdersSerializer(orders, many=False)
            order_data = ser.data
            order_data['order_items'] = []
            for qi in orders.get_parent_quote_items():
                qiser = OrderItemsSerializer(qi, many=False)
                qiser_data = qiser.data
                #qiser_data['related_addons'] = []
                order_data['order_items'].append(qiser.data)

            for qi in order_data['order_items']:
                qis = OrderItems.objects.filter(parent_order_item=OrderItems.objects.get(pk=qi.get('pk')))
                for order_orderitems_related in qis:
                    order_orderitems_related_ser = OrderItemsSerializer(order_orderitems_related, many=False)
                    if not qi.get('related_addons'):
                        qi['related_addons'] = []
                    qi['related_addons'].append(order_orderitems_related_ser.data)
            d = dict(result=order_data, message="ok", errors=False)
            return Response(d)
        return Response({"result": d, "message": "ok", "errors": False})


class API_Accounts(APIView):
    def get(self, request, format=None):
        # raise Exception(request.META)
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        acc = get_account_id(uid)
        #accounts = Accounts.objects.filter(pk=acc.pk)
        ser = AccountsSerializer([acc.account], many=True)
        return Response(ser.data)

    def post(self, request, format=None):
        serializer = AccountsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class API_Services(APIView):
    # this is used by portal
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        acc = get_account_id(uid)
        if not acc:
            return Response({'results': "Unauthorized"}, status=401)

        acc_id = acc.account.pk
        auth_api_id = acc.auth_api_id
        #for l in list_of_services:
        #    results[l] = []
        results = []
        for a in AccountServices.objects.filter(account=acc.account):
            aser = AccountServicesSerializer(a, many=False)
            results.append(aser.data)

        """
        for o in Orders.objects.filter(account=acc.account):
            for oi in o.get_items:
                p = oi.product
                if p.product_type == 'Service':
                    print p.part_number
                    #pmeta = json.loads(oi.metadata)
                    pmeta = oi.metadata
                    if not p.part_number in results.keys():
                        results[p.part_number] = []
                        if not pmeta in results[p.part_number]:
                            results[p.part_number].append(pmeta)
                    else:
                        if not pmeta in results[p.part_number]:
                            results[p.part_number].append(pmeta)"""
        return Response({'result': results, 'user_api_id': auth_api_id}, status=200)


class API_Devices(APIView):
    post_required_fields = [
        'name', 'extensionnumber', 'device_type'
    ]
    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        # this should be a common doauth func extension 
        # to not poison do_auth with db calls
        j = do_auth(token, get_full=True)
        uid = j.get('id')
        accnum = j.get('accountnumber')
        if not uid:
            return Response({'results': "Unauthorized uid"},
                            status=401)
        if not accnum:
            return Response({'results': "Unauthorized acc#"},
                            status=401)
        try:
            ac = Accounts.objects.get(accountnumber=accnum)
        except Accounts.DoesNotExist:
            return Response({'results': "Unauthorized - invalid account"},
                            status=401)
        try:
            c = Contacts.objects.get(auth_api_id=uid)
        except Contacts.DoesNotExist:
            return Response({'results': "Unauthorized - invalid contact"},
                            status=401)
        if not c.account == ac:
            return Response({'results': "Unauthorized - invalid contact/account"},
                            status=401)
        # end todo refactor here

        # check all required fields are present
        for k in self.post_required_fields:
            if not request.POST.get(k):
                return Response({'results': 'error', 'message': "Require field missing %s" % k},
                                status=400)
        d = {"name": request.POST.get('name'),
            "extensionnumber": request.POST.get('extensionnumber'),
            "device_type": request.POST.get('device_type'),
            'account': request.POST.get('account')}
        url = '%s/v1/devices?Authorization=%s' % \
            (self.VOICE_API_URL, self.VOICE_API_KEY)
        r = requests.post(url, json=j)
        if r.status_code == 200:
            return Response({"result": "ok",
                             "errors": None,
                             "message": "Device added successfully"}, 
                            status=200)
        else:
            return Response({"result": "error", "errors": True, "message": str(r.text)}, 
                            status=r.status_code)

    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        j = do_auth(token, get_full=True)
        uid = j.get('id')
        accnum = j.get('accountnumber')
        if not uid:
            return Response({'results': "Unauthorized uid"},
                            status=401)
        if not accnum:
            return Response({'results': "Unauthorized acc#"},
                            status=401)
        acc = get_account_id(uid)
        acc_id = acc.account.pk
        auth_api_id = acc.auth_api_id
        url = '%s/api/devices?Authorization=%s&account_id=%s' %\
            (settings.VOICE_API_URL,
             settings.VOICE_API_KEY, auth_api_id)
        jj = json.loads(requests.get(url).text)
        return Response(jj, status=200)


class API_CitySearch(APIView):
    def get(self, request, format=None):
        state = request.GET.get('state', None)
        print ("State = %s" % state)
        if state:
            if len(state) == 2:
                filters = {"shortcode": state.lower()}
            else:
                filters = {"name": state.lower()}
            state = str(state).lower()
            s = US_States.objects.filter(**filters)
            city = US_Cities.objects.filter(us_state=s[0]).order_by('name')
            aser = US_CitiesSerializer(city, many=True)
            resp = {"results": aser.data, "errors": False, "message": "ok"}
        else:
            state = US_States.objects.all().order_by('name')
            aser = US_StatesSerializer(state, many=True)
            city = None
            resp = {"results": aser.data, "errors": False, "message": "ok"}
        return Response(resp, status=200)

