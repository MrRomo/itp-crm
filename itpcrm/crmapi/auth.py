import jwt
from django.conf import settings
JWT_KEY = settings.JWT_SECRET_KEY


def do_auth(token, get_full=False, testing=False):
    validated = False
    if not token:
        return None
    token = str(token).replace('Bearer ', '').replace('bearer ', '')
    #print "TOKEN %s" % token
    print (token)
    try:
        j = jwt.decode(token, JWT_KEY, algorithms=['HS256'], verify=True)
        print (j)
        #print j
        validated = True
    except Exception as e:
        print ("Returning none cause %s" % str(e))
        return None
    if testing:
        if validated:
            j['validated'] = validated
        return j
    if j:
        user_id = j.get('id')
        if get_full:
            return j
        return user_id
    return None

