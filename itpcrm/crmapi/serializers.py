from rest_framework import serializers
from crm.models import (
    Accounts,
    Agents,
    AgentOwnedBy,
    Contacts,
    INDUSTRY_CHOICES,
    ACCT_STATUS_CHOICES,
    Quotes,
    QUOTE_STATUS,
    QuoteItems,
    QuoteItemGroup,
    ServiceAddress,
    Orders,
    OrderItems,
    OrderItemGroup,
    Products,
    AccountServices,
    ServiceAddons,
    US_States,
    US_Cities,
)
from django.contrib.auth.models import User

class AgentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agents
        fields = (
            'pk',
            'active',
            'companyname',
            'title',
            'firstname',
            'lastname',
            'email',
            'primary_street',
            'primary_city',
            'primary_state',
            'primary_zipcode',
            'primary_country',
            'phone',
            'mobile',
            #'auth_api_id',
            'commission_rate',
            'role',)


class UsersSerializer(serializers.ModelSerializer):
    model = User
    fields = (
        'username',
        'first_name',
        'last_name',
        'email',
        'is_staff',
        'is_active',
        'is_superuser'
    )


class AccountsSerializer(serializers.ModelSerializer):
    agent = AgentsSerializer(many=False, read_only=True)
    class Meta:
        model = Accounts
        fields = (
            'pk',
			'name',
			'phone',
			'website',
			'billing_address',
			'billing_city',
			'billing_state',
			'billing_zipcode',
			'billing_country',
			'industry',
			'accountnumber',
			'status',
			'net_terms',
			'imgurl',
			'billing_api_id',
            'account_type', 'agent',)


class AccountsSerializerLite(serializers.ModelSerializer):
    class Meta:
        model = Accounts
        fields = (
            'pk', 
            'name',
        )

class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacts
        fields = (
            'pk',
            'title',
            'firstname',
            'lastname',
            'email',
            'primary_street',
            'primary_city',
            'primary_state',
            'primary_zipcode',
            'primary_country',
            'phone',
            'mobile',
            #'auth_api_id',
            'billing_group',
            'technical_group',
            'admin_group',)


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = (
            'pk',
            'name',
            'category',
            'product_type',
            'currency',
            'price',
            'description',
            #'part_number',
            'item_type',
            'metadata',
            'auto_provision',
            'taxable',
            'tax_percent',
            'quantifiable',
        )


class ServiceAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceAddress
        fields = (
            'pk',
            'lprop_id',
            'addressid',
            'folio',
            'get_lat',
            'get_lon',
            'mailing_mu',
            'hse_num',
            'pre_dir',
            'st_name',
            'st_type',
            'suf_dir',
            'zip',
            'plus4',
            'munic',
            #'sname',
            'city',
            'state',
            'suite_num',
            'validated',
            'itpfiber_status',
            'full_address',
            'geo_data',
            'lat', 
            'lon',
        )


class AccountsContactsSerializer(serializers.ModelSerializer):
    get_contacts = ContactsSerializer(many=True, read_only=True)
    class Meta:
        model = Accounts
        fields = (
            'pk',
            'name',
            'get_contacts',)

class QuoteItemsSerializer(serializers.ModelSerializer):
    service_address = ServiceAddressSerializer(many=False, read_only=True)
    #related_addons = QuoteItemsSerializer(many=True, read_only=True)
    class Meta:
        model = QuoteItems
        fields = (
            'pk',
            'price',
            'price_changed',
            'auto_provision',
            'metadata',
            'product_name',
            'product_id',
            'service_address',
            'quantity',
            'req_activation_date',
            'prod_description',
            #'related_addons',
        )

class QuoteItemGroupSerializer(serializers.ModelSerializer):
    quoteitems_set = QuoteItemsSerializer(many=True, read_only=True)
    class Meta:
        model = QuoteItemGroup
        fields = (
            'pk',
            'name',
            'quote',
            'quoteitems_set',
        ) 

class QuotesSerializer(serializers.ModelSerializer):
    #account = AccountsSerializerLite(many=False, read_only=True)
    account = AccountsSerializer(many=False, read_only=True)
    #get_parent_quote_items = QuoteItemsSerializer(many=True, read_only=True)
    #quoteitemgroup_set = QuoteItemGroupSerializer(many=True, read_only=True)
    #created_user = UsersSerializer(many=False, read_only=True)
    #quote_items = QuoteItemsSerializer(many=True, read_only=True)
    class Meta:
        model = Quotes
        fields = ( 
            'pk',
            'name',
            'account',
            'date_created', 
            'quote_status',
            'customer_signed',
            'customer_signed_ip', 
            'accepted_date',
            'valid_until',
            #'packages',
            #'quoteitemgroup_set',
            #'get_parent_quote_items',
            'notes',
            'total_price',
            #'created_user',

        )

class OrderItemsSerializer(serializers.ModelSerializer):
    service_address = ServiceAddressSerializer(many=False, read_only=True)
    product = ProductsSerializer(many=False, read_only=True) 
    class Meta:
        model = OrderItems
        fields = (
            'pk',
            'product',
            'item_type',
            'price_per_unit',
            'price',
            'bill_interval',
            'next_bill',
            'metadata',
            'payment_complete',
            'auto_provision',
            'prov_status',
            'provision_status',
            'provision_result',
            'service_address',
            'purchase_type',
            'quantity',
            'req_activation_date',
            'activation_date',

        )

class OrderItemGroupSerializer(serializers.ModelSerializer):
    orderitems_set = OrderItemsSerializer(many=True, read_only=True)
    class Meta:
        model = OrderItemGroup
        fields = (
            'pk',
            'name',
            'order',
            'orderitems_set',
        )


class OrdersSerializer(serializers.ModelSerializer):
    account = AccountsSerializer(many=False, read_only=True)
    #orderitemgroup_set = OrderItemGroupSerializer(many=True, read_only=True)
    class Meta:
        model = Orders
        fields = (
            'pk',
            'status',
            'order_number',
            'account',
            'db_account_id',
            'db_quote_id',
            'date_created',
            'deleted',
            'db_agent_id',
            'notes',
            #'orderitemgroup_set',
        )







class AgentOwnedBySerializer(serializers.ModelSerializer):
    get_sub_agents = AgentsSerializer(many=True, read_only=True)
    class Meta:
        model = AgentOwnedBy
        fields = (
                'master_agent', 'sub_agent', 'get_sub_agents')





class ServiceAddonsSerializer(serializers.ModelSerializer):
    #order = OrdersSerializer(many=False, read_only=True)
    
    class Meta:
        model = ServiceAddons
        fields = (
            'pk',
            'service',
            'product',
            'product_name',
            'product_description',
            'price_per_unit',
            'price',
            'bill_interval',
            'next_bill',
            'metadata',
            'payment_complete',
            'auto_provision',
            'provision_status',
            'provision_result',
            'api_id',
            'req_activation_date',
            'activation_date',
            'disconnect_date',
            'item_type',

        )


class AccountServicesSerializer(serializers.ModelSerializer):
    #order = OrdersSerializer(many=False, read_only=True)
    get_service_addons = ServiceAddonsSerializer(many=True, read_only=True)
    service_address = ServiceAddressSerializer(many=False, read_only=True)
    class Meta:
        model = AccountServices
        fields = (
            'pk',
            'product',
            'product_name',
            'product_type',
            'product_description',
            'quantity',
            'price_per_unit',
            'price',
            'bill_interval',
            'next_bill',
            'metadata',
            'payment_complete',
            'auto_provision',
            'provision_status',
            'provision_result',
            'api_id',
            'req_activation_date',
            'activation_date',
            'get_service_addons',
            'service_address',
            'disconnect_date',
            'item_type',

        )


class US_StatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = US_States
        fields = (
            'formal_name', 'name', 'shortcode'
        )

class US_CitiesSerializer(serializers.ModelSerializer):
    #us_state = US_StatesSerializer(many=False, read_only=True)
    class Meta:
        model = US_Cities
        fields = (
            #'us_state',
            'name',
            'county',
            'city_alias',
        )

