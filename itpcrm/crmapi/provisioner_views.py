# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import requests
from datetime import datetime
from decimal import Decimal
from django.shortcuts import render
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.db import connection
from django.conf import settings

from crmapi.auth import do_auth
from crmapi.serializers import (
        AccountsSerializer,
        QuotesSerializer,
        ServiceAddressSerializer,
        OrdersSerializer,
        OrderItemsSerializer,
        AccountsContactsSerializer,
        AgentsSerializer,
        AgentOwnedBySerializer,
        QuoteItemGroupSerializer,
        QuoteItemsSerializer,
        ProductsSerializer,
        AccountServicesSerializer,
)
from crm.models import (
        Accounts,
        Contacts,
        Orders,
        OrderItems,
        Agents,
        AgentOwnedBy,
        Quotes,
        ServiceAddress,
        QuoteItemGroup,
        QuoteItems,
        Products,
        AccountServices,

        QUOTE_STATUSES,
)


PENDING_ORDER = 'quote'


def do_auth(api_key):
    # do we need a comment here? 
    if api_key == settings.PROVISIONER_API_KEY:
        return True
    return False


def get_pending_activation(request, format=None):
    token = request.META.get('HTTP_AUTHORIZATION')
    uid = do_auth(token)
    if not uid:
        return Response({'results': "Unauthorized"}, 401)
    
    accts = AccountServices.objects.filter(
            metadata__enabled=False,
            auto_provision=True,
            req_activation_date__lte=datetime.now())
    aser = AccountServicesSerializer(accts, many=True)

    return Response(aser.data, 200)




class API_GetOrders(APIView):
    """ This is not used yuet brooo"""
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        orders = Orders.objects.filter(status=PENDING_ORDER)
        orders_response = []

        for o in orders:
            oss = OrdersSerializer(o, many=False)
            for ig in o.orderitemgroup_set.select_related():
                # each oi
                order_items = []
                for oi in ig.items:
                    if oi.auto_provision and oi.provision_status == 'pending-provision':
                        # hell yeah
                        ois = OrderItemsSerializer(oi, many=False)
                        order_items.append(ois.data)
                        d = dict(order=oss.data, orderitems=order_items)
                        orders_response.append(d)

        return Response(orders_response, 200)



class API_UpdateOrderStatus(APIView):
    """
    To update the order status and provisioning results
    POST /api/provisioner/orders/[id]/order-items/[item-id]/update
    """
    def get(self, request, format=None):
        pass

    def post(self, request, order_id, item_id, format=None):
        # status and results and order id in payload
        data = json.loads(request.body)
        orderitem_id = item_id
        try:
            order = Orders.objects.get(pk=order_id)
        except Exception as e:
            pass
        try:
            orderitem = OrderItems.objects.get(order=order, pk=orderitem_id)
            orderitem.provision_status = data.get('provision_status')
            orderitem.provision_result = data.get('result')
            orderitem.save()
        except OrderItems.DoesNotExist:
            pass

        return Response({"result": "updated"}, 200)


class API_AccountServicesEnabled(APIView):
    def patch(self, request, pk, format=None):
        data = json.loads(request.body)
        acctserv = AccountServices.objects.get(pk=pk)
        acctserv.metadata['enabled'] = True
        acctserv.save()
        return Response({"result": "ok", "message": "Updated account service", "errors": False}, 200)


class API_AccountServices(APIView):
    """
    Create the account service object when order item is provisoined
    /api/order/account-services
    """
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"}, 401)

        if request.GET.get('pending-activation'):
            return get_pending_activation(request, format=None)

        return Response({"result": "errors",
                         "errors": True,
                         "message": "Anything?"}, 400)

    def post(self, request):
        data = json.loads(request.body)
        oi_id = data.get('orderitem').get('pk')
        oi = OrderItems.objects.get(pk=oi_id)
        api_id = data.get('api_id')
        quantity = data.get('quantity')
        pperunit = oi.price_per_unit
        price = oi.price
        if oi.upgrade_service:
            pperunit = oi.upgrade_service.price_per_unit
            price = oi.upgrade_service.price
        oim = oi.metadata
        oim['enabled'] = False
        ac = AccountServices(
            account=oi.order.account,
            product=oi.product,
            order=oi.order,
            oi=oi,
            item_type=oi.product.product_key.name,
            quantity=quantity,
            price_per_unit=pperunit,
            price=price,
            bill_interval=oi.bill_interval,
            next_bill=oi.next_bill,
            metadata=oim,
            payment_complete=oi.payment_complete,
            auto_provision=oi.auto_provision,
            provision_status='provisioned',
            api_id=api_id,
        )
        ac.save()
        return Response({"result": "Service Account created"}, 200)


