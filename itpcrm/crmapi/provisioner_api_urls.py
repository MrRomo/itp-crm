from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from crmapi import provisioner_views as views

urlpatterns = [

    url(r'^orders/(?P<order_id>[0-9]+)/order-items/(?P<item_id>[0-9]+)/update', views.API_UpdateOrderStatus.as_view()),
    url(r'^orders/account-services', views.API_AccountServices.as_view()),
    #url(r'^orders/pending-activation', views.API_GetPendingActivation.as_view()),
    url(r'^orders', views.API_GetOrders.as_view()),


]


