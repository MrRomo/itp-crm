# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import subprocess
import os
import requests
import base64
import logging
from datetime import datetime, timedelta
from decimal import Decimal
from django.shortcuts import render
from django.http import Http404
from django.template.loader import render_to_string
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.db import connection
from django.conf import settings
from weasyprint import HTML

from crmapi.auth import do_auth
from crmapi.serializers import (
        AccountsSerializer,
        QuotesSerializer,
        ServiceAddressSerializer,
        OrdersSerializer,
        AccountsContactsSerializer,
        AgentsSerializer,
        ContactsSerializer,
        AgentOwnedBySerializer,
        QuoteItemGroupSerializer,
        QuoteItemsSerializer,
        AccountServicesSerializer,
        ProductsSerializer,
        OrderItemsSerializer,
)
from crm.models import (
        Accounts,
        Contacts,
        Orders,
        OrderItems,
        Agents,
        AgentOwnedBy,
        Quotes,
        ServiceAddress,
        QuoteItemGroup,
        QuoteItems,
        Products,
        ITPServices_Addresses,
        QUOTE_STATUSES,
        ServiceAddons,
        AccountServices,
)

logger = logging.getLogger()


def get_account_id(uid):
    try:
        return Contacts.objects.get(auth_api_id=uid)
    except Contacts.DoesNotExist:
        return None

def _valiate_quote_to_agent():
    """Validate an agent is modifying or reading
    a quote they are assigned to..."""

    pass

def search_date_to_datetime(d):
    """
    parse
    YYYYMMDD HHMMSS <- this is auto added
    """
    yr = d[0:4]
    mon = d[4:6]
    day = d[6:8]
    d = datetime.strptime(d, '%Y%m%d %H%M%S')
    return d


class API_Quote(APIView):
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)

        filterargs = {"agent": agent}
        filter_type = request.GET.get('filtertype', 'eq')

        if request.GET.get('filterby'):
            if request.GET.get('filterby') == 'account':
                if request.GET.get('filtervalue'):
                    if filter_type == 'eq':
                        acct = Accounts.objects.get(pk=int(request.GET.get('filtervalue')))
                        filterargs['account'] = acct
            if request.GET.get('filterby') == 'date': 
                sd = request.GET.get('start_date')
                ed = request.GET.get('end_date')
                if not ed:
                    ed = sd
                d_sd = search_date_to_datetime('%s 000000' % sd)
                d_ed = search_date_to_datetime('%s 235900' % ed)
                filterargs['date_created__gte'] = d_sd
                filterargs['date_created__lte'] = d_ed


        # owned by this master daddy
        quotes = []
        if agent.agent_type == 'master':
            subagents = []
            ao = AgentOwnedBy.objects.filter(master_agent=agent)
            for agents in ao:
                subagents.append(agents.sub_agent)
            del filterargs['agent']
            filterargs['agent__in'] = subagents
            for q in Quotes.objects.filter(**filterargs):
                if not q in quotes:
                    quotes.append(q)

            del filterargs['agent__in']
            filterargs['agent'] = agent
            for q in Quotes.objects.filter(**filterargs):
                if not q in quotes:
                    quotes.append(q)
        else:
            for q in Quotes.objects.filter(**filterargs):
                quotes.append(q)


        if quotes:
            #results = []
            #for q in quotes:
            #    totals = Decimal('0.00')
            #    for qi in QuoteItems.objects.filter(quote=q):
            #        totals += qi.item_price_total
            #    ser = QuotesSerializer(q, many=False)
            #    ser.data['total_price'] = totals
            
            #    results.append(ser.data)
            ser = QuotesSerializer(quotes, many=True)
            d = dict(result=ser.data, message="ok", errors=False)
            return Response(d)
        else:
            return Response(dict(result=[], errors=True,
                                 message="No quotes found for this date range"), 404)


    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        data = json.loads(request.body)
        quote = Quotes(agent=agent)
        quote.quote_status = QUOTE_STATUSES.get('DRAFT')
        account_id = data.get('account')
        quote.account = Accounts.objects.get(pk=account_id)
        quote.name = data.get('name')
        quote.notes = data.get('notes')
        quote.save()
        ser = QuotesSerializer(quote, many=False)
        return Response(dict(result=ser.data, message="ok", errors=False))


class API_QuoteUpdate(APIView):

    def get(self, request, pk, format=None):
        return 'brah'

    def post(self, request, pk, format=None):
        return 'nahbra'

    def patch(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        data = json.loads(request.body)
        quote = Quotes.objects.get(pk=pk)
        status = data.get('quote_status')
        if quote.quote_status in [QUOTE_STATUSES.get('DRAFT')]:
            quote.quote_status = status
            quote.save()
            return Response(dict(result="ok", errors=False,
                                 message="Quote status updated"), 200)
        else:
            return Response(dict(result="errors", errors=True,
                                 message="Quote status change not allowed"), 400)


class API_GetQuoteByID(APIView):
    """
    GET /agent/quotes/[id]
    """
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION') 
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        try:
            quotes = Quotes.objects.get(pk=pk, agent=agent)
        except Quotes.DoesNotExist:
            quotes = None
            if agent.agent_type == 'master':
                subagents = []
                try:
                    q = Quotes.objects.get(pk=pk)
                    ao = AgentOwnedBy.objects.filter(master_agent=agent)
                    for agents in ao:
                        subagents.append(agents.sub_agent.pk)
                    try:
                        if q.agent.pk in subagents:
                            quotes = q
                    except AttributeError:
                        return Response(dict(result="errors", errors=True,
                                message="Quote permission forbidden"), 401)
                        pass
                except Quotes.DoesNotExist:
                    return Response(dict(result="errors", errors=True,
                                message="Quote does not exist"), 404)

        if quotes:
            ser = QuotesSerializer(quotes, many=False)
            quote_data = ser.data
            quote_data['quote_items'] = []
            for qi in quotes.get_parent_quote_items():
                qiser = QuoteItemsSerializer(qi, many=False)
                qiser_data = qiser.data
                #qiser_data['related_addons'] = []
                quote_data['quote_items'].append(qiser.data)

            for qi in quote_data['quote_items']:
                qis = QuoteItems.objects.filter(parent_quote_item=QuoteItems.objects.get(pk=qi.get('pk')))
                for quote_orderitems_related in qis:
                    quote_orderitems_related_ser = QuoteItemsSerializer(quote_orderitems_related, many=False)
                    if not qi.get('related_addons'):
                        qi['related_addons'] = []
                    qi['related_addons'].append(quote_orderitems_related_ser.data)
            d = dict(result=quote_data, message="ok", errors=False)
            return Response(d)
        else:
            return Response(dict(result="errors", errors=True,
                                message="Agent unauthorized quote access"), 401)

    def patch(self, request, pk, format=None):
        """
        PATCH /agent/quotes/[id]
        Limited access - only patch allowed_fields
        """
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quotes = Quotes.objects.get(pk=pk)


        if quotes.quote_status not in [QUOTE_STATUSES.get('DRAFT')]:
            return Response({'message': "Quote not in valid status to update",
                             "errors": True, "result": "errors"}, status=400)

        if not quotes.agent:
            return Response({'message': "Quote does not have an agent",
                             "errors": True, "result": "errors"}, status=400)

        if not agent.pk == quotes.agent.pk:
            return Response({'message': "Unauthorized access to update quote",
                             "errors": True, "result": "errors"}, status=401)

        if agent.agent_type == 'sub':
            return Response({'message': "Unauthorized access to update quote (sub)",
                             "errors": True, "result": "errors"}, status=401)

        allow_update = False
        if quotes.agent.pk == agent.pk:
            allow_update = True
        elif agent.agent_type == 'master':
            agent_from_quote = quotes.agent
            try:
                ao = AgentOwnedBy.objects.get(master_agent=agent, sub_agent=agent_from_quote)
                allow_update = True

            except AgentOwnedBy.DoesNotExist:
                return Response({'message': "Unauthorized access - not your subagent",
                                 "errors": True, "result": "errors"}, status=401)

        if allow_update:
                #allowed_fields = ['name', 'notes']
                allowed_fields = ['quote_status']

                data = json.loads(request.body)
                payload_patch = {}
                for k in allowed_fields:
                    if data.get(k):
                        payload_patch[k] = data.get(k)

                updated = False
                for k, v in payload_patch.items():
                    updated = True
                    setattr(quotes, k, v)

                if updated:
                    quotes.save()


        return Response(dict(result="ok",
                             message="Updated quote",
                             errors=False), 200)



class API_GetOrdersByID(APIView):
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        try:
            orders = Orders.objects.get(agent=agent.pk, pk=pk)
        except Orders.DoesNotExist:
            return Response({"result": "error", "errors": True, "message": "Order does not exist for this agent"}, status=400)
        d = {'order': None, 'orderitems': []}
        ser = OrdersSerializer(orders, many=False)
        d['order'] = ser.data
        #oi = OrderItems.objects.filter(order=orders)
        #for i in oi:
        #    ois = OrderItemsSerializer(i, many=False)
        #    d['orderitems'].append(ois.data)
        if orders:
            ser = OrdersSerializer(orders, many=False)
            order_data = ser.data
            order_data['order_items'] = []
            for qi in orders.get_parent_quote_items():
                qiser = OrderItemsSerializer(qi, many=False)
                qiser_data = qiser.data
                #qiser_data['related_addons'] = []
                order_data['order_items'].append(qiser.data)

            for qi in order_data['order_items']:
                qis = OrderItems.objects.filter(parent_order_item=OrderItems.objects.get(pk=qi.get('pk')))
                for order_orderitems_related in qis:
                    order_orderitems_related_ser = OrderItemsSerializer(order_orderitems_related, many=False)
                    if not qi.get('related_addons'):
                        qi['related_addons'] = []
                    qi['related_addons'].append(order_orderitems_related_ser.data)
            d = dict(result=order_data, message="ok", errors=False)
            return Response(d)
        return Response({"result": d, "message": "ok", "errors": False})

    def patch(self, request, pk, format=None):
        """
        PATCH /agent/quotes/[id]
        Limited access - only patch allowed_fields
        """
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quotes = Quotes.objects.get(pk=pk, agent=agent)

        allowed_fields = ['name', 'notes']

        data = json.loads(request.body)
        payload_patch = {}
        for k in allowed_fields:
            if data.get(k):
                payload_patch[k] = data.get(k)

        updated = False
        for k, v in payload_patch.items():
            updated = True
            setattr(quotes, k, v)

        if updated:
            quotes.save()

        return Response(dict(result="ok",
                             message="Updated quote",
                             errors=False), 200)



class API_QuotesSubmit(APIView):
    """
    POST /agent/quotes/[id]/[contact_id]/submit

    """
    def post(self, request, pk, contact_id, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)

        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        
        cid = int(contact_id)
        if not cid:
            return Response(dict(result="errors", errors=True,
                                message="contact_id missing in JSON body"), 400)
        try:
            contact = Contacts.objects.get(pk=cid,
                                          admin_group=True,
                                          account=quote.account)
        except Contacts.DoesNotExist:
            return Response(dict(result="errors", errors=True,
                                message="Not admin contact / account ownership."), 400)

        # if the quote is in draft status
        allowed_statuses = [
            QUOTE_STATUSES.get('DRAFT'),
            QUOTE_STATUSES.get('MANAGER_APPROVED')
        ]
        quote.quote_assignee = contact
        if quote.quote_status in allowed_statuses:
            quote.quote_status = QUOTE_STATUSES.get('PENDING_CUSTOMER_PRE_APPROVAL')
            quote.save()

        # if the quote is in manager approval required 
        if quote.quote_status == QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED'):
            quote.quote_status = QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL')
            quote.save()
        ser = QuotesSerializer(quote, many=False)
        d = dict(result=ser.data, submit='ok', errors=False)
        return Response(d, 200)


class API_NewQuoteItemGroup(APIView):
    """
    POST /agent/quotes/[id]/item-group
    """
    def post(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        if not str(quote.quote_status) in [QUOTE_STATUSES.get('DRAFT'),\
                QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED')]:
            return Response({}, 400)

        data = json.loads(request.body)
        qig = QuoteItemGroup(quote=quote, name=data.get('name', 'Items'))
        qig.save()
        ser = QuoteItemGroupSerializer(qig, many=False)
        d = dict(result=ser.data, created=True)
        return Response(d, 200)




class API_AddItemToQuote(APIView):
    """
    POST /agent/quotes/[id]/item-group/[id]/add/[product-id]
    """
    def post(self, request, pk, pid, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        data = json.loads(request.body)

        # if the quote is in allowed statuses
        allowed_statuses = [
            QUOTE_STATUSES.get('DRAFT'),
            QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL'),
            QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED'),
        ]
        if quote.quote_status not in allowed_statuses:
           return Response({'message':"Error, Quote not in valid state for changes",
													"error": True,
													"result": "error"},
													status=400)



        # if not str(quote.quote_status) in [QUOTE_STATUSES.get('DRAFT'),\
        #         QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL')]:
        #     return Response({}, 400)

        # qig = QuoteItemGroup.objects.get(quote=quote, pk=ig)
        prod = Products.objects.get(pk=pid)
        purchase_type='new-order'
        if prod.product_type == 'ServiceAddon':
            purchase_type = 'service-addon'

        qoi = QuoteItems(quote=quote, account=quote.account,
                        product=prod,price=prod.price,
                        quantity=data.get('quantity', int(1)),
                        purchase_type=purchase_type,
        )
        if data.get('parent_quoteitem'):
            pqi = QuoteItems.objects.get(pk=data.get('parent_quoteitem'))
            qoi.parent_quote_item = pqi

        if data.get('parent_accountservice'):
            pqi = AccountServices.objects.get(pk=data.get('parent_accountservice'))
            qoi.upgrade_service = pqi

        if data.get('service_address'):
            sv = ServiceAddress.objects.get(pk=data.get('service_address'))
            qoi.service_address = sv

        qoi.save()
        ser = QuoteItemsSerializer(qoi, many=False)
        d = dict(result=ser.data, created=True, errors=False, message="ok")
        return Response(d, 200)

    def delete(self, request, pk, qid, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        if not str(quote.quote_status) in [QUOTE_STATUSES.get('DRAFT'),\
                QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED'),
                QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL')]:
            return Response({}, 400)

        #qig = QuoteItemGroup.objects.get(quote=quote, pk=ig)
        qoi = QuoteItems.objects.get(pk=qid, quote=quote)
        qoi.delete()
        d = dict(result='deleted', deleted=True)
        return Response(d, 200)


class API_UpdateQuoteItemMetadata(APIView):
    """
    PATCH /agent/quotes/[id]/quote-item/[oid]/metadata
    payload:
    
    """
    def patch(self, request, pk, oid, format=None):
        data = json.loads(request.body)
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        fields = [
            str(QUOTE_STATUSES.get('DRAFT')),
            str(QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED')),
            str(QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL'))
        ]
        if not str(quote.quote_status) in fields:
            return Response(dict(result="errors",
                                message="Quote not in proper status",
                                errors=True), 400)

        qi = QuoteItems.objects.get(pk=oid, quote=quote)
        oim = qi.metadata
        excluded_fields = ['staus', 'enabled', 
                           'date_modified', 'date_added',
                           'create_result',]
        for k, v in data.items():
            if not k in excluded_fields:
                oim[k] = v
        qi.metadata = oim
        qi.save()
        """new_price = Decimal(str(data.get('price', qi.price)))
        qi.price = new_price"""
        ser = QuoteItemsSerializer(qi, many=False)
        d = dict(result='ok', errors=False, updated=True, response=ser.data)
        return Response(d, 200)


class API_UpdateQuoteItemDescription(APIView):
    """
    PATCH /agent/quotes/[id]/quote-item/[oid]/description

    payload:
        {"description": "hello this is description"}

    """
    def patch(self, request, pk, oid, format=None):
        data = json.loads(request.body)
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        fields = [
            str(QUOTE_STATUSES.get('DRAFT')),
            str(QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED'))
        ]
        if not str(quote.quote_status) in fields:
            return Response({}, 400)

        qi = QuoteItems.objects.get(pk=oid, quote=quote)
        new_description = data.get('description')
        qi.prod_description = new_description
        qi.save()
        ser = QuoteItemsSerializer(qi, many=False)
        d = dict(result='ok', errors=False, updated=True, response=ser.data)
        return Response(d, 200)



class API_UpdateQuoteItemPrice(APIView):
    """
    PATCH /agent/quotes/[id]/quote-item/[oid]/price

    payload:
        {"price": "xx.xx"}

    """
    def patch(self, request, pk, oid, format=None):
        data = json.loads(request.body)
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        quote = Quotes.objects.get(pk=pk, agent=agent)
        fields = [
            str(QUOTE_STATUSES.get('DRAFT')),
            str(QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED'))
        ]
        if not str(quote.quote_status) in fields:
            return Response({}, 400)

        qi = QuoteItems.objects.get(pk=oid, quote=quote)
        new_price = Decimal(str(data.get('price', qi.price)))
        qi.price = new_price
        qi.save()
        ser = QuoteItemsSerializer(qi, many=False)
        d = dict(result='ok', errors=False, updated=True, response=ser.data)
        return Response(d, 200)



class API_Products(APIView):
    """
    GET /agent/products
    """
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        prod = Products.objects.filter(deleted=False)
        ser = ProductsSerializer(prod, many=True)
        return Response(ser.data, 200)


class API_ProductsByID(APIView):
    """
    GET /agent/products/[id]
    """
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        prod_obj = Products.objects.get(deleted=False, pk=pk)
        ser = ProductsSerializer(prod_obj, many=False)
        service_addons = prod_obj.get_addons_for_product()

        #service_addons = Products.objects.filter(product_type="ServiceAddon", product_rel__icontains=prod.part_number)
        
        serv_addons = []
        for x in service_addons:
            s = ProductsSerializer(x, many=False)
            serv_addons.append(s.data)
        return Response(dict(result={"product": ser.data, "service_addons": serv_addons},
                            errors=False, message="ok"), 200)


class ServicesByServiceAddressID(APIView):
    """
    /agents/service-address/{address_id}
    """
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        sort_by = request.GET.get('sort_by')
        agent = Agents.objects.get(auth_api_id=uid)
        service_address = ServiceAddress.objects.get(pk=int(pk))
        l = ITPServices_Addresses.objects.filter(address=service_address)
        products = []
        for i in Products.objects.filter(depends_on_service_address=False, product_type='Service'):
            products.append(i.pk)
        for i in l:
            if not i.product.pk in products:
                products.append(i.product.pk)
        # get the actual objs
        prod = Products.objects.filter(id__in=products)
        if sort_by:
            sort_by_fields = sort_by.split(',')
            tp = tuple(sort_by_fields)
            prod = prod.order_by(*tp)
        
        ser = ProductsSerializer(prod, many=True)
        servaddr = ServiceAddressSerializer(service_address, many=False)
        return Response({"result": {"services": ser.data,
                                     "address": servaddr.data},
                         "errors": False,
                         "message": "ok"}, 200)



class API_ServiceAddress(APIView):
    """
    /agent/service-address

    GET - gets a list of the service addresses
    """
    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        data = json.loads(request.body)
        if not data:
            return Response({"result": "empty-request", "errors": True, "message": "No fields provided in body"}, 400)

        agent = Agents.objects.get(auth_api_id=uid)
        full_address = data.get('full_address')
        suite_num = data.get('suite_num')
        itpfiber_status = data.get('itpfiber_status')
        
        create_params = dict(full_address=full_address, 
                             itpfiber_status=itpfiber_status, 
                             suite_num=suite_num, 
                             validated=False)
        try:
            s = ServiceAddress(**create_params)
            s.save()
            return Response({"result": "ok", "errors": False, 
                             "message": "Service address created", 
                             "data": create_params})
        except Exception as e:
            logger.error("POST Agent Service address: \n- Exception service address create %s" % str(e))
            return Response({'result': "error", "errors": True, "message": "exception %s" % str(e)},
                            status=400)


    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        d = {}
        try:
            limit = int(request.GET.get('limit', int(10)))
        except (ValueError, TypeError):
            limit = int(10)

        filter_type = request.GET.get('filter_type')
        ex_fields = ['limit', 'filter_type']
        for k, v in dict(request.GET).items():
            if not k in ex_fields:
                d[k] = v[0]
        return self.do_search(request, data=d, limit=limit, filter_type=filter_type)

    def do_search(self, request, format=None, data=None, limit=1000, filter_type='like'):
        #raise Exception(request.body)
        if not data:
            try:
                data = json.loads(request.body)
            except Exception as e:
                data = {}
                return Response({"result": "error", "errors": True,
                                "message": "Invalid JSON body"}, 500)
        filters = {}
        for k, v in data.items():
            if filter_type == 'eq':
                filters[k] = v
            if filter_type == 'like':
                filters['%s__icontains' % k] = v
            if filter_type == 'startswith':
                filters['%s__istartswith' % k] = v
        #limit = request.GET.get('limit', int(1000))
        addrs = ServiceAddress.objects.filter(**filters)[:limit]
        ser = ServiceAddressSerializer(addrs, many=True)
        return Response({"result": ser.data, "errors": False, "message": "Service address response"}, status=200)



class API_GetAccountServices(APIView):
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)

        acc = Accounts.objects.get(pk=pk)
        serv = AccountServices.objects.filter(account=acc)
        #servaddons = ServiceAddons.objects.filter(service=serv)

        serv_json = AccountServicesSerializer(serv, many=True)

        response_json = {"result": serv_json.data, "errors": False, "message": "ok"}
        return Response(response_json, status=200)
        #return Response({}, status=200)



class API_GetAgentAccountContacts(APIView):
    """
    GET /agent/accounts/contacts
    Get a list of contacts from
    accounts that are linked to an agent
    """
    def get(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        account = Accounts.objects.get(pk=pk)
        agent = Agents.objects.get(auth_api_id=uid)

        if account.allow_agent_add_contact(agent):
            contacts = Contacts.objects.filter(account=account)
            ser = ContactsSerializer(contacts, many=True)
            return Response({"result": ser.data, "message": "ok", "errors": False}, status=200)
        return Response({"result": [], "message": "No contacts", "errors": True}, status=400)
        
        #ser = AccountsSerializer(Accounts.objects.filter(agent=agent), many=True)
        #return Response(ser.data)

    def post(self, request, pk, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        acc = Accounts.objects.get(pk=pk)
        data = json.loads(request.body)

        # checks if agent is subagent of master who owns the account
        # or if master agent of this account. 
        contact = None
        if acc.allow_agent_add_contact(agent):
            contact_excluded_fields = [
                'account',
                'client_portal_login_enabled',
                'auth_api_id',
                'billing_api_id',
                #'admin_group',
                #'billing_group',
                #'technical_group',
                'password',
            ]
            try:
                contact = Contacts()
                contact.account = acc
                logger.debug(contact)
                contact.admin_group = True
                for k, v in data.items():
                    if not k in contact_excluded_fields:
                        logger.debug("set %s to %s" % (k, v))
                        setattr(contact, k, v)
                contact.save()
            except Exception as e:
                return Response({"result": "error", "errors": True,
                                "message": "Exception %s" % str(e)},
                                status=500)
        if contact:
            ser = ContactsSerializer(contact, many=False)
            d = dict(result=ser.data, errors=False, message="Contact added")
            return Response(d, status=200)
        else:
            d = dict(result="errors", errors=True, message="Agent permission - must be part of an account")
            return Response(d, status=400)




class API_GetAgentAccounts(APIView):
    """
    GET /agent/accounts/
    Get a list of accounts that are linked to an agent
    """
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'message': "Unauthorized", "errors": True, "result": "errors"},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        filterargs = {"agent": agent}
        accounts = []
        if request.GET.get('filterby') and request.GET.get('filtervalue'):
            if request.GET.get('filterby', 'x') == "name":
                filter_type = request.GET.get('filtertype', 'contains')
                if filter_type == 'eq':
                    filterargs['name'] = request.GET.get('filtervalue')
                if filter_type == 'contains':
                    filterargs['name__icontains'] = request.GET.get('filtervalue')
                if filter_type == 'startswith': 
                    filterargs['name__istartswith'] = request.GET.get('filtervalue')

        if agent.agent_type == 'master':
            for x in AgentOwnedBy.objects.filter(master_agent=agent):
                filterargs['agent'] = x.sub_agent
                for acc in Accounts.objects.filter(**filterargs):
                    if not acc in accounts:
                        accounts.append(acc)

            filterargs['agent'] = agent
            for acc2 in Accounts.objects.filter(**filterargs):
                if not acc2 in accounts:
                    accounts.append(acc2)
        else:
            accounts = Accounts.objects.filter(**filterargs)

        ser = AccountsSerializer(accounts, many=True)
        d = dict(result=ser.data, message="ok", errors=False)
        return Response(d)

    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                            'message': "Unauthorized",
                            "errors": True},
                            status=401)
        #Not sure if this is correct, but we need to allow the Magent 
        #to specicy the agent in the body.
        #agent = Agents.objects.get(id=uid)
        data = json.loads(request.body)
        if not data.get('account'):
            return Response({"result": "error",
                             "errors": True,
                             "message": "Missing required field "},
                             status=500)
        if not data['account']['agent']:
            return Response({"result": "error",
                            "errors": True,
                            "message": "Missing Agent in Body"},
                            status=500)
        agent = Agents.objects.get(id=data['account']['agent'])
        #Will remove Agent from blob now to avoid errors

        excluded_fields = [
            'accountnumber',
            'billing_api_id',
            'crm_login_user',
            'agent',
        ]
        try:
            acct = Accounts()
            for k, v in data.get('account').items():
                if not k in excluded_fields:
                    logger.debug(k)
                    setattr(acct, k, v)
            acct.agent = agent
            acct.save()
        except Exception as e:
            return Response({"result": "error", "errors": True,
                            "message": "Exception %s" % str(e)},
                            status=500)
            
        contact_excluded_fields = [
            'account',
            'client_portal_login_enabled',
            'auth_api_id',
            'billing_api_id',
            'admin_group',
            'billing_group',
            'technical_group',
            'password',
        ]
        """try:
            contact = Contacts()
            contact.account = acct
            contact.admin_group = True
            for k, v in data.get('contact', {}).items():
                if not k in contact_excluded_fields:
                    setattr(contact, k, v)
            contact.save()
        except Exception, e:
            return Response({"result": "error", "errors": True,
                            "message": "Exception %s" % str(e)},
                            status=500)"""

        ser = AccountsSerializer(acct, many=False)
        d = dict(result=ser.data, errors=False, message="Account added")
        return Response(d, status=200)



class API_GetAgentAccountByID(APIView):
    """
    GET /agent/accounts/<id>
    Get a single account by ID
    Must be an account that is related to the agent
    or will return a 404
    """
    def get(self, request, pk, format=None):
        logger.debug("BY ID")
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        try:
            account = Accounts.objects.get(pk=pk)
        except Accounts.DoesNotExist:
            return Response({'result': "errors",
                             'message': "Unauthorized account access",
                             'errors': True},
                            status=401)
        acc = Accounts.objects.filter(agent=agent)
        if acc:
            ser = AccountsSerializer(account, many=False)
            return Response(ser.data)
        return Response({'message': "Unauthorized account access", "errors": True, "result": "errors"},
                            status=401)
        


class API_Orders(APIView):
    """
    GET /agent/orders
    Gets a list of the orders assigned to a user
    """
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        account_pk = request.GET.get('accountpk', None)
        """
        if not account_pk:
            return Response({"result": "errors", "message": "Missing account pk",
                                "errors": True}, status=400)
        """
        results = []
        orders_list = []
        accounts = None
        if account_pk:
            try:
                accounts = Accounts.objects.get(pk=account_pk)
            except Accounts.DoesNotExist:
                return Response({"result": "errors", "message": "Invalid Account",
                                "errors": True}, status=400)

            orders = Orders.objects.filter(agent=agent, account=accounts)
        else:
            orders = Orders.objects.filter(agent=agent)

        for i in orders:
            if not i in orders_list:
                orders_list.append(i)
                o = OrdersSerializer(i, many=False)
                results.append(o.data)

        agents = [agent]
        for a in AgentOwnedBy.objects.filter(master_agent=agent):
            agents.append(a.sub_agent)

        for a in agents:
            if accounts:
                f = {"agent": a, 'account': accounts}
            else:
                f = {"agent": a}
            for o in Orders.objects.filter(**f):
                if not o in orders_list:
                    orders_list.append(o)
                    ser = OrdersSerializer(o, many=False)
                    results.append(ser.data)

        if not results:
            results = {"result": [], "errors": False, "Message": "Empty"}
        return Response({"result": results, "errors": False, "Message": "ok"}, 200)


class API_AgentPassword(APIView):
    """
    Agent Password PATCH
    """
    def patch(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        try:
            data = json.loads(request.body)
            #agent.update_password(data["password"])
            if data.get('pk'):
                logger.debug("Trying to get agent2")
                agent2 = Agents.objects.get(agent_type='sub', pk=int(data.get('pk')))
                try:
                    agent_owned = AgentOwnedBy.objects.get(master_agent=agent, sub_agent=agent2)
                    agent = agent2
                    subagent_update = True
                    agent.update_password(data["password"])
                    logger.debug("Setting agent2") 
                except AgentOwnedBy.DoesNotExist:
                    return Response({"result": "error",
                            "errors": True,
                            "message": "Not allowed to update this sub-agent"}, 401)
            else:
                agent.update_password(data["password"])

            return Response({'result': "ok", "errors": False,
                    "message": "Password updated successfully"} )
        except ValueError:
            #json err
            data = {}
            return Response({"result": "error",
                            "errors": True,
                            "message": "Invalid or no JSON provided"}, 400)


class API_AgentStatusChange(APIView):
    """
    Master agent update activate/deactivate sub-agent
    """
    def set_agent_status(self, request, pk, format=None, activate=True):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)

        agent = Agents.objects.get(auth_api_id=uid)
        if not agent.agent_type == 'master':
            return Response({"result": "Unauthorized",
                             "errors": True,
                             "message": "Master Agent Requried"},
                    status=401)

        try:
            agent_to_del = AgentOwnedBy.objects.get(
                master_agent=agent,
                sub_agent=Agents.objects.get(pk=pk)
            )
        except (AgentOwnedBy.DoesNotExist, Agents.DoesNotExist):
            return Response({"result": "Unauthorized",
                             "errors": True,
                             "message": "No permission on subagent"},
                             401)

        a = agent_to_del.sub_agent
        a.active = status_to_change
        a.save()
        if status_to_change:
            msg = "Activated agent account"
        else:
            msg = "Deactivated agent account"
        return Response({"result": "Unauthorized",
                         "errors": True,
                         "message": msg},
                    status=200)


class API_AgentActivate(API_AgentStatusChange):
    """
    Activate the sub-agent
    """
    def post(self, request, pk, format=None):
        return self.set_agent_status(
            request,
            pk,
            format=None,
            activate=True)


class API_AgentDeactivate(API_AgentStatusChange):
    """
    dectivate the sub-agent
    """
    def post(self, request, pk, format=None):
        return self.set_agent_status(
            request,
            pk,
            format=None,
            activate=False)


class API_AgentProfile(APIView):
    """
    Agent Profile GET / PATCH 
    """
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        ser = AgentsSerializer(agent, many=False)
        return Response({"result": ser.data,
                        "errors": False,
                        "message": "Agent Profile"}, 200)
        #return Response(ser.data)

    def post(self, request, format=None):
        pass

    def patch(self, request, format=None):
        data = json.loads(request.body)
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        subagent_update = False
        if data.get('pk'):
            logger.debug("Trying to get agent2")
            agent2 = Agents.objects.get(agent_type='sub', pk=int(data.get('pk')))
            try:
                agent_owned = AgentOwnedBy.objects.get(master_agent=agent, sub_agent=agent2)
                agent = agent2
                subagent_update = True
                logger.debug("Setting agent2") 
            except AgentOwnedBy.DoesNotExist:
                return Response({"result": "error",
                        "errors": True,
                        "message": "Not allowed to update this sub-agent"}, 401)
        excluded_fields = [
            #'commission_rate',
            'auth_api_id',
            'agent_type',
            'password',
            'pricing_threshold',
            'client_portal_login_enabled',
            'active',
            'email',
            'pk']
        if not subagent_update:
            excluded_fields.append('commission_rate')
        logger.debug("Ready to update %s" % agent.pk)
        for k in excluded_fields:
            try:
                del data[k]
            except KeyError:
                pass
        for k, v in data.items():
            try:
                setattr(agent, k, v)
            except Exception:
                pass
        agent.save()
        ser = AgentsSerializer(agent, many=False)
        d = dict(result=ser.data, updated=True, errors=False)
        return Response(d)




class API_Agents(APIView):
    """
    Create agent as a master admin
    """
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        if not agent.agent_type == 'master':
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agents2 = AgentOwnedBy.objects.filter(master_agent=agent)
        results = []
        for i in agents2:
            ag = AgentsSerializer(i.sub_agent, many=False)
            results.append(ag.data)
        d = dict(result=results, errors=False)
        return Response(d, 200) 


    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'result': "errors",
                             'message': "Unauthorized",
                             'errors': True},
                            status=401)
        agent = Agents.objects.get(auth_api_id=uid)
        if agent.agent_type == 'master':
            data = json.loads(request.body)
            a = Agents(**data)
            a.agent_type = 'sub'
            a.client_portal_login_enabled = True
            try:
                a.save()
            except Exception as e:
                return Response({"created": False,
                                 "result": "error",
                                 "message": "Exception - %s" % str(e)}, 400)
            try:
                ow = AgentOwnedBy.objects.get(master_agent=agent, sub_agent=a)
            except AgentOwnedBy.DoesNotExist:
                ow = AgentOwnedBy(master_agent=agent, sub_agent=a)
                ow.save()

            ser = AgentOwnedBySerializer(ow, many=True)
            return Response({"created": True, "result": "ok"})

        out = json.dumps(dict(result='errors', error=True))
        return Response(out, 400)


class API_PDFRender(APIView):
    def get(self, request, pk, format=None):
        #pdf_hash = PDFHash.objects.get(pk=request.GET.get('k', None))
        #print "top wtf"
        quote = Quotes.objects.get(pk=pk)
        sub_total = Decimal('0.0')
        tax_sum = Decimal('0.0')
        tax_totals = Decimal('0.0')
        mrc_subtotal = Decimal('0.0')
        nrc_subtotal = Decimal('0.0')

        excluded_acctserv = []
        for items in QuoteItems.objects.filter(quote=quote):
            logger.debug(items.item_price_total)
            if items.product.product_type == 'Product':
                nrc_subtotal += items.item_price_total
                if items.taxable:
                    tax_sum += items.item_price_total + items.tax_percent * items.item_price_total / 100
                    tax_totals += items.tax_percent * items.item_price_total / 100
            else:
                mrc_subtotal += items.item_price_total
                if items.upgrade_service and not items.product.product_type == 'ServiceAddon':
                    excluded_acctserv.append(items.upgrade_service)

        for a in AccountServices.objects.filter(account=quote.account):
            if not a in excluded_acctserv:
                mrc_subtotal += a.price
            for sa in ServiceAddons.objects.filter(service=a):
                mrc_subtotal += sa.price

        acctserv = AccountServices.objects.filter(account=quote.account)
        valid_until = quote.date_created + timedelta(days=30)
        quote_service_addons = []

        if acctserv:
            quote_item_services = QuoteItems.objects.filter(quote=quote)
            quote_service_addons = []
            for qq in quote_item_services:
                if qq.product.product_type == 'ServiceAddon':
                    quote_service_addons.append(qq)

        html_string = render_to_string('crm/email_templates/send-quote.html', {
            'quote': quote,
            'account_services': acctserv,
            'sub_total': sub_total,
            'valid_until': valid_until,
            'nrc_subtotal': nrc_subtotal,
            'mrc_subtotal': mrc_subtotal,
                'tax_sum': Decimal('%.2f' % tax_sum),
                'tax_totals': Decimal('%.2f' % tax_totals),
            'quote_service_addons': quote_service_addons,
            })
        html_string_sig = render_to_string('crm/email_templates/send-quote-sig.html', {
            'quote': quote,
            'account_services': acctserv,
            'sub_total': sub_total,
            'valid_until': valid_until,
            'nrc_subtotal': nrc_subtotal,
                'tax_sum': Decimal('%.2f' % tax_sum),
                'tax_totals': Decimal('%.2f' % tax_totals),
            'mrc_subtotal': mrc_subtotal,
            'quote_service_addons': quote_service_addons,
            })

        #print "doc pages"
        #print document.pages()
        #print dir(document)
        #print "RESULT"

        send_pages = []
        html = HTML(string=html_string)
        document = html.render()
        #print dir(document.pages[0])
        #print document.pages[0].__dict__
        for pg in document.pages:
            send_pages.append(pg)

        html2 = HTML(string=html_string_sig)
        document2 = html2.render()
        #for pg in document2.pages:
        send_pages.append(document2.pages[0])
        #send_pages.append(pg)

        #print result
        #print result
        # FINALLy write the pdf
        newdoc = document.copy(send_pages)
        result = newdoc.write_pdf()

        # just testing here...
        o = open('/tmp/pdfcaca1-%s.pdf' % quote.pk, 'wb+')
        o.write(result)
        o.close()
        #try:
        #    pypdftk.fill_form('/tmp/pdfcaca-%s.pdf' % quote.pk,
        #                        out_file='/tmp/pdfcaca2-%s.pdf' % quote.pk, flatten=True)
        #except Exception, e:
        #    print "Exception flattening: %s" % str(e)
        #    pass
        cmd = "gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE -sDEVICE=pdfwrite "
        cmd += "-sColorConversionStrategy=/LeaveColorUnchanged  -dAutoFilterColorImages=true "
        cmd += "-dAutoFilterGrayImages=true -dDownsampleMonoImages=true -dDownsampleGrayImages=true "
        cmd += "-dDownsampleColorImages=true -sOutputFile=/tmp/pdfcaca12-%s.pdf /tmp/pdfcaca1-%s.pdf" % (quote.pk, quote.pk)

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        p.wait()
        rout, rerr = p.communicate()
        logger.debug("%s - %s" % (rout, rerr))

        os.system('rm /tmp/pdfcaca1-%s.pdf' % quote.pk)
        r = open('/tmp/pdfcaca12-%s.pdf' % quote.pk, 'rb+')
        result2 = r.read()
        pdf_data = base64.b64encode(result2)
        out = dict(message="ok", errors=False, result=str(pdf_data))
        return Response(out, 200)
        """r.close()
        # testing end 

        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = 'inline; filename=list_people.pdf'
        response['Content-Transfer-Encoding'] = 'binary'
        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result2)
            output.flush()
            output = open(output.name, 'rb')
            response.write(output.read())
        # testing
        os.system('rm /tmp/pdfcaca12-%s.pdf' % quote.pk)
        return response"""


