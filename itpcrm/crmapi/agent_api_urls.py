from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from crmapi import agent_views as views

urlpatterns = [
    # url(r'^accounts', views.API_Accounts.as_view()),

    # DEVICES URL
    url(r'^password', views.API_AgentPassword.as_view()),
    url(r'^profile', views.API_AgentProfile.as_view()),
    url(r'^activate/(?P<pk>[0-9]+)', views.API_AgentActivate.as_view()),
    url(r'^deactivate/(?P<pk>[0-9]+)', views.API_AgentDeactivate.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/item-group', views.API_NewQuoteItemGroup.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/pdf/render', views.API_PDFRender.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/quote-item/(?P<oid>[0-9]+)/price', views.API_UpdateQuoteItemPrice.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/quote-item/(?P<oid>[0-9]+)/description', views.API_UpdateQuoteItemDescription.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/quote-item/(?P<oid>[0-9]+)/metadata', views.API_UpdateQuoteItemMetadata.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/(?P<contact_id>[0-9]+)/submit', views.API_QuotesSubmit.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)/(?P<pid>[0-9]+)', views.API_AddItemToQuote.as_view()),
    url(r'^quotes/(?P<pk>[0-9]+)', views.API_GetQuoteByID.as_view()),
    url(r'^quotes', views.API_Quote.as_view()),
    url(r'^orders/(?P<pk>[0-9]+)', views.API_GetOrdersByID.as_view()),
    url(r'^orders', views.API_Orders.as_view()),
    url(r'^service-address/(?P<pk>[0-9]+)', views.ServicesByServiceAddressID.as_view()),
    url(r'^service-address', views.API_ServiceAddress.as_view()),
    url(r'^accounts/(?P<pk>[0-9]+)/services/$', views.API_GetAccountServices.as_view()),
    #url(r'^accounts/(?P<pk>[0-9]+)/contacts/$', views.API_GetAgentAccountContacts.as_view()),
    url(r'^accounts/(?P<pk>[0-9]+)/contacts$', views.API_GetAgentAccountContacts.as_view()),
    url(r'^accounts/(?P<pk>[0-9]+)$', views.API_GetAgentAccountByID.as_view()),
    url(r'^accounts/$', views.API_GetAgentAccounts.as_view()),
    #url(r'^account/(?P<pk>[0-9]+)$', views.API_GetAgentAccountContacts.as_view()),
    url(r'^account/$', views.API_GetAgentAccounts.as_view()),
    #url(r'^contacts/$', views.API_GetAgentAccountContacts.as_view()),
    url(r'^agents', views.API_Agents.as_view()),
    url(r'^products/(?P<pk>[0-9]+)', views.API_ProductsByID.as_view()),
    url(r'^products', views.API_Products.as_view()),

    #url(r'^services/itpvoice/devices', views.API_Services.as_view()),
    #url(r'^accounts/(?P<pk>[0-9]+)/$', views.snippet_detail),

    # SERVICES URL
    # url(r'^services', views.API_Services.as_view()),
]


