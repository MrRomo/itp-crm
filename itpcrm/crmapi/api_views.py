# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import requests
import xmltodict
from datetime import datetime
from django.shortcuts import render
from crm.models import (
    Accounts,
    Contacts,
    Orders,
    AccountServices,
    OrderItems,
    OrderItemGroup,
    Products,
    QuoteApprovals,
    QUOTE_STATUSES,
    AccountServices,
    ProductKeyNames,
    Quotes,
    QuoteItems,
)
from crmapi.serializers import (
    AccountsSerializer,
    AccountServicesSerializer,
    OrdersSerializer,
    OrderItemsSerializer,
)
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from django.db import connection
from django.conf import settings
from crmapi.crm_utils.loader import ItemTypeLoader

#from crmapi.auth import do_auth


def do_auth(api_key):
    # do we need a comment here? 
    if api_key == settings.VOICE_API_KEY:
        return True
    return False


def get_account_id(uid):
    try:
        return Contacts.objects.get(auth_api_id=uid)
    except Contacts.DoesNotExist:
        return None


class API_DocusignCallbacks(APIView):
    def get(self, request):
        print ("GET request callback")
        print (request)
        print (dir(request))
        return Response({"result": "ok"})

    def post(self, request):
        print ("POST request callback")
        #print request.body
        #data = json.loads(request.body)
        #print dir(request)
        #print request.data
        o = open('/tmp/caca1.txt', 'wb')
        o.write(request.body)
        o.close()

        x = json.loads(request.body)
        print (x)
        event_type = x.get('event_type')
        dochash = x.get('meta').get('related_document_hash')
        if event_type == 'document_signed':
            try:
                qa = QuoteApprovals.objects.get(docusign_envelope_id=dochash)
                #qa.add_a_note("Completed Quote Signature envid %s" % envelope_id)
                quote = qa.quote
                quote.quote_status = 'Customer Approved'
                #qa.add_a_note("Set quote status Customer Approved")
                quote.customer_signed = True
                quote.customer_signed_ip = 'x'
                #qa.add_a_note("Customer signature IP %s" % recipient_ip)
                quote.save()
            except QuoteApprovals.DoesNotExist:
                return Response({"result": "ok", "message": "not updated, but thnx anyway"})

        #print str(request.body)
        #x = xmltodict.parse(request.data)
        #print x
        #print request
        return Response({"result": "ok"})


class API_BandwidthCallbackDIDLookup(APIView):
    # TODO - DEPRECATE IC-258
    def get(self, request, did, format=None):
        f = {"did": did}
        o = OrderItems.objects.filter(metadata__contains=f)
        if not o:
            return Response({"results": []}, status=404)
        oser = OrderItemsSerializer(o, many=True)
        return Response({"results": oser.data, "errors": False}, status=200)



class API_BandwidthOrderSearch(APIView):
    """
    Returns the orderitems object
    # TODO - DEPRECATE IC-258
    """
    def get(self, request, oid, format=None):
        f = {'bandwidth_order_id': oid}
        o = OrderItems.objects.filter(metadata__contains=f)
        if not o:
            return Response({'results': []}, status=404)

        oser = OrderItemsSerializer(o, many=True)
        return Response({"results": oser.data, "errors": False}, status=200)



class API_Orders(APIView):
    """To do clean up from the provisioner_views, shits confugsing"""
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)

        if request.GET and not request.GET.get('pending-provision'):
            filters = {}
            for k, v in request.GET.items():
                if not k == 'Authorization':
                    filters[k] = v
            if not filters:
                orders = Orders.objects.all()
            #orders = Orders.objects.filter(**filters)
            else:
                orders = Orders.objects.filter(**filters)

            ser = OrdersSerializer(orders, many=True)
            return Response(ser.data, 200)

        elif request.GET.get('pending-provision'):
            order_filters = {'status': 'quote'}
            if request.GET.get('order-id'):
                order_filters['pk'] = request.GET.get('order-id')
            orders = Orders.objects.filter(**order_filters)
            orders_response = []
            for o in orders:
                oss = OrdersSerializer(o, many=False)
                for ig in o.orderitemgroup_set.select_related():
                    # each oi
                    order_items = []
                    for oi in ig.items:
                        if oi.auto_provision and oi.provision_status == 'pending-provision':
                            # hell yeah
                            ois = OrderItemsSerializer(oi, many=False)
                            order_items.append(ois.data)
                    d = dict(order=oss.data, orderitems=order_items)
                    orders_response.append(d)
            return Response(orders_response, 200)

        else:
            orders = Orders.objects.all()
            ser = OrdersSerializer(orders, many=True)
            return Response(ser.data, 200)

    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        data = json.loads(request.body)
        acctnum = data.get('accountnumber')
        account = Accounts.objects.get(accountnumber=acctnum)
        o = Orders(
                status='quote',
                account=Accounts.objects.get(accountnumber=acctnum),
                date_created=datetime.now(),
                deleted=False
        )
        o.save()
        og = OrderItemGroup(name='from portal', order=o)
        og.save()
        return Response({"result": "ok",
                        "errors": False,
                        "message": "Order Created",
                        "order_number": o.order_number}, status=200)

    def delete(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        data = json.loads(request.body)
        ordernum = data.get('order_number')
        o = Orders.objects.get(order_number=ordernum)
        for i in o.orderitemgroup_set.select_related():
            i.delete()
        o.delete()
        return Response({"result": "ok",
                        "errors": False,
                        "message": "Order Deleted"}, 200)


class API_OrderItems(APIView):
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        f = {}
        metadata_filters = {}
        #if request.args.get('md_bandwidth_order_id'):
        for k, v in request.GET.items():
            if 'md_' in k:
                metadata_filters[k.replace('md_','')] = v
            else:
                if k == 'order':
                    f['order'] = Orders.objects.get(pk=v)
                else:
                    f[k] = v
        if metadata_filters:
            f['metadata__contains'] = metadata_filters

        o = OrderItems.objects.filter(**f)
        if not o:
            return Response({'results': []}, status=404)

        oser = OrderItemsSerializer(o, many=True)
        return Response({"results": oser.data, "errors": False}, status=200)


    def post(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        data = json.loads(request.body)
        acctnum = data.get('accountnumber')
        order_id = data.get('order_id')
        pid = data.get('product_id')
        purchase_type = data.get('purchase_type', 'x')
        metadata = data.get('metadata')
        item_type = data.get('product_part_number')
        if data.get('item_type'):
            item_type = data.get('item_type')

        if pid:
            prod = Products.objects.get(pk=pid)
        else:
            pkey = ProductKeyNames.objects.get(name=item_type)
            prod = Products.objects.filter(product_key=pkey)
            if prod:
                prod = prod[0]
                    
        api_id = data.get('api_id')
        account = Accounts.objects.get(accountnumber=acctnum)
        serv = None
        if purchase_type == 'upgrade':
            serv = AccountServices.objects.get(api_id=api_id)
            if not serv.account != account:
                serv = None

        if order_id:
            o = Orders.objects.get(
                account=account,
                pk=order_id)
        else:
            # assume its from order#
            o = Orders.objects.get(
                account=account,
                order_number=data.get('order_number'))

        if data.get('auto_provision'):
            provision_status = 'pending-provision'
        else:
            provision_status = 'unprovisioned'

        og = OrderItemGroup.objects.filter(order=o)[0]
        oi = OrderItems(
            order=o,
            item_group=og,
            product=prod,
            purchase_type=purchase_type,
            item_type=prod.item_type,
            price=prod.price,
            prov_status=False,
            auto_provision=data.get('auto_provision'),
            metadata=metadata,
            provision_status=provision_status)
        if serv:
            oi.upgrade_service = serv
        oi.save()
        return Response({"result": "ok",
                        "errors": False,
                        "message": "Order Item Created"}, status=200)

    def patch(self, request, format=None):
        """
        Patch just sendso rder to complete for now
        """
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        data = json.loads(request.body)
        print ("PATCH22 order item %s" % data)
        oi_id = data.get('orderitem_id')

        # get the orderid
        o = OrderItems.objects.get(pk=oi_id)

        if data.get('completed'):
            o.completed=True
        print ("o = %s" % o)
        for k, v in data.items():
            print ("setting attr to %s=%s" % (k, v))
            if k == 'metadata':
                om = o.metadata
                for kk, vv in v.items():
                    # print ("KK = %s" % kk)
                    # print ("VV = %s" % vv)
                    om[kk] = vv
                setattr(o, 'metadata', om)
            else:
                setattr(o, k, v)
        o.save()
        return Response({"result": "ok",
                         "errors": False,
                         "message": "Order Item Updated"}, 200)



class API_Services(APIView):
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)

        billing_api_id = request.GET.get('billing_api_id')
        if not billing_api_id:
            acc = get_account_id(uid)
            account = acc.account
            acc_id = acc.account.pk
            auth_api_id = acc.auth_api_id
        else:
            try:
                acc = Accounts.objects.get(billing_api_id=billing_api_id)
            except Accounts.DoesNotExist:
                return Response({"result": [], 
                                "message": "No matching account",
                                "errors": True}, status=404)
            account = acc
            acc_id = acc.pk

        results = []
        for a in AccountServices.objects.filter(account=account):
            aser = AccountServicesSerializer(a, many=False)
            results.append(aser.data)

        return Response({'result': results, 'billing_api_id': billing_api_id}, status=200)


class API_ServiceAddons(APIView):
    def get_service_addon_details(self, api_id, item_type):
        try:
            acctserv = AccountServices.objects.get(api_id=api_id)
        except AccountServices.DoesNotExist:
            return Response({'results': "Invalid API ID"},
                            status=404)

        try:
            pkey = ProductKeyNames.objects.get(name=item_type)
        except ProductKeyNames.DoesNotExist:
            return Response({'results': "Invalid Item Type Name"},
                            status=404)

        try:
            product = Products.objects.get(product_key=pkey)
        except Products.DoesNotexist:
            return Response(dict(results='Invalid Product Key / Product not found'), status=404)
        return acctserv, pkey, product


    def get(self, request, format=None):
        # GET service addon
        pass

    def post(self, request, format=None):
        # new ServiceAddon
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)

        # print(request.body)
        data = json.loads(request.body)
        api_id = data.get('api_id')
        item_type = data.get('item_type')

        acctserv, pkey, product = self.get_service_addon_details(
                api_id, item_type)
 
        p = ItemTypeLoader(item_type=item_type,
                           parent_account_service=acctserv,
                           api_id=api_id,
                           product=product,
                           request_data=data)
        resp = p.create_order(purchase_type='service-addon')
        return Response(resp, status=resp.get('code', 200))

    def delete(self, request, format=None):
        # This endpoint is to create disconnect orders for specific item types
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)

        data = json.loads(request.body)
        api_id = data.get('api_id')
        item_type = data.get('item_type')

        acctserv, pkey, product = self.get_service_addon_details(
                api_id, item_type)

        p = ItemTypeLoader(item_type=item_type,
                           parent_account_service=acctserv,
                           api_id=api_id,
                           product=product,
                           request_data=data)
        resp = p.create_order(purchase_type='disconnect')
        return Response(resp, status=resp.get('code', 200))



class API_ServiceByApiId(APIView):
    def get(self, request, api_id):
        token = request.META.get('HTTP_AUTHORIZATION', 'x')
        uid = do_auth(token)
        if not uid:
            return Response({'results': "Unauthorized"},
                            status=401)
        try:
            serv = AccountServices.objects.get(api_id=api_id)
            serv_ser = AccountServicesSerializer(serv, many=False)
        except AccountServices.DoesNotExist:
            serv = None
            serv_ser = None
        if serv_ser:
            return Response({'result': serv_ser.data,
                             'api_id': api_id,
                             'errors': False}, status=200)
        return Response({"result": {},
                         "api_id": api_id,
                         "errors": True}, status=404)


        # todo - pass did type (toll free. etc)


