# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import (
    Accounts,
    Contacts,
    Products,
    ProductPackages,
    Orders,
    Quotes
)



@login_required
def login_success(request):
    # set any pre-stuff for post login
    if request.user.is_staff:
        return HttpResponseRedirect(reverse_lazy('accounts_list'))
    if request.user.groups.filter(name='Manager').exists():
        return HttpResponseRedirect(reverse_lazy('manager_approval_quote'))
    #if request.user.groups.filter(name='Customer').exists():
    #    return HttpResponseRedirect(reverse_lazy('customer_quote_approval'))
    return HttpResponse()
