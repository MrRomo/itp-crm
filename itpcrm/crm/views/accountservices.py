# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator # , user_passes_test
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, AccountServices, Contacts, Products, ProductPackages

logger = logging.getLogger()



@method_decorator(staff_member_required, name="dispatch")
class AccountServicesDetail(DetailView):
    model = AccountServices
    def get_context_data(self, **kwargs):
        context = super(AccountServicesDetail, self).get_context_data(**kwargs)
        obj = context.get('object')   
        item_type = obj.product.product_key.name

        adr = obj.service_address
        map_markers = []
        if adr:
            if adr.geo_data:
                lat = adr.lat
                lon = adr.lon
                map_markers.append(
                    "L.marker([%s, %s]).addTo(map);" % (lat, lon))
        
        #phone numbers
        phone_numbers = []
        if item_type == 'itp_voice':
            for serv_addon in obj.get_service_addons():
                if 'did' in serv_addon.product.product_key.name:
                    phone_numbers.append(serv_addon)
        
        all_other_addons = []
        for serv_addon in obj.get_service_addons():
            if not 'did' in serv_addon.product.product_key.name:
                all_other_addons.append(serv_addon)

        context['item_type'] = item_type
        context['geo_markers'] = map_markers
        context['phone_numbers'] = phone_numbers
        context['all_other_addons'] = all_other_addons
        return context 



@method_decorator(staff_member_required, name='dispatch')
class AccountServicesCreate(CreateView):
    model = AccountServices
    success_url = reverse_lazy('accountserviceslist')
    fields = [
        #'account',
        'product',
        'service_address',
        #'item_type',
        'price_per_unit',
        'price',
        'bill_interval',
        #'next_bill',
        'metadata',
        #'payment_complete',
        'auto_provision',
        'quantity',
        #'req_activation_date',
        #'activation_date',
        #'disconnect_date',
        'prod_description',
        'contract_terms',
        'api_id',
    ]
    def get_form(self, *args, **kwargs):
        form = super(AccountServicesCreate, self).get_form(*args, **kwargs)
        form.fields['product'].queryset = Products.objects.filter(product_type='Service')
        return form
    
    def get_context_data(self, **kwargs):
        context = super(AccountServicesCreate, self).get_context_data(**kwargs)
        #context['google_api_key'] = settings.GOOGLE_API_KEY
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        # custom shit
        logger.debug("AccountServicesCreate - form_valid()")
        logger.debug("- POST - {0}".format(self.request.POST))
        a = Accounts.objects.get(pk=self.request.POST.get('account'))
        self.object.account = a
        am = self.object.metadata
        for k, v in self.request.POST.items():
            print("req post {0}".format(k))
            if 'md__' in k:
                am[k.replace('md__', '')] = v
        self.object.metadata = am
        self.object.save()
        
        return HttpResponseRedirect('{0}?pk={1}'.format(
            reverse('accountservice_detail', kwargs={'pk': self.object.pk}),
            self.object.pk
        ))

