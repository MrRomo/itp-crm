# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, Contacts, Products, ProductPackages, Orders


@login_required
def claim_order(request):
    if not request.user.is_staff:
        return Http404
    oid = request.GET.get('order_id')
    o = Orders.objects.get(pk=oid)
    o.assigned_user = request.user
    o.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



# ORDERS/QUOTES PACKAGES
@method_decorator(staff_member_required, name='dispatch')
class OrdersList(ListView):
    model = Orders
    paginate_by = 100
    ordering = ['-id']

    def get_queryset(self):
        qs = super(OrdersList, self).get_queryset()
        ob = self.request.GET.get('order_by', None)
        if ob:
            return qs.order_by(ob)
        return qs

    def get_context_data(self, **kwargs):
        context = super(OrdersList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context



@method_decorator(staff_member_required, name='dispatch')
class OrdersCreate(CreateView):
    model = Orders
    success_url = reverse_lazy('orders_list')
    fields = [
        'account',
        'status',
        'date_created',
		
    ]
    def get_context_data(self, **kwargs):
        context = super(OrdersCreate, self).get_context_data(**kwargs)
        acc = Accounts.objects.all().order_by('name')
        context['accounts'] = acc
        return context


@method_decorator(staff_member_required, name='dispatch')
class OrdersUpdate(UpdateView):
    model = Orders
    success_url = reverse_lazy('orders_list')
    fields = [
        'account',
        'status',
        'date_created',
        'assigned_user',
    ]
    def get_context_data(self, **kwargs):
        context = super(OrdersUpdate, self).get_context_data(**kwargs)
        return context


@method_decorator(staff_member_required, name="dispatch")
class OrdersDetail(DetailView):
    model = Orders
    def get_context_data(self, **kwargs):
        context = super(OrdersDetail, self).get_context_data(**kwargs)
        context['extra'] = 'here'
        return context


@method_decorator(staff_member_required, name='dispatch')
class OrdersDelete(DeleteView):
    model = Orders
    success_url = reverse_lazy('orders_list')
    def get_context_data(self, **kwargs):
        context = super(OrdersDelete, self).get_context_data(**kwargs)
        if self.request.GET.get('godelete', 'n') == 'y':
            try:
                p = Orders.objects.get(pk=context.get('object').pk)
                p.deleted = True
                p.save()
                context['deleted_object'] = True
            except Orders.DoesNotExist:
                p = None
        return context
