from django.views.generic import View
from django.shortcuts import render
from django.http import (
    HttpResponse,
    JsonResponse,
    HttpResponseRedirect
)
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.utils import timezone
#from models import *
#from crm.pdfrender import Render
# other way
#from easy_pdf.views import PDFTemplateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.template.loader import render_to_string
from decimal import Decimal
from django.views.generic import TemplateView,ListView
from weasyprint import HTML
import tempfile
import pypdftk
import os
import subprocess
from crm.models import Quotes, PDFHash, AccountServices, QuoteItems, ServiceAddons



allowed_ips = [
        '192.112.255.44',
        '192.112.255.145',
        '1278.0.0.1',
        '98.254.99.36',]

def print_quote_html(request, pk):
    #if not request.META.get('REMOTE_ADDR') in allowed_ips:
    #    raise Exception("Not allowed man, chill out")
    quote = Quotes.objects.get(pk=int(pk))
    return render(request, 'crm/email_templates/send-quote2.html', {
        'quote': quote}, RequestContext(request))


def gen_pdf(request, pk):
    #pdf_hash = PDFHash.objects.get(pk=request.GET.get('k', None))
    #print "top wtf"
    quote = Quotes.objects.get(pk=pk)
    sub_total = Decimal('0.0')
    tax_sum = Decimal('0.0')
    tax_totals = Decimal('0.0')
    mrc_subtotal = Decimal('0.0')
    nrc_subtotal = Decimal('0.0')

    excluded_acctserv = []
    for items in QuoteItems.objects.filter(quote=quote):
        print(quote.get_quote_items())
        if items.product.product_type == 'Product':
            nrc_subtotal += items.item_price_total
            if items.taxable:
                tax_sum += items.item_price_total + items.tax_percent * items.item_price_total / 100
                tax_totals += items.tax_percent * items.item_price_total / 100
        else:
            mrc_subtotal += items.item_price_total
            if items.upgrade_service and not items.product.product_type == 'ServiceAddon':
                excluded_acctserv.append(items.upgrade_service)

    for a in AccountServices.objects.filter(account=quote.account):
        if not a in excluded_acctserv:
            mrc_subtotal += a.price
        for sa in ServiceAddons.objects.filter(service=a):
            mrc_subtotal += sa.price

    acctserv = AccountServices.objects.filter(account=quote.account)
    valid_until = quote.date_created + timedelta(days=30)
    quote_service_addons = []

    if acctserv:
        quote_item_services = QuoteItems.objects.filter(quote=quote)
        quote_service_addons = []
        for qq in quote_item_services:
            if qq.product.product_type == 'ServiceAddon':
                quote_service_addons.append(qq)

    if request.GET.get('render_as', 'x') == 'html':
        return render(request, 'crm/email_templates/send-quote.html', {
            'quote': quote,
            'account_services': acctserv,
            'sub_total': sub_total,
            'valid_until': valid_until,
            'nrc_subtotal': nrc_subtotal,
            'tax_sum': Decimal('%.2f' % tax_sum),
            'tax_totals': Decimal('%.2f' % tax_totals),
            'quote_service_addons': quote_service_addons,
            'mrc_subtotal': mrc_subtotal}, RequestContext(request))

    html_string = render_to_string('crm/email_templates/send-quote.html', {
        'quote': quote,
        'account_services': acctserv,
        'sub_total': sub_total,
        'valid_until': valid_until,
        'nrc_subtotal': nrc_subtotal,
        'mrc_subtotal': mrc_subtotal,
            'tax_sum': Decimal('%.2f' % tax_sum),
            'tax_totals': Decimal('%.2f' % tax_totals),
        'quote_service_addons': quote_service_addons,
        })
    html_string_sig = render_to_string('crm/email_templates/send-quote-sig.html', {
        'quote': quote,
        'account_services': acctserv,
        'sub_total': sub_total,
        'valid_until': valid_until,
        'nrc_subtotal': nrc_subtotal,
            'tax_sum': Decimal('%.2f' % tax_sum),
            'tax_totals': Decimal('%.2f' % tax_totals),
        'mrc_subtotal': mrc_subtotal,
        'quote_service_addons': quote_service_addons,
        })

    #print "doc pages"
    #print document.pages()
    #print dir(document)
    #print "RESULT"

    send_pages = []
    html = HTML(string=html_string)
    document = html.render()
    #print dir(document.pages[0])
    #print document.pages[0].__dict__
    for pg in document.pages:
        send_pages.append(pg)

    html2 = HTML(string=html_string_sig)
    document2 = html2.render()
    #for pg in document2.pages:
    send_pages.append(document2.pages[0])
    #send_pages.append(pg)

    #print result
    #print result
    # FINALLy write the pdf
    newdoc = document.copy(send_pages)
    result = newdoc.write_pdf()

    # just testing here...
    o = open('/tmp/pdfcaca-%s.pdf' % quote.pk, 'wb+')
    o.write(result)
    o.close()
    #try:
    #    pypdftk.fill_form('/tmp/pdfcaca-%s.pdf' % quote.pk,
    #                        out_file='/tmp/pdfcaca2-%s.pdf' % quote.pk, flatten=True)
    #except Exception, e:
    #    print "Exception flattening: %s" % str(e)
    #    pass
    cmd = "gs -dSAFER -dBATCH -dNOPAUSE -dNOCACHE -sDEVICE=pdfwrite "
    cmd += "-sColorConversionStrategy=/LeaveColorUnchanged  -dAutoFilterColorImages=true "
    cmd += "-dAutoFilterGrayImages=true -dDownsampleMonoImages=true -dDownsampleGrayImages=true "
    cmd += "-dDownsampleColorImages=true -sOutputFile=/tmp/pdfcaca2-%s.pdf /tmp/pdfcaca-%s.pdf" % (quote.pk, quote.pk)

    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    p.wait()
    rout, rerr = p.communicate()
    print ("%s - %s" % (rout, rerr))

    os.system('rm /tmp/pdfcaca-%s.pdf' % quote.pk)
    r = open('/tmp/pdfcaca2-%s.pdf' % quote.pk, 'rb+')
    result2 = r.read()
    r.close()
    # testing end 

    response = HttpResponse(content_type='application/pdf;')
    response['Content-Disposition'] = 'inline; filename=list_people.pdf'
    response['Content-Transfer-Encoding'] = 'binary'
    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result2)
        output.flush()
        output = open(output.name, 'rb')
        response.write(output.read())
    # testing
    os.system('rm /tmp/pdfcaca2-%s.pdf' % quote.pk)
    return response

