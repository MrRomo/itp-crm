# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator # , user_passes_test
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType

# VIEW IMPORTS
from crm.views.accounts import (
    AccountsList,
    AccountsCreate,
    AccountsUpdate,
    AccountsDelete,
    AccountsDetail,
)
from crm.views.accountservices import (
    AccountServicesDetail,
    AccountServicesCreate,
)
from crm.views.contacts import (
    ContactsList,
    ContactsCreate,
    ContactsUpdate,
    ContactsDelete,
    ContactsDetail,
)
from crm.views.products import (
    ProductsList,
    ProductsCreate,
    ProductsUpdate,
    ProductsDetail,
    ProductsDelete,
)
from crm.views.product_packages import (
    ProductPackagesList,
    ProductPackagesCreate,
    ProductPackagesUpdate,
    ProductPackagesDetail,
    ProductPackagesDelete,
)
from crm.views.orders import (
    claim_order,
    OrdersList,
    OrdersCreate,
    OrdersUpdate,
    OrdersDetail,
    OrdersDelete,
)
from crm.views.orderitems import (
    orderitem_provision,
    orderitem_mark_complete,
    orderitem_fiber_mark_complete,
    orderitem_manual_approve,
    OrderItemsList,
    OrderItemsCreate,
    OrderItemsUpdate,
    OrderItemsDetail,
    OrderItemsDelete,
)
from crm.views.quotes import (
    QuotesList,
    QuotesDraftList,
    QuotesPendingList,
    QuotesCreate,
    QuotesUpdate,
    QuotesDetail,
    QuotesDelete,
    manager_approval,
    manager_view_order,
    manager_approve_the_quote,
    manager_decline_the_quote,
    customer_approve_quote,
)
from crm.views.wizards import (
    gen_html_form,
    FIELDS_PER_PARTNUM,
    ajax_autocomplete_accounts,
    ajax_autocomplete_products,
    ajax_account_detail,
    ajax_city_by_state,
    ajax_products_by_id,
    wizard_add_itemgroup,
    wizard_autocomplete_serviceaddress,
    wizard_products_by_address,
    wizard_add_item_to_group,
    add_nrc_product,
    add_service,
    upgrade_service,
    downgrade_service,
    remove_service,
    remove_service_addon,
    wizard_del_item_from_group,
    wizard_change_item_price,
    wizard_change_item_quantity,
    wizard_change_item_description,
    wizard_quotes_step1,
    wizard_quotes_step2,
    wizard_quotes_step3,
    wizard_quotes_step3_readonly,
    wizard_addons_for_account_service,
    wizard_addons_for_quote_item,
    wizard_add_new_service,
    wizard_disconnect_service,
    wizard_upgrade_service,
    wizard_downgrade_service,
    wizard_add_new_service_addon,
    wizard_add_new_nrc_product,
    wizard_add_new_service_addon_qi,
    add_service_addon,
    wizard_quotes_submit,
    wizard_quotes_convert_to_order,
)
from crm.views.service_address import (
    service_address_search,
    service_address_formalize,
    ServiceAddressList,
    ServiceAddressCreate,
    ServiceAddressUpdate,
    ServiceAddressDetail,
    ServiceAddressDelete,
)
from crm.views.auth import login_success
from crm.views.pdf_views import (
    print_quote_html,
    gen_pdf,
)
from crm.views.service_addons import (
    ServiceAddonsDetail,
    ServiceAddonsCreate,
)
from crm.views.admin_international_rates import (
    international_rates
)

# END VIEW IMPORTS


from django.contrib.auth.decorators import login_required
from crm.models import (
    Accounts,
    Quotes,
    Orders,
    Contacts,
    AccountServices,
    ServiceAddons,
)

# Create your views here.
@login_required
def index(request):
    #return HttpResponseRedirect(reverse_lazy('accounts_list'))
    return render(request, 'crm/testv2.html', {
        'data': 'test'
    }, RequestContext(request))


@login_required
def global_search(request):
    query = request.GET.get('search')
    search_query = query.lower()

    # accounts
    global_search = []
    accounts_search = {
        'cl': Accounts,
        'key': 'accounts',
        'filters': {
            'name__icontains': search_query,
            'phone__icontains': search_query.replace('(','').replace('-', ''),
            'website__icontains': search_query,
            'billing_address__icontains': search_query,
            'billing_city__icontains': search_query,
            'billing_zipcode__icontains': search_query,
            'accountnumber__icontains': search_query,
        }
    }
    quotes_search = {
        'cl': Quotes,
        'key': 'quotes',
        'filters': {
            'name__icontains': search_query,
            #'pk': search_query
        }
    }
    try:
        x = int(search_query)
        quotes_search['filters']['pk'] = int(search_query)
    except ValueError:
        pass
    orders_search = {
        'cl': Orders,
        'key': 'orders',
        'filters': {
            'order_number__icontains': search_query
        }
    }
    contacts_search = {
        'cl': Contacts,
        'key': 'contacts',
        'filters': {
            'firstname__icontains': search_query,
            'lastname__icontains': search_query,
            'email__icontains': search_query,
        }
    }
    acctserv_search = {
        'cl': AccountServices,
        'key': 'acctserv',
        'filters': {
            #'service_address__icontains': search_query,
            #'metadata__contains': {"did": search_query.replace('(','').replace('-', '')},
        }
    }
    serviceaddon_search = {
        'cl': ServiceAddons,
        'key': 'serviceaddon',
        'filters': {
            #'service_address__icontains': search_query,
            'metadata__contains': {"did": search_query.replace('(','').replace('-', '')},
        }
    }
    global_search.append(accounts_search)
    global_search.append(quotes_search)
    global_search.append(orders_search)
    global_search.append(contacts_search)
    global_search.append(acctserv_search)
    global_search.append(serviceaddon_search)


    results = {}
    for srch in global_search:
        results[srch.get('key')] = []
        for k, v in srch.get('filters').items():
            for res in  srch.get('cl').objects.filter(**{k: v}):
                if not res in results[srch.get('key')]:
                    results[srch.get('key')].append(res)

    return render(request, 'crm/global-search-results.html', {
        'search_results': results,
        'global_search': global_search,
    }, RequestContext(request))
    

    
