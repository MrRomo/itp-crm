# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.shortcuts import render
from django.http import (
    HttpResponse,
    JsonResponse,
    HttpResponseRedirect
)
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
# from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from django.forms.models import model_to_dict
from crm.models import (
    Accounts,
    Contacts,
    Products,
    ProductPackages,
    Quotes,
    QuoteItemGroup,
    QuoteItems,
    QUOTE_STATUSES,
    AccountServices,
    ServiceAddress,
    ITPServices_Addresses,
    ServiceAddons,
    QUANTIFIABLE_ITEMS,
    NON_QUANTIFIABLE_ITEMS,
    ProductKeyNames,
    ProductKeyServiceAddons,
    US_Cities,
    US_States,
    HIDE_QUANTITY,
)
from string import ascii_lowercase
from decimal import Decimal
from crmapi.serializers import (
    ServiceAddressSerializer,
    ProductsSerializer,
)


def gen_html_form(data):
    for k, v in data.items():
        pass

FIELDS_PER_PARTNUM = {
        "itpvoice_pbx_did": [
            'num_of_new_dids',
            'new_did_state'
            'new_did_ratecenter',
        ],
        "itpvoice_pbx_did_port": [
            "port_numbers"
        ],
}


# AJAX urls
@login_required
def ajax_products_by_id(request):
    if not request.user.is_staff:
        return JsonResponse({"result": "Unauthorized"}, safe=False)
    q = request.GET.get('q', None)
    if not q:
        return {}
    p = Products.objects.get(pk=int(q))
    pser = ProductsSerializer(p, many=False)
    return JsonResponse(pser.data, safe=False)

@login_required
def ajax_autocomplete_accounts(request):
    if not request.user.is_staff:
        return JsonResponse({"result": "Unauthorized"}, safe=False)
    q = request.GET.get('q', None)
    if not q:
        return {}
    srch = {'name__icontains': q}
    acc = Accounts.objects.filter(**srch).order_by('name')
    if not acc:
        return JsonResponse({})
    l = []
    for i in acc:
        l.append({'id': i.pk, 'name': i.name})
    return JsonResponse(l, safe=False)

@login_required
def ajax_autocomplete_products(request):
    if not request.user.is_staff:
        return {}
    q = request.GET.get('q', None)
    if not q:
        return {}
    srch = {'name__icontains': q}
    prod = Products.objects.filter(**srch).order_by('name')
    if not prod:
        return JsonResponse({})
    l = []
    for i in prod:
        l.append({
            'id': i.pk,
            'name': i.name,
            'price': i.price,
            'metadata': i.metadata,
        })
    return JsonResponse(l, safe=False)

@login_required
def ajax_account_detail(request):
    if not request.user.is_staff:
        return {}
    # get the account details to prepopulate the form
    q = request.GET.get('account_id', None)
    if not q:
        return JsonResponse({'result': None})
    try:
        a = Accounts.objects.get(pk=q)
        return JsonResponse({'result': model_to_dict(a)}, safe=False)
    except Accounts.DoesNotExist:
        return JsonResponse({'result': None})


@login_required
def ajax_city_by_state(request):
    if not request.user.is_staff:
        return {}
    # get the account details to prepopulate the form
    state = request.GET.get('state')
    us_state = US_States.objects.get(shortcode=str(state).upper())
    us_cities = US_Cities.objects.filter(us_state=us_state).order_by('name')
    print (us_cities)
    cities = []
    for i in us_cities:
        cities.append(i.name)
    return JsonResponse({"result": cities})


# ##### END AJAX URLS


@login_required
def wizard_add_itemgroup(request):
    if not request.user.is_staff:
        return {}
    # quote item group add
    if request.POST:
        q = Quotes.objects.get(pk=request.POST.get('quoteid'))
        qig = QuoteItemGroup(
                name=request.POST.get("new_itemgroup_name"),
                quote=q)
        qig.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    raise Http404


@login_required
def wizard_autocomplete_serviceaddress(request):
    if not request.user.is_staff:
        return {}
    q = request.GET.get('q', None)
    if not q:
        return {}
    srch = {'full_address__icontains': q}
    sa = ServiceAddress.objects.filter(**srch).order_by('full_address')
    ser = ServiceAddressSerializer(sa, many=True)
    return JsonResponse(ser.data, safe=False)


@login_required
def wizard_products_by_address(request):
    if not request.user.is_staff:
        return {}
    pk = request.GET.get('pk')
    servaddr = ServiceAddress.objects.get(pk=int(pk))

    l = ITPServices_Addresses.objects.filter(address=servaddr)
    p = []
    for i in l:
        p.append(i.product.pk)
    for i in Products.objects.filter(depends_on_service_address=False, product_type='Service'):
        if not i.pk in p:
            p.append(i.pk)
    # get the actual obj
    products = Products.objects.filter(id__in=p)
    prod = {}
    for i in products:
        if not i.product_type == 'ServiceAddon':
            if not str(i.category) in prod.keys():
                prod[i.category] = []
            ser = ProductsSerializer(i, many=False)
            prod[i.category].append(ser.data)
    for i in Products.objects.filter(product_type='Product'):
        if not str(i.category) in prod.keys():
            prod[i.category] = []
        ser = ProductsSerializer(i, many=False)
        prod[i.category].append(ser.data)
    #ser = ProductsSerializer(products, many=True)
    #servaddr = ServiceAddressSerializer(service_address, many=False)
    return JsonResponse(prod)


@login_required
def wizard_add_item_to_group(request, pk):
    """
    Add item to group
    -deprecate soon
    """
    if not request.user.is_staff:
        return {}
    #raise Exception(request.POST)
    pid = request.POST.get('add_product_id')
    qig = request.POST.get('quote_item_groups')
    repeat = request.POST.get('item_grp_qty', int(1))
    quantity = request.POST.get('item_quantity', int(1))
    service_address_id = request.POST.get('service_address_id')
    req_act_date = request.POST.get('req_activation_date')
    #purch_type = request.POST.get('')
    req_activation_date = None

    if str('-') in req_act_date:
        req_activation_date = datetime.strptime(req_act_date, '%Y-%m-%d')

    servaddr = None
    if service_address_id:
        servaddr = ServiceAddress.objects.get(pk=int(service_address_id))

    #ig = QuoteItemGroup.objects.get(pk=qig)
    product = Products.objects.get(pk=pid)
    quote = Quotes.objects.get(pk=pk)

    print (request.POST.__dict__.keys())
    print (request.POST)
    purchase_type = request.POST.get('quote_item_purchase_type')
    serv = None

    if purchase_type:
        serv = AccountServices.objects.get(pk=purchase_type,
                                           account=quote.account)

    purchase_type = "new-order"
    upgrade_service = None   # <-- thjis will bemoved to upgrade_service method 

    # legacy old shit might go away soon 
    if serv:
        purchase_type = "upgrade"
        upgrade_service = serv

    for n in range(0, int(repeat)):
        price = product.price
        quoteitem = QuoteItems(
            quote=quote,
            quantity=quantity,
            #item_group=ig,
            account=quote.account,
            product=product,
            price=price,
            purchase_type=purchase_type,
            upgrade_service=upgrade_service,
        )
        if req_activation_date:
            quoteitem.req_activation_date = req_activation_date

        if servaddr:
            quoteitem.service_address = servaddr

        quoteitem.save()
    return quoteitem


@login_required
def add_nrc_product(request):
    """
    ADD NEW SERVICE
    add a new service to quote as a quoite item.
    """
    data = request.POST
    product_id = data.get("product_selected")
    quantity = data.get('item_quantity')
    item_taxable = False
    if data.get('item_taxable'):
        item_taxable = True
    #service_address_id = data.get('service_address_id')
    req_act_date = data.get('req_activation_date')
    #  now suckit
    purchase_type = "nrc-product"
    product = Products.objects.get(pk=product_id)
    price = product.price
    quote = Quotes.objects.get(pk=data.get('quote_id'))
    repeat = int(1)
    req_activation_date = None

    if str('-') in req_act_date:
        req_activation_date = datetime.strptime(req_act_date, '%Y-%m-%d')
    

    for n in range(0, int(repeat)):
        price = product.price
        quoteitem = QuoteItems(
            quote=quote,
            quantity=quantity,
            #item_group=ig,
            account=quote.account,
            product=product,
            price=price,
            purchase_type=purchase_type,
            tax_percent=data.get('item_tax_percent'),
            taxable=item_taxable,
            #upgrade_service=upgrade_service,
        )
        if req_activation_date:
            quoteitem.req_activation_date = req_activation_date

        quoteitem.save()
    return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))



@login_required
def add_service(request):
    """
    ADD NEW SERVICE
    add a new service to quote as a quoite item.
    """
    data = request.POST
    product_id = data.get("add_product_id")
    quantity = data.get('item_quantity')
    service_address_id = data.get('service_address_id')
    req_act_date = data.get('req_activation_date')
    #  now suckit
    purchase_type = "new-order"
    product = Products.objects.get(pk=product_id)
    price = product.price
    quote = Quotes.objects.get(pk=data.get('add_service_quote_id'))
    repeat = int(1)
    req_activation_date = None
    partnum = product.part_number

    if str('-') in req_act_date:
        req_activation_date = datetime.strptime(req_act_date, '%Y-%m-%d')
    servaddr = None
    if service_address_id:
        servaddr = ServiceAddress.objects.get(pk=int(service_address_id))

    for n in range(0, int(repeat)):
        price = product.price
        # prartnumber overrides
        metadata = None
        if partnum == 'itp_voice':
            metadata = product.metadata.copy()
            if data.get('itp_voice_platform'):
                if data.get('itp_voice_platform') == 'legacy':
                    metadata['legacy'] = True

        quoteitem = QuoteItems(
            quote=quote,
            quantity=quantity,
            #item_group=ig,
            account=quote.account,
            product=product,
            price=price,
            purchase_type=purchase_type,
            contract_terms=data.get('contract_terms', None),
            #upgrade_service=upgrade_service,
        )
        if metadata:
            quoteitem.metadata = metadata

        if req_activation_date:
            quoteitem.req_activation_date = req_activation_date

        if servaddr:
            quoteitem.service_address = servaddr

        quoteitem.save()
        #if data.get('contract_terms'):
        #    quoteitem.metadata['contract_terms'] = data.get('contract_terms')
        #    quoteitem.save()
    return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))


def upgrade_service(request, purchase_type='upgrade'):
    """
    POST
    upgrade service
    """
    data = request.POST
    quote = Quotes.objects.get(pk=data.get('quote', None))
    acctserv = AccountServices.objects.get(pk=data.get('account_service', None))
    #if QuoteItems.objects.filter(quote=quote, purchase_type='upgrade', upgrade_service=acctserv):
    #    raise Exception("nah bro you need to relax man, seriously.")
    #if QuoteItems.objects.filter(quote=quote, purchase_type='downgrade', upgrade_service=acctserv):
    #    raise Exception("nah bro you need to relax man, seriously.")
    if 'itp_fiber' in acctserv.product.part_number:
        # itpfiber
        if_prod = data.get('itp_fiber__product2upgrade', None)
        if if_prod:
            p = Products.objects.get(pk=if_prod)
            quoteitem = QuoteItems(
                quote=quote,
                quantity=int(1),
                account=quote.account,
                product=p,
                price=p.price,
                price_per_unit=p.price,
                purchase_type='upgrade',
                upgrade_service=acctserv,
            )
            # if upgrade item is quantifiable, retain price from acctservice
            if acctserv and acctserv.product.part_number in QUANTIFIABLE_ITEMS:
                quoteitem.price = acctserv.price_per_unit
                quoteitem.price_per_unit = acctserv.price_per_unit
            quoteitem.save()
            return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))

    if acctserv.product.part_number == 'itp_voice': 
        if_prod = data.get('itp_voice__quantity', None)
        if if_prod:
            p = acctserv.product
            quoteitem = QuoteItems(
                    quote=quote,
                    quantity=if_prod,
                    account=quote.account,
                    product=p,
                    price=p.price,
                    purchase_type='upgrade',
                    upgrade_service=acctserv,
                    price_per_unit=p.price,
                    contract_terms=data.get('contract_terms', None),
            )
            quoteitem.metadata = acctserv.metadata
            if acctserv:
                quoteitem.price = acctserv.price_per_unit
                quoteitem.price_per_unit = acctserv.price_per_unit
            quoteitem.save()
            return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))


@login_required
def downgrade_service(request):
    data = request.POST
    quote = Quotes.objects.get(pk=data.get('quote', None))
    
    acctserv = AccountServices.objects.get(pk=data.get('account_service', None)) 
    #if QuoteItems.objects.filter(quote=quote, purchase_type='upgrade', upgrade_service=acctserv):
    #    raise Exception("nah bro you need to relax man, seriously.")
    #if QuoteItems.objects.filter(quote=quote, purchase_type='downgrade', upgrade_service=acctserv):
    #    raise Exception("nah bro you need to relax man, seriously.")
    if "itp_fiber" in acctserv.product.part_number :
        # itpfiber
        if_prod = data.get('itp_fiber__product2upgrade', None)
        if if_prod:
            p = Products.objects.get(pk=if_prod)
            quoteitem = QuoteItems(
                quote=quote,
                quantity=int(1),
                account=quote.account,
                product=p,
                price=p.price,
                purchase_type='downgrade',
                upgrade_service=acctserv,
            )
            if acctserv and acctserv.product.part_number in QUANTIFIABLE_ITEMS:
                quoteitem.price = acctserv.price_per_unit
                quoteitem.price_per_unit = acctserv.price_per_unit
            quoteitem.save()
            return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))
    
    if acctserv.product.part_number == 'itp_voice': 
        if_prod = data.get('itp_voice__quantity', None)
        if if_prod:
            p = acctserv.product
            quoteitem = QuoteItems(
                    quote=quote,
                    quantity=if_prod,
                    account=quote.account,
                    product=p,
                    price=p.price,
                    purchase_type='downgrade',
                    upgrade_service=acctserv,
            )
            quoteitem.metadata = acctserv.metadata
            quoteitem.save()
            return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))



@login_required
def remove_service(request):
    """
    REMOVE SERVICE
    remove a service to quote as a quoite item.

    TO DO implement some security here or no?
    """
    data = request.POST
    acct_serv_id = data.get('account_service')
    account_service = AccountServices.objects.get(pk=acct_serv_id)

    req_disco_date = request.POST.get('req_disconnect_date', None)
     
    #if str('-') in req_disco_date:
    #    req_disco_date = datetime.strptime(req_disco_date, '%Y-%m-%d')

    #  now suckit
    purchase_type = "disconnect"
    #product = Products.objects.get(pk=product_id)
    
    quote = Quotes.objects.get(pk=data.get('quote'))
    repeat = int(1)
    
    for n in range(0, int(repeat)):
        price = account_service.oi.price
        quoteitem = QuoteItems(
            quote=quote,
            quantity=int(1),
            #item_group=ig,
            account=quote.account,
            #product=product,
            price=price,
            purchase_type=purchase_type,
            upgrade_service=account_service,
        )
            
        if account_service.metadata:
            quoteitem.metadata = account_service.metadata
        else:
            quoteitem.metadata = {'purchase_type': 'disconnect'}
        quoteitem.save()
        if req_disco_date:
            qm = quoteitem.metadata
            quoteitem.metadata['req_disco_date'] = str(req_disco_date)
            quoteitem.save()

        for servaddon in account_service.get_service_addons():
            price = servaddon.price
            quoteitem_addon = QuoteItems(
                    quote=quote,
                    quantity=int(1),
                    #item_group=ig,
                    account=quote.account,
                    #product=product,
                    price=price,
                    purchase_type='disconnect',
                    #upgrade_service=self.upgrade_service,
                    upgrade_service_addon=servaddon,
            )
            quoteitem_addon.save()


    return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))


@login_required
def remove_service_addon(request):
    # same as remove service but only for addons, 
    # do not auto-disconnect any related addons logic
    pass


@login_required
def wizard_del_item_from_group(request):
    if not request.user.is_staff:
        return {}
    qid = request.GET.get('quoteitem_id')
    quoteitem = QuoteItems.objects.get(pk=qid)
    # delete the relaeted quote items
    for qq in QuoteItems.objects.filter(parent_quote_item=quoteitem):
        qq.delete()
    quote = quoteitem.quote
    quoteitem.delete()
    return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': quote.pk}))


@login_required
def wizard_change_item_price(request):
    if not request.user.is_staff:
        return {}
    iid = request.GET.get('id')
    price = request.GET.get('price')
    qi = QuoteItems.objects.get(pk=int(iid))
    prev_price = qi.price
    new_price = Decimal(price)
    qi.price = new_price
    qi.price_per_unit = new_price
    qi.item_price_total = qi.price * qi.quantity
    if prev_price != new_price:
        qi.price_changed = True
    qi.save()
    return HttpResponseRedirect(reverse_lazy(
        'wizard_quotes_step3', kwargs={'pk': qi.quote.pk}))

@login_required
def wizard_change_item_quantity(request):
    if not request.user.is_staff:
        return {}
    iid = request.GET.get('id')
    quantity = request.GET.get('quantity')
    qi = QuoteItems.objects.get(pk=int(iid))
    qi.quantity = quantity
    qi.save()
    return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': qi.quote.pk}))



@login_required
def wizard_change_item_description(request):
    if not request.user.is_staff:
        return {}
    data = request.GET
    if request.POST:
        data = request.POST
    iid = data.get('id')
    desc = data.get('description')
    qi = QuoteItems.objects.get(pk=int(iid))
    qi.prod_description = desc
    qi.save()
    return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': qi.quote.pk}))



@login_required
def wizard_quotes_step1(request):
    if not request.user.is_staff:
        return {}
    return render(
        request,
        'crm/wizard-quotes-1.html', {
    }, RequestContext(request))

@login_required
def wizard_quotes_step2(request, pk=None):
    if not request.user.is_staff:
        return {}
    # new quote for specific customer (pk)
    account = Accounts.objects.get(pk=pk)
    if request.POST:
        quotename = request.POST.get('quotename')
        if quotename:
            acc = Accounts.objects.get(pk=int(pk))
            q = Quotes(
                    name=quotename,
                    account=acc,
                    quote_status=QUOTE_STATUSES.get('DRAFT'),
                    customer_signed=False,
                    customer_signed_ip='n',
            )
            q.save()
            return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': q.pk}))
    return render(
        request,
        'crm/wizard-quotes-2.html', {
        'pk': pk,
        'account': account,
    }, RequestContext(request))


@login_required
def wizard_quotes_step3(request, pk):
    if not request.user.is_staff:
        return {}
    # new quote pk is quote id / edit quote page
    if request.POST:
        # do we still need this post shit here, I think i moved it to a form but 
        # i also forgot cause I never take notes or do anything right.
        # add_product_id, and "quote_item_groups" elements
        quoteitem = wizard_add_item_to_group(request, pk)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    # GET
    itemgroups = []
    q = Quotes.objects.get(pk=pk)
    edit_statuses = [QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED'), QUOTE_STATUSES.get('DRAFT')]

    if not q.quote_status in edit_statuses:
        #return HttpResponseRedirect("fkme")
        return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3_ro', kwargs={'pk': q.pk}))

    account = q.account
    contacts = Contacts.objects.filter(account=account, admin_group=True)
    # centralize
    if q:
        itemgroups = q.quoteitemgroup_set.select_related()

    quote_grand_total = q.get_quote_grand_total()

    prod = {}
    for i in Products.objects.all():
        if not i.product_type == 'ServiceAddon':
            if not str(i.category) in prod.keys():
                prod[i.category] = []
            prod[i.category].append(i)

    service_addons = Products.objects.filter(product_type="ServiceAddon")
    acct_service_addons = ServiceAddons.objects.filter(account=account).order_by('service')
    acct_services = AccountServices.objects.filter(account=account)
    # caesar is going to ask to filter out the quote item types. 
    quote_item_services = QuoteItems.objects.filter(quote=q)
    quote_items_addon = []
    for qq in quote_item_services:
        if qq.product.product_type == 'Service':
            quote_items_addon.append(qq)

    quote_service_addons = []
    tax_sum = Decimal('0.0')
    tax_totals = Decimal('0.0')
    nrc_sum = Decimal('0.0')
    for qq in quote_item_services:
        #print "PROD TYPE %s" % qq.product.product_type
        if qq.product.product_type == 'ServiceAddon':
            quote_service_addons.append(qq)

        if qq.product.product_type == 'Product':
            # print "PRODUCT"
            # print qq.taxable
            # print qq.pk
            # print qq.tax_percent
            # print qq.item_price_total
            if qq.taxable:
                print ("TAX")
                nrc_sum += qq.item_price_total
                tax_totals += qq.tax_percent * qq.item_price_total / 100
                tax_sum += qq.item_price_total + qq.tax_percent * qq.item_price_total / 100

    return render(
        request,
        'crm/wizard-quotes-3.html', {
        'quote_id': pk,
        'quote': q,
        'itemgroups': itemgroups,
        #'total': amt,
        'total': quote_grand_total,
        'debug_msg': request.GET.get('msg'),
        'account_services': acct_services,
        'products_list': prod,
        'contacts': contacts,
        'quote_items': quote_item_services,
        "service_addons": service_addons,
        "quote_items_addon": quote_items_addon,
        'quote_service_addons': quote_service_addons,
        "acct_service_addons": acct_service_addons,
        'hide_quantity': HIDE_QUANTITY,
        'nrc_sum': nrc_sum,
        #'tax_sum': tax_sum,
        'tax_sum': Decimal('%.2f' % tax_sum),
    }, RequestContext(request))


@login_required
def wizard_quotes_step3_readonly(request, pk):
    if not request.user.is_staff:
        return {}
    # new quote pk is quote id / edit quote page
    if request.POST:
        # do we still need this post shit here, I think i moved it to a form but 
        # i also forgot cause I never take notes or do anything right.
        # add_product_id, and "quote_item_groups" elements
        quoteitem = wizard_add_item_to_group(request, pk)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    # GET
    itemgroups = []
    q = Quotes.objects.get(pk=pk)

    account = q.account
    contacts = Contacts.objects.filter(account=account, admin_group=True)
    if q:
        itemgroups = q.quoteitemgroup_set.select_related()

    quote_grand_total = q.get_quote_grand_total()

    prod = {}
    for i in Products.objects.all():
        if not i.product_type == 'ServiceAddon':
            if not str(i.category) in prod.keys():
                prod[i.category] = []
            prod[i.category].append(i)

    service_addons = Products.objects.filter(product_type="ServiceAddon")
    acct_service_addons = ServiceAddons.objects.filter(account=account).order_by('service')
    acct_services = AccountServices.objects.filter(account=account)
    # caesar is going to ask to filter out the quote item types. 
    quote_item_services = QuoteItems.objects.filter(quote=q)
    quote_items_addon = []
    for qq in quote_item_services:
        if qq.product.product_type == 'Service':
            quote_items_addon.append(qq)

    quote_service_addons = []
    for qq in quote_item_services:
        if qq.product.product_type == 'ServiceAddon':
            quote_service_addons.append(qq)

    return render(
        request,
        'crm/wizard-quotes-3-readonly.html', {
        'quote_id': pk,
        'quote': q,
        'itemgroups': itemgroups,
        'total': quote_grand_total,
        'debug_msg': request.GET.get('msg'),
        'account_services': acct_services,
        'products_list': prod,
        'contacts': contacts,
        'quote_items': quote_item_services,
        "service_addons": service_addons,
        "quote_items_addon": quote_items_addon,
        'quote_service_addons': quote_service_addons,
        "acct_service_addons": acct_service_addons,
    }, RequestContext(request))


@login_required
def wizard_addons_for_account_service(request, pk, apk):
    """
    Account service detial page
    which shows the following actions
    upgrade / downgrade / add service addon. 
    """
    if not request.user.is_staff:
        return {}

    q = Quotes.objects.get(pk=pk)
    account = q.account

    ac = AccountServices.objects.get(pk=apk)
    sa = ServiceAddons.objects.filter(service=ac)

    upgrade_quote_items = QuoteItems.objects.filter(
        quote=q, upgrade_service=ac, purchase_type='upgrade'
    ).order_by('record_created')

    return render(
        request,
        'crm/wizard-quotes-acctservice.html', {
            'account_service': ac,
            'service_addons': sa,
            'account': account,
            'quote': q,
            'upgrade_quote_items': upgrade_quote_items,
    }, RequestContext(request))


@login_required
def wizard_addons_for_quote_item(request, pk, qipk):
    """
    This is the similar view to the account service detail
    where you can add addons / upgrade / downgrade
    except this is for quote items, not account services.
    (cannot upgrade/downgrade a temp pending new service that hasnt been completed)
    """
    if not request.user.is_staff:
        return {}

    q = Quotes.objects.get(pk=pk)
    account = q.account

    qi = QuoteItems.objects.get(pk=qipk)
    qi_addons = QuoteItems.objects.filter(parent_quote_item=qi)

    return render(
        request,
        'crm/wizard-quotes-item.html', {
            'qi': qi,
            'quoteitem_addons': qi_addons,
            'account': account,
            'quote': q,
    }, RequestContext(request))


@login_required
def wizard_add_new_service(request, pk):
    print ("FUCKKIIKGKGK")
    if not request.user.is_staff:
        return {}
    q = Quotes.objects.get(pk=pk)
    account = q.account
    return render(
        request,
        'crm/wizard-add-new-service.html', {
            'account': account,
            'quote': q,
    }, RequestContext(request))

@login_required
def wizard_disconnect_service(request, pk, apk):
    print ("disconnect view")
    if not request.user.is_staff:
        return {}
    q = Quotes.objects.get(pk=pk)
    account_service = AccountServices.objects.get(pk=apk)
    account = q.account
    return render(
        request,
        'crm/wizard-disconnect-service.html', {
            'account': account,
            'quote': q,
            'account_service': account_service,
    }, RequestContext(request))

@login_required
def wizard_upgrade_service(request, pk, apk):
    if not request.user.is_staff:
        return {}
    quote = Quotes.objects.get(pk=pk)
    account_service = AccountServices.objects.get(pk=apk)
    itp_fiber_products = []
    for p in Products.objects.filter(product_type="Service"):
        if 'itp_fiber' in p.item_type:
            itp_fiber_products.append(p)
    return render(
        request,
        'crm/wizard-upgrade-service.html', {
            'quote': quote,
            'account_service': account_service,
            'itp_fiber_products': itp_fiber_products,
            
    }, RequestContext(request))


@login_required
def wizard_downgrade_service(request, pk, apk):
    if not request.user.is_staff:
        return {}
    quote = Quotes.objects.get(pk=pk)
    account_service = AccountServices.objects.get(pk=apk)
    itp_fiber_products = []
    for p in Products.objects.filter(product_type="Service"):
        if 'itp_fiber' in p.part_number:
            itp_fiber_products.append(p)
    return render(
        request,
        'crm/wizard-downgrade-service.html', {
            'quote': quote,
            'account_service': account_service,
            'itp_fiber_products': itp_fiber_products,
    }, RequestContext(request))


@login_required
def wizard_add_new_service_addon(request, pk, apk):
    """
    View to show the add new service addon form""
    """
    quote = Quotes.objects.get(pk=pk)
    ac = AccountServices.objects.get(pk=apk)
    service_addons = ac.product.get_addons_for_product()
    did_cities = US_Cities.objects.filter()
    did_states = US_States.objects.filter().order_by('name')
    print("service addons {0}".format(service_addons))

    if not request.user.is_staff:
        return {}

    return render(
        request,
        'crm/wizard-new-service-addon.html', {
            'account_service': ac,
            'service_addons': service_addons,
            'account': quote.account,
            'quote': quote,
            #'did_cities': did_cities,
            'did_states': did_states,
            'FIELDS_PER_PARTNUM': FIELDS_PER_PARTNUM,
            'custom_edit_fields': FIELDS_PER_PARTNUM.keys(),
            'products': Products.objects.filter(product_type='ServiceAddon'),
    }, RequestContext(request))


@login_required
def wizard_add_new_nrc_product(request, pk):
    if not request.user.is_staff:
        return {}
    q = Quotes.objects.get(pk=pk)
    products = Products.objects.filter(product_type="Product")
    account = q.account
    p = None
    if request.GET.get('p'):
        p = Products.objects.get(pk=int(request.GET.get('p')))

    return render(
        request,
        'crm/wizard-add-new-nrc-product.html', {
            'account': account,
            'quote': q,
            'products': products,
            'product': p,
    }, RequestContext(request))


@login_required
def wizard_add_new_service_addon_qi(request, pk, apk):
    """
    View to show the add new service addon form
    FOR QOUTE ITEMS - not accountservice""
    """
    quote = Quotes.objects.get(pk=pk)
    qi = QuoteItems.objects.get(pk=apk)
    #ac = AccountServices.objects.get(pk=apk)
    #sa = Products.objects.filter(product_type='ServiceAddon')
    sa = []
    part_num = qi.product.product_key.name
    prodkey = ProductKeyNames.objects.get(name=part_num)
    for x in ProductKeyServiceAddons.objects.get(product_key=prodkey).addon_products.all():
        sa.append(x)
    did_cities = US_Cities.objects.filter()
    did_states = US_States.objects.filter().order_by('name')
    for x in ProductKeyServiceAddons.objects.get(product_key=prodkey).addon_products.all():
        if not x in sa:
            sa.append(x)

    if not request.user.is_staff:
        return {}
    return render(
        request,
        'crm/wizard-new-service-addon-qi.html', {
            #'account_service': ac,
            'quote_item': qi,
            'service_addons': sa,
            'account': quote.account,
            'quote': quote,
            'did_states': did_states,
            'FIELDS_PER_PARTNUM': FIELDS_PER_PARTNUM,
            'custom_edit_fields': FIELDS_PER_PARTNUM.keys(),
            'products': Products.objects.filter(product_type='ServiceAddon'),
    }, RequestContext(request))




@login_required
def add_service_addon(request):
    """
    POST - handle teh posties
    Add a new service addon
    """
    if not request.user.is_staff:
        return {}

    data = request.POST
    pk = data.get('new_service_addon_quoteid')
    purch_type = "service-addon"

    #raise Exception(data.get('purchase_type'))
    q = Quotes.objects.get(pk=int(data.get('quote_pk')))

    if data.get('action') == 'new-addon-service' or \
            data.get('action') == 'upgrade-service':

        # the quoteitem pk
        serv = data.get('service_for_addon')

        # if the upgrade service selected is a new quoteitem 
        # (not a completed account service)
        if "quoteitem_" in str(serv):
            try:
                serv = serv.replace('quoteitem_', '')
                acct_serv = None
                addon_parent = "quoteitem"
                parent_quote_item = QuoteItems.objects.get(pk=serv)
            except QuoteItems.DoesNotExist:
                raise Exception("Account Service does not exist")
        else:
            # is trying to match to an accountService, not quote item.
            try:
                parent_quote_item = None
                acct_serv = AccountServices.objects.get(pk=serv, account=q.account)
            except AccountServices.DoesNotExist:
                raise Exception("Account Service does not exist")

        # get the product
        prod = None
        try:
            k = data.get('new_service_addon_product', 'x')
            kk = k.split('##')[0]
            prod = Products.objects.get(pk=int(kk))
            if not prod.product_type == 'ServiceAddon':
                raise Exception("Product is not a service-addon")
        except (Products.DoesNotExist, ValueError):
            # check if its an account service or a NEW service that
            # this order item is being tied to
            raise Exception("Product / service-addon does not exist pk %s" % kk)

        # todo will need to change this logic by changing the numdid name from dom form
        price = prod.price
        pperunit = prod.price

        product_partnum = request.POST.get('selected_product_partnum')
        repeat_qi = int(1)

        if prod.product_type == 'ServiceAddon':
            # DIDzzz
            if product_partnum == 'itpvoice_pbx_did':
                did_action = request.POST.get('did_action', 'newnum')
                price_total = prod.price
                pperunit = prod.price

                if did_action == 'newnum':
                    quantity = int(request.POST.get('num_of_new_dids', int(1)))
                    #price_total = price * quantity
                    metadata = prod.metadata.copy()
                    metadata['city'] = request.POST.get('new_did_ratecenter')
                    metadata['state'] = request.POST.get('new_did_state')
                    metadata['api_id'] = acct_serv.api_id
                    # set fiulters to use for quoteitem creation
                    repeat_qi = int(1)
                    qi_filters = [{
                        'account': q.account,
                        'quote': q,
                        'upgrade_service': acct_serv,
                        'purchase_type': purch_type,
                        'product': prod,
                        'metadata': metadata,
                        'price': price_total,
                        'price_per_unit': pperunit,
                        'parent_quote_item': parent_quote_item,
                        'quantity': quantity}]

                if did_action == 'newnum_withnum':
                    dids = request.POST.get('newnum_withnum_dids')
                    quantity = 0
                    qi_filters = []
                    for d in dids.split('\n'):
                        if len(d) > 5:
                            quantity += 1
                            metadata = prod.metadata.copy()
                            metadata['did'] = d.replace('\r', '').replace(' ','').replace('-','')
                            metadata['api_id'] = acct_serv.api_id


                            qi_filters.append({
                                'account': q.account, 'quote': q, 'upgrade_service': acct_serv,
                                'purchase_type': purch_type, 'product': prod,
                                'metadata': metadata,
                                'price': price_total,
                                'price_per_unit': pperunit,
                                'parent_quote_item': parent_quote_item,
                                'quantity': int(1)})

            if product_partnum == 'itpvoice_pbx_did_port':
                #raise Exception(request.POST)
                numbers_to_port = str(request.POST.get('numbers_to_port')).split('\n')
                quantity = 0
                # just incase theres caca data
                for n in numbers_to_port:
                    if len(str(n)) > 5:
                        quantity += 1

                #price_total = price * quantity
                price_total = prod.price
                pperunit = prod.price

                # determine logic / custom data or metadata
                # for given products by prod.part_number
                repeat_qi = quantity
                qi_filters = []
                for n in range(0, quantity):
                    qm = prod.metadata.copy()
                    if not qm.get('did'):
                        qm['did'] = numbers_to_port[n].replace('\r', '')
                        qm['api_id'] = acct_serv.api_id
                    qi_filters.append({
                        'metadata': qm,
                        'account': q.account,
                        'quote': q,
                        'upgrade_service': acct_serv,
                        'purchase_type': purch_type,
                        'product': prod,
                        'metadata': qm,
                        'price': price_total,
                        'price_per_unit': pperunit,
                        'parent_quote_item': parent_quote_item,
                        'quantity': int(1)
                    })

            if product_partnum not in FIELDS_PER_PARTNUM.keys():
                quantity = request.POST.get('%s_universal_quantity' % product_partnum, int(1))
                qi_filters = []
                #qm = prod.metadata.copy()
                print (prod)
                qm = {}
                if type(prod.metadata) == str:
                    qm = json.loads(prod.metadata).copy()
                qm['api_id'] = acct_serv.api_id
                print (qm)
                price_total = prod.price
                qi_filters.append({
                        #'metadata': qm,
                        'account': q.account,
                        'quote': q,
                        'upgrade_service': acct_serv,
                        'purchase_type': purch_type,
                        'product': prod,
                        'metadata': qm,
                        'price': price_total,
                        'price_per_unit': price_total,
                        'parent_quote_item': parent_quote_item,
                        'quantity': quantity, 
                })

            for qf in qi_filters:
                qi = QuoteItems(**qf)
                qi.save()


        return HttpResponseRedirect(reverse_lazy(
                'wizard_quotes_step3', kwargs={'pk': q.pk}))



@login_required
def wizard_quotes_submit(request, pk):
    if not request.user.is_staff:
        return {}
    q = Quotes.objects.get(pk=int(pk))
    contact_id = request.GET.get('contact_id')
    if not contact_id:
        contact_id = request.POST.get('contact_id')

    contact = Contacts.objects.get(pk=contact_id)
    if not contact:
        return HttpResponseRedirect('%s?msg=No Contact was assigned - Please try again....' %
                request.META.get('HTTP_REFERER'))
    if not contact.account == q.account:
        return HttpResponseRedirect('%s?msg=Invalid Contact / Account Ownership....' %
                request.META.get('HTTP_REFERER'))

    q.quote_assignee = contact

    if q.quote_status == QUOTE_STATUSES.get('DRAFT'):
        q.quote_status = QUOTE_STATUSES.get('PENDING_CUSTOMER_PRE_APPROVAL')
        q.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponseRedirect('%s?msg=Quote submitted for Manager Approval!' %
                request.META.get('HTTP_REFERER'))


@login_required
def wizard_quotes_convert_to_order(request, pk):
    # this will auto convert to an order
    if not request.user.is_staff:
        return {}
    q = Quotes.objects.get(pk=int(pk))
    # by setting to this status, model class will have logic to 
    # auto create the Order() object
    q.quote_status = QUOTE_STATUSES.get('CUSTOMER_APPROVED')
    q.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
