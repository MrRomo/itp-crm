# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
# from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from decimal import Decimal
from crm.models import (
    Accounts,
    Contacts,
    Products,
    ProductPackages,
    Orders,
    Quotes,

    QUOTE_STATUSES,
    QuoteApprovals,
)




# Quotes/QUOTES PACKAGES
@method_decorator(staff_member_required, name='dispatch')
class QuotesList(ListView):
    model = Quotes
    paginate_by = 100
    def get_queryset(self):
        qs = super(QuotesList, self).get_queryset()
        ob = self.request.GET.get('order_by', None)
        if ob:
            return qs.order_by(ob)
        return qs

    def get_context_data(self, **kwargs):
        context = super(QuotesList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context



@method_decorator(staff_member_required, name='dispatch')
class QuotesDraftList(ListView):
    model = Quotes
    paginate_by = 100
    queryset = Quotes.objects.filter(quote_status=QUOTE_STATUSES.get('DRAFT', 'Draft'))

    def get_context_data(self, **kwargs):
        context = super(QuotesDraftList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context


@method_decorator(staff_member_required, name='dispatch')
class QuotesPendingList(ListView):
    model = Quotes
    paginate_by = 100
    queryset = Quotes.objects.filter(quote_status=QUOTE_STATUSES.get('PENDING_CUSTOMER_PRE_APPROVAL', 'x'))

    def get_context_data(self, **kwargs):
        context = super(QuotesPendingList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)


@method_decorator(staff_member_required, name='dispatch')
class QuotesCreate(CreateView):
    model = Quotes
    success_url = reverse_lazy('quotes_list')
    fields = [
        'account',
        'date_created',
        'quote_status',
        #'quote_order_status',
        'customer_signed',
        'customer_signed_ip',
        'accepted_date',
        'valid_until',
        'packages',
    ]
    def get_context_data(self, **kwargs):
        context = super(QuotesCreate, self).get_context_data(**kwargs)
        acc = Accounts.objects.all().order_by('name')
        context['accounts'] = acc
        return context


@method_decorator(staff_member_required, name='dispatch')
class QuotesUpdate(UpdateView):
    model = Quotes
    success_url = reverse_lazy('quotes_list')
    fields = [
        'account',
        'date_created',
        'quote_status',
        #'quote_order_status',
        'customer_signed',
        'customer_signed_ip',
        'accepted_date',
        'valid_until',
        'packages',
    ]
    def get_context_data(self, **kwargs):
        context = super(QuotesUpdate, self).get_context_data(**kwargs)
        return context


@method_decorator(staff_member_required, name="dispatch")
class QuotesDetail(DetailView):
    model = Quotes
    def get_context_data(self, **kwargs):
        context = super(QuotesDetail, self).get_context_data(**kwargs)
        context['extra'] = 'here'
        return context


@method_decorator(staff_member_required, name='dispatch')
class QuotesDelete(DeleteView):
    model = Quotes
    success_url = reverse_lazy('quotes_list')
    def get_context_data(self, **kwargs):
        context = super(QuotesDelete, self).get_context_data(**kwargs)
        if self.request.GET.get('godelete', 'n') == 'y':
            try:
                p = Quotes.objects.get(pk=context.get('object').pk)
                p.deleted = True
                p.save()
                context['deleted_object'] = True
            except Quotes.DoesNotExist:
                p = None
        return context




@login_required
def manager_approval(request):
    if not request.user.is_staff:
        manager_check = request.user.groups.filter(name='Manager').exists()
        if not manager_check:
            raise Http404
    quotes = Quotes.objects.filter(quote_status=QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL'))
    return render(
        request,
        'crm/quotes_manager_approval.html',{
        'quotes': quotes,
        'object_list': quotes,
    },RequestContext(request))


@login_required
def manager_view_order(request, pk):
    if not request.user.is_staff:
        manager_check = request.user.groups.filter(name='Manager').exists()
        if not manager_check:
            raise Http404
    order_id = pk
    qstat = QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL')
    quote = Quotes.objects.get(pk=order_id, quote_status=qstat)

    sumtotal = Decimal('0.0')
    return render(
        request,
        'crm/quotes_manager_approval_detail.html',{
        'quote': quote,
        'object': quote
    },RequestContext(request))


@login_required
def manager_approve_the_quote(request, pk):
    if not request.user.is_staff:
        manager_check = request.user.groups.filter(name='Manager').exists()
        if not manager_check:
            raise Http404
    order_id = pk
    qstat = QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL')
    quote = Quotes.objects.get(pk=order_id, quote_status=qstat)
    if request.POST:
        quote.quote_status = QUOTE_STATUSES.get("MANAGER_APPROVED")
        quote.save()
        return HttpResponseRedirect('/quotes/manager-approval')

    return render(
        request,
        'crm/manager_submit_quote_approval.html',{
        'quote': quote,
        'object': quote
    })

@login_required
def manager_decline_the_quote(request, pk):
    if not request.user.is_staff:
        manager_check = request.user.groups.filter(name='Manager').exists()
        if not manager_check:
            raise Http404
    order_id = pk
    qstat = QUOTE_STATUSES.get('PENDING_MANAGER_APPROVAL')
    quote = Quotes.objects.get(pk=order_id, quote_status=qstat)
    if request.POST:
        quote.quote_status = QUOTE_STATUSES.get("MANAGER_CANCELED")
        quote.save()
        return HttpResponseRedirect('/quotes/manager-approval')

    return render(
        request,
        'crm/manager_submit_quote_cancel.html',{
        'quote': quote,
        'object': quote
    })


def customer_approve_quote(request):
    tkn = request.GET.get('token')
    qa = QuoteApprovals.objects.get(token=tkn)
    q = qa.quote
    if not q.quote_status == QUOTE_STATUSES.get("PENDING_CUSTOMER_PRE_APPROVAL"):
        return render(
            request, 
            'crm/customer_submit_quote_already-approved.html', {
                'quote': q,
                'token': tkn,
                'quote_statuses': QUOTE_STATUSES,
                'quote_declined': QUOTE_STATUSES.get('CLOSED NOT ACCEPTED'),
                'quote_approved': QUOTE_STATUSES.get('PENDING_CUSTOMER_SIGNATURE'),
            })
        #raise Exception("not the right status....")
    if request.POST:
        if request.POST.get('action', None):
            act = request.POST.get('action')
            if act == 'approved':
                qa = QuoteApprovals.objects.get(token=tkn)
                q = qa.quote
                # approved yay
                q.quote_status = QUOTE_STATUSES.get('PENDING_CUSTOMER_SIGNATURE')
                q.save()

            if act == 'declined':
                qa = QuoteApprovals.objects.get(token=tkn)
                q = qa.quote
                # declined... aw boo
                q.quote_status = QUOTE_STATUSES.get('CLOSED NOT ACCEPTED')
                q.save()
            return HttpResponseRedirect('/quotes/customer-approval?token=%s' % tkn)

    return render(
        request, 
        'crm/customer_submit_quote_approval.html', {
            'quote': q,
            'token': tkn,
        })



