# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, Contacts, Products, ProductPackages



# PRODUCTS
@method_decorator(staff_member_required, name='dispatch')
class ProductsList(ListView):
    model = Products
    paginate_by = 10
    def get_queryset(self):
        qs = super(ProductsList, self).get_queryset()
        ob = self.request.GET.get('order_by', None)
        if ob:
            return qs.order_by(ob)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ProductsList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context


@method_decorator(staff_member_required, name='dispatch')
class ProductsCreate(CreateView):
    model = Products
    success_url = reverse_lazy('products_list')
    fields = [
        'name',
        'category',
        'product_type',
        'currency',
        'cost',
        'price',
        'description',
        # 'part_number',
        'deleted',
        'auto_provision',
        #'metadata',
    ]
    def get_context_data(self, **kwargs):
        context = super(ProductsCreate, self).get_context_data(**kwargs)
        return context


@method_decorator(staff_member_required, name='dispatch')
class ProductsUpdate(UpdateView):
    model = Products
    success_url = reverse_lazy('products_list')
    fields = [
        'name',
        'category',
        'product_type',
        'currency',
        'cost',
        'price',
        'description',
        # 'part_number',
        'deleted',
        'metadata',
        'auto_provision',
    ]


@method_decorator(staff_member_required, name="dispatch")
class ProductsDetail(DetailView):
    model = Products
    def get_context_data(self, **kwargs):
        context = super(ProductsDetail, self).get_context_data(**kwargs)
        context['extra'] = 'here'
        return context

@method_decorator(staff_member_required, name='dispatch')
class ProductsDelete(DeleteView):
    model = Products
    success_url = reverse_lazy('products_list')
    def get_context_data(self, **kwargs):
        context = super(ProductsDelete, self).get_context_data(**kwargs)
        if self.request.GET.get('godelete', 'n') == 'y':
            try:
                p = Products.objects.get(pk=context.get('object').pk)
                p.deleted = True
                p.save()
                context['deleted_object'] = True
            except Products.DoesNotExist:
                p = None
        return context

