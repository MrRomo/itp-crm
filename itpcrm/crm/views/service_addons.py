# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator # , user_passes_test
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, ServiceAddons, Contacts, Products, ProductPackages





@method_decorator(staff_member_required, name="dispatch")
class ServiceAddonsDetail(DetailView):
    model = ServiceAddons
    def get_context_data(self, **kwargs):
        context = super(ServiceAddonsDetail, self).get_context_data(**kwargs)
        obj = context.get('object')   
        
        return context 



@method_decorator(staff_member_required, name='dispatch')
class ServiceAddonsCreate(CreateView):
    model = ServiceAddons
    success_url = reverse_lazy('serviceaddons_list')
    fields = [
        'service',
        #'account',
        'product',
        'price_per_unit',
        'price',
        #'bill_interval',
        #'next_bill',
        'metadata',
        #'payment_complete',
        'auto_provision',
        'provision_status',
        'prod_description',
        
    ]
    def get_form(self, *args, **kwargs):
        form = super(ServiceAddonsCreate, self).get_form(*args, **kwargs)
        form.fields['product'].queryset = Products.objects.filter(product_type='ServiceAddon')
        return form


    def get_context_data(self, **kwargs):
        context = super(ServiceAddonsCreate, self).get_context_data(**kwargs)
        #context['google_api_key'] = settings.GOOGLE_API_KEY
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        # custom shit
        
        self.object.save()
        return HttpResponseRedirect('{0}?pk={1}'.format(
            reverse('accountservice_detail', kwargs={'pk': self.object.service.pk}),
            self.object.pk
        ))

