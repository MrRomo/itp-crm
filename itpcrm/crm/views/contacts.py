# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, Contacts, Products, ProductPackages
from django.contrib.admin.views.decorators import staff_member_required


# CONTACTS

@method_decorator(staff_member_required, name='dispatch')
class ContactsList(ListView):
    model = Contacts
    paginate_by = 100

    def get_queryset(self):
        qs = super(ContactsList, self).get_queryset()
        ob = self.request.GET.get('order_by', None)
        if ob:
            return qs.order_by(ob)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ContactsList, self).get_context_data(**kwargs)
        context['account_filter'] = self.request.GET.get('account')
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
        if context.get('account_filter'):
            try:
                acc = Accounts.objects.get(pk=int(context.get('account_filter')))
                context['account_filter_name'] = acc.name
                context['account_filter_id'] = acc.pk
                context['object_list'] = Contacts.objects.filter(account=acc)
            except Accounts.DoesNotExist:
                context['account_filter'] = None
                pass
        return context


@method_decorator(staff_member_required, name='dispatch')
class ContactsCreate(CreateView):
    model = Contacts
    
    fields = [
        'account',
        #'user',
        'firstname',
        'lastname',
        'title',
        'email',
        'primary_street',
        'primary_city',
        'primary_state',
        'primary_zipcode',
        'primary_country',
        'phone',
        'mobile',
        'fax',
        'description',
        'password',
        'client_portal_login_enabled',
        #'auth_api_id',
        #'billing_api_id ',
        "admin_group",
        "technical_group",
        "billing_group"
    ]
    def get_success_url(self):
        return reverse('accounts_detail', kwargs={"pk": self.object.account.pk})

    def get_context_data(self, **kwargs):
        context = super(ContactsCreate, self).get_context_data(**kwargs)
        context['account_filter'] = self.request.GET.get('account')
        if context.get('account_filter'):
            try:
                acc = Accounts.objects.get(pk=int(context.get('account_filter')))
                context['account_filter_name'] = acc.name
                context['account_filter_id'] = acc.pk
            except Accounts.DoesNotExist:
                context['account_filter'] = None
                pass
        return context


@method_decorator(staff_member_required, name='dispatch')
class ContactsUpdate(UpdateView):
    model = Contacts
   
    fields = [
		'account',
        #'user',
        'firstname',
        'lastname',
        'title',
        'email',
        'primary_street',
        'primary_city',
        'primary_state',
        'primary_zipcode',
        'primary_country',
        'phone',
        'mobile',
        'fax',
        'description',
        'password',
        'client_portal_login_enabled',
        "admin_group",
        "technical_group",
        "billing_group"
        #'auth_api_id',
        #'billing_api_id ',
    ]
    def get_success_url(self):
        return reverse('accounts_detail', kwargs={"pk": self.object.account.pk})

@method_decorator(staff_member_required, name='dispatch')
class ContactsDelete(DeleteView):
    model = Contacts
    success_url = reverse_lazy('contacts_list')


@method_decorator(staff_member_required, name="dispatch")
class ContactsDetail(DetailView):
    model = Contacts
    def get_context_data(self, **kwargs):
        context = super(ContactsDetail, self).get_context_data(**kwargs)
        context['check_contacts'] = Contacts.objects.filter(
                account=context.get('object').pk)
        context['extra'] = 'here'
        return context



