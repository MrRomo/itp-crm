# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import requests
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import ServiceAddress, Products, ITPServices_Addresses
from crmapi.serializers import ServiceAddressSerializer
from django.conf import settings


@staff_member_required
def service_address_search(request):
    context = {}
    if request.POST:
        service_address = ServiceAddress.objects.get(pk=request.POST.get('pk'))
        if not service_address:
            raise Exception("Service address pk does not exist: {0}".format(request.POST.get('pk')))
        
        for pid in request.POST.getlist('prod_service_address'):
            p_sa = ITPServices_Addresses.objects.filter(product=Products.objects.get(pk=pid), address=service_address)
            if not p_sa:
                p_sa = ITPServices_Addresses(product=Products.objects.get(pk=pid), address=service_address)
                p_sa.save()
        #raise Exception(request.POST.getlist('prod_service_address[]'))
        return HttpResponseRedirect('{0}?pk={1}'.format(reverse('serviceaddress_search'), service_address.pk))

    if request.GET.get('pk'):
        s = ServiceAddress.objects.get(pk=request.GET.get('pk'))
        context['service_address_object'] = s
        ser = ServiceAddressSerializer(s, many=False)
        j = ser.data.__dict__
        context['pk'] = request.GET.get('pk')
        context['service_address_json'] = json.dumps(ser.data, indent=4)
        context['products_to_add'] = Products.objects.filter(depends_on_service_address=True)
        context['debug_json'] = request.GET.get('debug_json')
        prod_avail = []
        for i in Products.objects.filter(depends_on_service_address=False, product_type='Service'):
            if not i in prod_avail:
                prod_avail.append(i)
        for i in ITPServices_Addresses.objects.filter(address=s):
            if not i in prod_avail:
                prod_avail.append(i.product)
        context['products_available'] = prod_avail
    return render(request, 'crm/serviceaddress_search.html', context, RequestContext(RequestContext))


@staff_member_required
def service_address_formalize(request):
    # formalize with third party api (google maps
    context = {'google_api_key': settings.GOOGLE_API_KEY_AUTOCOMPLETE}
    if request.GET.get('q'):
        addr = request.GET.get('q')
        getplace = 'http://nominatim.openstreetmap.org/search?format=json&q={0}'.format(addr)
        r = requests.get(getplace)
        search_json = json.loads(r.text)
        place_id = search_json[0]['place_id']
        place_url = 'https://nominatim.openstreetmap.org/details?format=json&place_id={0}'.format(place_id)
        place_response = requests.get(place_url)
        place_json = json.loads(place_response.text)
        context['service_address_json'] = json.dumps(place_json, indent=4)
        context['q'] = addr
    return render(request, 'crm/serviceaddress_formalize.html', context, RequestContext(RequestContext))


# PRODUCTS
@method_decorator(staff_member_required, name='dispatch')
class ServiceAddressList(ListView):
    model = ServiceAddress
    paginate_by = 10
    def get_context_data(self, **kwargs):
        context = super(ServiceAddressList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context


@method_decorator(staff_member_required, name='dispatch')
class ServiceAddressCreate(CreateView):
    model = ServiceAddress
    success_url = reverse_lazy('serviceaddress_list')
    fields = [
        'full_address',
        'itpfiber_status',
        'suite_num',
    ]
    def get_context_data(self, **kwargs):
        context = super(ServiceAddressCreate, self).get_context_data(**kwargs)
        context['google_api_key'] = settings.GOOGLE_API_KEY_AUTOCOMPLETE
        context['full_address'] = self.request.GET.get('full_address')
        if context.get('full_address'):
            address_parsed = context.get('full_address').split(',')[0]
            context['serviceaddress_parsed'] = address_parsed
            s = ServiceAddress.objects.filter(full_address__icontains=address_parsed)
            #raise Exception(s)
            if s:
                context['serviceaddress_results'] = s

        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.validated = True
        # custom shit
        
        self.object.save()
        return HttpResponseRedirect('{0}?pk={1}'.format(
            reverse('serviceaddress_search'),
            self.object.pk
        ))



@method_decorator(staff_member_required, name='dispatch')
class ServiceAddressUpdate(UpdateView):
    model = ServiceAddress
    success_url = reverse_lazy('serviceaddress_list')
    fields = [
        'full_address',
        'itpfiber_status',
       
    ]
    def form_valid(self, form):
        self.object = form.save(commit=False)
        # custom shit
        self.object.updated_address = True
        self.object.save()
        return HttpResponseRedirect('{0}?pk={1}'.format(
            reverse('serviceaddress_detail'),
            self.object.pk
        ))


@method_decorator(staff_member_required, name="dispatch")
class ServiceAddressDetail(DetailView):
    model = ServiceAddress
    def get_context_data(self, **kwargs):
        context = super(ServiceAddressDetail, self).get_context_data(**kwargs)
        context['extra'] = 'here'
        return context

@method_decorator(staff_member_required, name='dispatch')
class ServiceAddressDelete(DeleteView):
    model = ServiceAddress
    success_url = reverse_lazy('products_list')
    def get_context_data(self, **kwargs):
        context = super(ServiceAddressDelete, self).get_context_data(**kwargs)
        if self.request.GET.get('godelete', 'n') == 'y':
            try:
                p = ServiceAddress.objects.get(pk=context.get('object').pk)
                p.deleted = True
                p.save()
                context['deleted_object'] = True
            except ServiceAddress.DoesNotExist:
                p = None
        return context
