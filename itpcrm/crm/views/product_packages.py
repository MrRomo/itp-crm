# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, Contacts, Products, ProductPackages



# PRODUCT PACKAGES
@method_decorator(staff_member_required, name='dispatch')
class ProductPackagesList(ListView):
    model = ProductPackages


@method_decorator(staff_member_required, name='dispatch')
class ProductPackagesCreate(CreateView):
    model = ProductPackages
    success_url = reverse_lazy('productpackages_list')
    fields = [
        'name',
        'package_type',
		'phase_type',
		'num_of_days',
		'discount_percent',
        'owner',
		'products',
    ]
    def get_context_data(self, **kwargs):
        context = super(ProductPackagesCreate, self).get_context_data(**kwargs)
        customers = Accounts.objects.all().order_by('name')
        context['customers'] = customers
        return context


@method_decorator(staff_member_required, name='dispatch')
class ProductPackagesUpdate(UpdateView):
    model = ProductPackages
    success_url = reverse_lazy('productpackages_list')
    fields = [
        'name',
        'package_type',
		'phase_type',
		'num_of_days',
		'discount_percent',
        'owner',
		'products',
    ]
    def get_context_data(self, **kwargs):
        context = super(ProductPackagesCreate, self).get_context_data(**kwargs)
        return context


@method_decorator(staff_member_required, name="dispatch")
class ProductPackagesDetail(DetailView):
    model = ProductPackages
    def get_context_data(self, **kwargs):
        context = super(ProductPackagesDetail, self).get_context_data(**kwargs)
        context['extra'] = 'here'
        return context


@method_decorator(staff_member_required, name='dispatch')
class ProductPackagesDelete(DeleteView):
    model = ProductPackages
    success_url = reverse_lazy('productpackages_list')
    def get_context_data(self, **kwargs):
        context = super(ProductPackagesDelete, self).get_context_data(**kwargs)
        if self.request.GET.get('godelete', 'n') == 'y':
            try:
                p = ProductPackages.objects.get(pk=context.get('object').pk)
                p.deleted = True
                p.save()
                context['deleted_object'] = True
            except ProductPackages.DoesNotExist:
                p = None
        return context
