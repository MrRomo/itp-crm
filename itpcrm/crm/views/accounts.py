# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests
import json
import logging
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy, reverse
# from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator # , user_passes_test
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, AccountServices, Contacts, Products, ProductPackages, BillingAPI
from crm.logger import CRMLogger


logger = CRMLogger()


# ACCOUTNS 
@method_decorator(staff_member_required, name='dispatch')
class AccountsList(ListView):
    model = Accounts
    paginate_by = 100

    def get_queryset(self):
        qs = super(AccountsList, self).get_queryset()
        ob = self.request.GET.get('order_by', None)
        if ob:
            return qs.order_by(ob)
        return qs

    def get_context_data(self, **kwargs):
        context = super(AccountsList, self).get_context_data(**kwargs)
        logger.debug('crm.views.accounts.AccountsList - INIT')
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context


@method_decorator(staff_member_required, name='dispatch')
class AccountsCreate(CreateView):
    model = Accounts
    fields = [
		'name',
        'account_type',
		'phone',
		'website',
		'billing_address',
		'billing_address',
		'billing_city',
		'billing_state',
		'billing_zipcode',
		'billing_country',
        'industry',
		'accountnumber',
		'status',
		'net_terms'
    ]
    def get_success_url(self):
        return reverse('accounts_detail', kwargs={"pk": self.object.pk})


@method_decorator(staff_member_required, name='dispatch')
class AccountsUpdate(UpdateView):
    model = Accounts
    #success_url = reverse_lazy('accounts_detail')
    fields = [
		'name',
        'account_type',
		'phone',
		'website',
		'billing_address',
		'billing_address',
		'billing_city',
		'billing_state',
		'billing_zipcode',
		'billing_country',
		
		'industry',
		'accountnumber',

		'status',
		'net_terms'
    ]
    def get_success_url(self):
        return reverse('accounts_detail', kwargs={"pk": self.object.pk})
        #pass #return the appropriate success url


@method_decorator(staff_member_required, name='dispatch')
class AccountsDelete(DeleteView):
    model = Accounts
    success_url = reverse_lazy('accounts_list')



@method_decorator(staff_member_required, name="dispatch")
class AccountsDetail(DetailView):
    model = Accounts
    def get_context_data(self, **kwargs):
        context = super(AccountsDetail, self).get_context_data(**kwargs)

        check_contacts = False
        for x in context.get('object').contacts_set.select_related():
            check_contacts = True
        context['check_contacts'] = check_contacts
        context['orders'] = context.get('object').recent_orders()
        context['quotes'] = context.get('object').recent_quotes()
        context['services'] = []
        context['invoices'] = BillingAPI.get_invoices(context.get('object').billing_api_id)
        

        for a in AccountServices.objects.filter(account=context.get('object')).order_by('-id'):
            context.get('services', []).append(a)

        map_markers = []
        for a in context.get('services'):
            adr = a.service_address
            if adr:
                if adr.geo_data:
                    try:
                        lat = adr.geo_data[0].get('lat')
                        lon = adr.geo_data[0].get('lon')
                    except KeyError:
                        try:
                            lat = adr.geo_data.get('place').get('geometry').get('coordinates')[1]
                            lon = adr.geo_data.get('place').get('geometry').get('coordinates')[0]
                        except AttributeError:
                            lat = adr.lat
                            lon = adr.lon
                    map_markers.append(
                        "L.marker([%s, %s]).addTo(map);" % (lat, lon))
        context['geo_markers'] = map_markers
        context['billing_account_balance'] = None

        billing_obj = context.get('object').billing_api
        context['billing_api'] = billing_obj
        context['billing_account_balance'] = billing_obj.get('balance')

        return context
