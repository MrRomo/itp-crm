# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
# from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, AccountServices, Contacts, Products, ProductPackages, OrderItems



@login_required
def orderitem_provision(request):
    if not request.user.is_staff:
        return Http404
    oid = request.GET.get('orderitem_id')
    o = OrderItems.objects.get(pk=oid)
    o.provision_status = 'pending-provision'
    o.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def orderitem_mark_complete(request):
    """
    Mark completed
    Default product mark complete from quotes
    """
    # mark an order complete manually
    if not request.user.is_staff:
        return Http404
    oid = request.GET.get('orderitem_id')
    acs_id = request.GET.get('account_service_id')
    o = OrderItems.objects.get(pk=oid)
    o.completed = True
    m = o.metadata
    m['status'] = 'enabled'
    o.metadata = m
    o.provision_status = 'provisioned'    
    o.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def orderitem_fiber_mark_complete(request):
    """
    Mark completed
    fiber product special cases
    """
    # mark an order complete manually
    if not request.user.is_staff:
        return Http404
    oid = request.GET.get('orderitem_id')
    acs_id = request.GET.get('account_service_id')
    o = OrderItems.objects.get(pk=oid)
    o.completed = True
    m = o.metadata
    m['status'] = 'enabled'
    o.metadata = m
    o.provision_status = 'provisioned'
    oi = o
    #o.update_metadata('status', 'enabled')
    try:
        if acs_id:
            ac = AccountServices.objects.get(pk=acs_id)
            # upgrade / downgrade of a service
            if o.purchase_type == 'downgrade' or o.purchase_type == 'upgrade':
                ac.product = o.product
                ac.metadata = o.metadata
                ac.quantity = 1
                ac.price = o.product.price 
                ac.price_per_unit = o.product.price
                ac.save()
            
        else:
            ac = AccountServices.objects.get(oi=oi)
    except AccountServices.DoesNotExist:
        ac = AccountServices(
            account=oi.order.account,
            product=oi.product,
            order=oi.order,
            oi=oi,
            item_type=oi.product.item_type,
            price_per_unit=oi.price_per_unit,
            price=oi.price,
            metadata=oi.metadata,
            prov_status=True,
            provision_status='provisioned',
            provision_result='manually provisioned',
            quantity=oi.quantity,
            req_activation_date=oi.req_activation_date,
            activation_date=datetime.now())
        ac.save()

    o.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))



@login_required
def orderitem_manual_approve(request):
    if not request.user.is_staff:
        return Http404
    oid = request.GET.get('orderitem_id')
    oi = OrderItems.objects.get(pk=oid)
    oi.provision_status = 'provisioned'
    oi.save()
    if oi.product.product_type == 'ServiceAddon':
        try:
            ac = AccountServices.objects.get(oi=oi)
        except AccountServices.DoesNotExist:
            ac = AccountServices(
                account=oi.order.account,
                product=oi.product,
                order=oi.order,
                oi=oi,
                item_type=oi.product.part_number,
                price_per_unit=oi.price_per_unit,
                price=oi.price,
                metadata=oi.metadata,
                prov_status=True,
                provision_status='provisioned',
                provision_result='manually provisioned',
                quantity=oi.quantity,
                req_activation_date=oi.req_activation_date,
                activation_date=datetime.now())
            ac.save()


    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# ORDERS/QUOTES PACKAGES
@method_decorator(staff_member_required, name='dispatch')
class OrderItemsList(ListView):
    model = OrderItems
    paginate_by = 25

    def get_queryset(self):
        qs = super(OrderItemsList, self).get_queryset()
        ob = self.request.GET.get('order_by', None)
        if ob:
            return qs.order_by(ob)
        return qs

    def get_context_data(self, **kwargs):
        context = super(OrderItemsList, self).get_context_data(**kwargs)
        orderby = self.request.GET.get('order_by')
        if orderby:
            context['order_by'] = orderby
            #context['object_list'] = context['object_list'].order_by(orderby)
        return context



@method_decorator(staff_member_required, name='dispatch')
class OrderItemsCreate(CreateView):
    model = OrderItems
    success_url = reverse_lazy('orderitems_list')
    fields = [
        'order',
        'product',
        'status',
        'item_type',
        'price_per_unit',
        'price',
        'bill_interval',
        'payment_complete',
        'auto_provision',
        'prov_status',
		
    ]
    def get_context_data(self, **kwargs):
        context = super(OrderItemsCreate, self).get_context_data(**kwargs)
        acc = Accounts.objects.all().order_by('name')
        context['accounts'] = acc
        return context


@method_decorator(staff_member_required, name='dispatch')
class OrderItemsUpdate(UpdateView):
    model = OrderItems
    success_url = reverse_lazy('orderitems_list')
    fields = [
        'order',
        'product',
        'status',
        'item_type',
        'price_per_unit',
        'price',
        'bill_interval',
    ]
    def get_context_data(self, **kwargs):
        context = super(OrderItemsUpdate, self).get_context_data(**kwargs)
        return context


@method_decorator(staff_member_required, name="dispatch")
class OrderItemsDetail(DetailView):
    model = OrderItems
    def get_context_data(self, **kwargs):
        context = super(OrderItemsDetail, self).get_context_data(**kwargs)
        if context.get('object').metadata:
            context['meta_data'] = context.get('object').metadata
        context['extra'] = 'here'
        try:
            context['ac'] = AccountServices.objects.get(oi=context.get('object'))
        except AccountServices.DoesNotExist:
            context['ac'] = None

        manual_approve_provision = False
        if not context.get('object').auto_provision:
            if not context.get('ac', None):
                if context.get('object').provision_status == 'unprovisioned':
                    manual_approve_provision = True
        context['manual_approve_provision'] = manual_approve_provision
        return context


@method_decorator(staff_member_required, name='dispatch')
class OrderItemsDelete(DeleteView):
    model = OrderItems
    success_url = reverse_lazy('orders_list')
    def get_context_data(self, **kwargs):
        context = super(OrderItemsDelete, self).get_context_data(**kwargs)
        if self.request.GET.get('godelete', 'n') == 'y':
            try:
                p = OrderItems.objects.get(pk=context.get('object').pk)
                p.deleted = True
                p.save()
                context['deleted_object'] = True
            except OrderItems.DoesNotExist:
                p = None
        return context
