# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import requests
import json
import csv
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.generic import TemplateView,ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView,
)
from decimal import Decimal
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.http import Http404
from datetime import datetime, timedelta
from django.contrib.contenttypes.models import ContentType
from crm.models import Accounts, Contacts, Products, ProductPackages, Orders, BillingAPI
from django.conf import settings

from crm.logger import CRMLogger


logger = CRMLogger()

def decode_utf8(input_iterator):
    for l in input_iterator:
        yield l.decode('utf-8')


@login_required
def international_rates(request):
    if not request.user.is_staff:
        return Http404

    if request.POST:
        post = request.POST
        action = post.get('action')

        if action == 'add-single':
            payload = {
                "carrier": post.get('carrier'),
                "destination": post.get('destination'),
                "cost": float(post.get('cost')),
                "country": post.get('country'),
            }
            r = BillingAPI.write_billing_entry(payload)
            resp_code = r.status_code
            resp_data = r.text
            errors = ''
            logger.debug("Response: {0}".format(resp_code))
            logger.debug(resp_data)
            if not resp_code == 200 or not resp_data:
                errors = '?errors={0}'.format(resp_data)
            return HttpResponseRedirect('{0}{1}'.format(reverse('admintools_international_rates'), errors))

        if action == 'add-csv':
            logger.debug("Request Files: {0}".format(request.FILES))
            data = csv.DictReader(decode_utf8(request.FILES['csv_file']))
            logger.debug("CSV Data {0}".format(data))
            for row in data:
                logger.debug("row - {0}".format(row))
                payload = {
                    "carrier": row.get('carrier'),
                    "destination": row.get('destination'),
                    "cost": float(row.get('cost')),
                    "country": row.get('country'),
                }
                r = BillingAPI.write_billing_entry(payload)
                resp_code = r.status_code
                resp_data = r.text
                errors = ''
                logger.debug("Response: {0}".format(resp_code))
                logger.debug(resp_data)
                if not resp_code == 200 or not resp_data:
                    errors = '?errors={0}'.format(resp_data)
                


    try:
        r = BillingAPI.get_billing_international_rates()
        object_list = json.loads(r.text).get('result')
        # logger.debug(object_list)
        errors = None
    except Exception as e:
        object_list = []
        errors = "Failed to get data from billing API. Please contact administrator"
        logger.debug("Get international rates from billing api.")
        logger.debug(str(e))
        

    return render(request, 'crm/admintools/international_rates.html', {
        'data': 'test',
        'object_list': object_list,
        "errors": errors
    }, RequestContext(request))
