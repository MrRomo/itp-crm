


def create_api_itp_voice(obj):
    # when an itp voice service is created, create teh
    # accouunt in the API
    url = 'http://api.itpscorp.com/itpvoice/api/accounts/'
    apikey = '376c72e7-72ad-4d1f-8a81-635e4cc308b8'
    create_ok = False

    if not obj.pk:
        # new 
        d = {
            "name": obj.account.name,
            "role": "admin",
            "website": obj.account.website,
            "type": "Business"
        }

        headers = {
            'apikey': apikey,
            'content-type': "application/json",
            'cache-control': "no-cache",
            #'postman-token': "f87930c6-f157-1912-57c4-f7b045a6a08a"
        }
        response = requests.request("POST", url, data=json.dumps(d), headers=headers)
        if '503' in str(response.status_code):
            create_ok = False
            response2 = requests.request("POST", url,
                                        data=json.dumps(d), headers=headers)
            if '200' in str(response2.status_code):
                create_ok = response2.text
            else:
                create_ok = False
                obj.create_results = response2.text

        # IF 200
        if '200' in str(response.status_code):
            create_ok = response.text

        jr = json.loads(response.text)
        #raise Exception(jr)
        obj.apiid = jr.get('_id')
        obj.create_results = 'ok'

    else:
        # update
        create_ok = True
        pass