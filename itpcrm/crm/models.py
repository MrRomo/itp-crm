# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.core.mail import send_mail, EmailMessage
import logging
import uuid
import requests
import json
import monthdelta
import asyncio
from datetime import timedelta
from django_mysql.models import JSONField
from decimal import Decimal
from django.db import models
from django.contrib.auth.models import User
from crm.industry_choices import INDUSTRY_CHOICES
from random import randint
from datetime import datetime, timedelta
from crm.postbacks import create_api_itp_voice
from threading import Thread
from django.conf import settings

#from crm.docusign_api import run_docusign_on_quote
from crm.e_sign import run_esign_on_quote


from crm.logger import CRMLogger
logger = CRMLogger()


ACCT_STATUS_CHOICES = (
    ('Active', 'Active'),
    ('Suspended', 'Suspended'),
    ('Pending', 'Pending'),
)

CURRENCY_TYPES = (
    ('USD', 'USD'),
    ('CAD', 'CAD'), 
)

PRODUCT_CATEGORY = (
    ('Hosted Service', 'Hosted Service'),
    ('Hardware', 'Hardware'),
    ('Software', 'Software'),
    ('Labor', 'Labor'),
    ('Phone', 'Phone'),
    ('Cloud', 'Cloud'),
    ('Data', 'Data'),
    ('IP Address', 'IP Address'),
    ('Fiber SMB', 'Fiber SMB'),
    ('Fiber DIA', 'Fiber DIA'),
)

# the part number relates to the keys inside 
SERVICE_PART_NUMBERS = [
    'google_apps',
    'it_managed_services',
    'itp_cloud',
    'itp_fiber',
    'itp_voice',
    'sip_trunking',
    'itpvoice_pbx_did',
    'itpvoice_pbx_did_port',
    'hardware_for_resale',
]
SERVICE_PART_NUMBER_CHOICE = (
    ('google_apps', 'google_apps'),
    ('it_managed_services', 'it_managed_services',),
    ('itp_cloud', 'itp_cloud',),
    ('itp_fiber', 'itp_fiber',),
    ('itp_voice', 'itp_voice',),
    ('sip_trunking', 'sip_trunking',),
    ('itp_vfax', 'itp_vfax',),
    ('itpvoice_pbx_did', 'itpvoice_pbx_did',),
    ('itpvoice_pbx_did_port', 'itpvoice_pbx_did_port',),
    ('itp_fiber_dia', 'itp_fiber_dia'),
    ('hardware_for_resale', 'hardware_for_resale'),
)

# when upgrading items these will 
# retain the price ffrom the related account service
QUANTIFIABLE_ITEMS = (
    'itp_voice',
    'sip_trunking',
    'itp_vfax',
    'itpvoice_pbx_did',
    'itpvoice_pbx_did_port',
)

# when upgrading/downgrading, these items will
# retain the price from the related product
NON_QUANTIFIABLE_ITEMS = (
    'itp_fiber_dia',
    'itp_fiber',
    'it_managed_services',
)

HIDE_QUANTITY = [
    'itp_fiber',
    'itp_vfax_tollfree_port',
    'itp_vfax_ported_number',
]


ORDER_STATS = (
    ('quote', 'quote'),
    ('processing','processing'),
    ('fulfilled', 'fulfilled'),
    ('declined', 'declined'),
)

QUOTE_STATUSES = {
    "DRAFT": "Draft",
    "MANAGER_APPROVAL_REQUIRED": "Manager Approval Required",
    "PENDING_MANAGER_APPROVAL": "Pending Manager Approval",
    "MANAGER_APPROVED": "Manager Approved",
    "MANAGER_CANCELED": "Manager Canceled",
    "PENDING_CUSTOMER_PRE_APPROVAL": "Pending Customer Pre Approval",
    "PENDING_CUSTOMER_SIGNATURE": "Pending Customer Signature",
    "CUSTOMER_APPROVED": "Customer Approved",
    "CLOSED ACCEPTED": "Closed Accepted",
    "CLOSED NOT ACCEPTED": "Closed Not Accepted",
    "AGENT_CANCELED": "Agent Canceled",

}

PURCHASE_TYPES = (
    ('new-order', 'new-order',),
    ('service-addon', 'service-addon',),
    ('disconnect', 'disconnect',),
    ('upgrade', 'upgrade',),
    ('downgrade', 'downgrade',),
    ('nrc-product', 'nrc-product',),
)


# i think this is a dud..... TODO 
#ITEM_TYPES = ()
#for k, v in SERVICES_META.items():
#    ITEM_TYPES += (k, k),
#ITEM_TYPES += ('product', 'product')

ACCT_TYPES = (
    ('business', 'business',),
    ('residential', 'residential',),
    ('residential mdu', 'residential mdu'),
)


class BillingAPI(object):
    @classmethod 
    def get_account(cls, account):
        # main propertu to access billing api data
        billing_api_url = "{0}/api/billing/account".format(settings.ITP_BASE_API_URL)
        url = '{0}/{1}'.format(billing_api_url, account.billing_api_id)
        headers = {
                    'Authorization': "abc123123",
                    'content-type': "application/json",
                    'cache-control': "no-cache",
                    'postman-token': "7370b070-228f-efe3-8a02-d7ea501c5609"
        }
        r = requests.get(url, headers=headers)
        try:
            rj = json.loads(r.text)
            logger.debug("Response Accounts.billing_api: {0}".format(r.text))
            return rj.get('result')
        except Exception as e:
            logger.error("Exception reading from Billing API")
            logger.debug("Billing api response: {0}".format(str(e)))
            logger.debug("Response text: {0}".format(str(r.text)))
            return {}
    
    @classmethod
    def get_invoices(cls, account_id):
        billing_api_url = "{0}/api/billing/invoices".format(settings.ITP_BASE_API_URL)
        url = '{0}?account_id={1}'.format(billing_api_url, account_id)
        headers = {
                    'Authorization': "abc123123",
                    'content-type': "application/json",
                    'cache-control': "no-cache",
                    'postman-token': "7370b070-228f-efe3-8a02-d7ea501c5609"
        }
        r = requests.get(url, headers=headers)
        try:
            rj = json.loads(r.text)
            logger.debug("Response Accounts.billing_api: {0}".format(r.text))
            return rj.get('result')
        except Exception as e:
            logger.error("Exception reading from invoices Billing API")
            logger.debug("Billing api invoices response: {0}".format(str(e)))
            logger.debug("Response text: {0}".format(str(r.text)))
            return {}


    @classmethod 
    def write_billing_entry(cls, payload):
        billing_api_url = "{0}/api/billing/rates/international".format(settings.ITP_BASE_API_URL)
        logger.debug("Add international rate")
        url = billing_api_url
        logger.debug("- url: {0}".format(url))
        headers = {
                    'Authorization': "abc123123",
                    'content-type': "application/json",
                    'cache-control': "no-cache",
        }
        r = requests.post(url, headers=headers, json=payload)
        return r

    @classmethod
    def get_billing_international_rates(cls):
        billing_api_url = "{0}/api/billing/rates/international".format(settings.ITP_BASE_API_URL)
        logger.debug("GET international rates")
        url = billing_api_url
        logger.debug("- url: {0}".format(url))
        headers = {
                    'Authorization': "abc123123",
                    'content-type': "application/json",
                    'cache-control': "no-cache",
        }
        r = requests.get(url, headers=headers)
        return r




class Accounts(models.Model):
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)

    name = models.CharField(max_length=150, blank=True, null=True)
    phone = models.CharField(max_length=45, blank=True, null=True)
    website = models.TextField(blank=True, null=True)
    billing_address = models.TextField(blank=True, null=True)
    billing_city = models.CharField(max_length=45, blank=True, null=True)
    billing_state = models.CharField(max_length=45, blank=True, null=True)
    billing_zipcode = models.CharField(max_length=45, blank=True, null=True)
    billing_country = models.CharField(max_length=45, blank=True, null=True)
    industry = models.CharField(max_length=50, blank=True, null=True, choices=INDUSTRY_CHOICES)
    accountnumber = models.CharField(max_length=45, blank=True, null=True)
    
    status = models.CharField(max_length=9, blank=True, null=True, choices=ACCT_STATUS_CHOICES)
    net_terms = models.CharField(max_length=2, blank=True, null=True)
    imgurl = models.TextField(null=True, blank=True)
    billing_api_id = models.CharField(max_length=40, blank=True, null=True)
    crm_login_user = models.ForeignKey(User, blank=True, null=True , on_delete=models.CASCADE)
    agent = models.ForeignKey('Agents', blank=True, null=True  , on_delete=models.CASCADE)
    account_type = models.CharField(max_length=20, blank=False, null=False, choices=ACCT_TYPES)

    class Meta:
        managed = True
        db_table = 'accounts'

    @property
    def billing_api(self):
        # main propertu to access billing api data
        account = self
        return BillingAPI.get_account(account)
        
        
    @property
    def get_contacts(self):
        return self.contacts_set.select_related()

    def expected_payment(self):
        total_price = Decimal('0.0')
        for x in AccountServices.objects.filter(account=self):
            total_price += x.price
            for sa in ServiceAddons.objects.filter(service=x):
                total_price += sa.price
        return total_price

    def recent_quotes(self):
        return Quotes.objects.filter(account=self).order_by('-id')

    def recent_orders(self):
        return Orders.objects.filter(account=self).order_by('-id')

    def allow_agent_add_contact(self, agent):
        if self.agent.pk == agent.pk:
            return True

        # get any subagents under this agent, assume self.agent is master
        agent_owners = AgentOwnedBy.objects.filter(
                master_agent=self.agent)

        for o in agent_owners:
            if agent.pk == o.sub_agent.pk:
                return True
            if agent.pk == o.master_agent.pk:
                return True

        agent_owners_sub = AgentOwnedBy.objects.filter(
                sub_agent=self.agent)

        for o in agent_owners_sub:
            if agent.pk == o.sub_agent.pk:
                return True
            if agent.pk == o.master_agent.pk:
                return True

        return False


    def save(self, *args, **kwargs):
        # do stuff before here
        if not self.accountnumber:
            self.accountnumber = '%s' % randint(100001,99999999)
        # make call to billing api to sync account info
        if not self.billing_api_id:
            try:
                # pay = {"itpcrm_pkid": self.pk, "itp_accountnumber": self.accountnumber}
                pay = {
                    "tax_exempt": False,
                    "net_terms": self.net_terms,
                    "crm_account_number": self.accountnumber
                }
                logger.debug("New Account - Payload for billing api %s" % pay)
                headers = {
                    'Authorization': "abc123123",
                    'content-type': "application/json",
                    'cache-control': "no-cache",
                    'postman-token': "7370b070-228f-efe3-8a02-d7ea501c5609"
                }
                url = "{0}/api/billing/account".format(settings.ITP_BASE_API_URL)
                response = requests.request("POST", url, data=json.dumps(pay), headers=headers, verify=False)
                logger.debug("Billing API Response text: {0}".format(response.text))
                try:
                    j = json.loads(response.text)
                    if j.get('result').get('_id'):
                        self.billing_api_id = j.get('result').get('_id')
                        logger.debug("Storing bill api id: {0}".format(self.billing_api_id))
                except Exception as e:
                    # just kepe trreyiung for now
                    logger.error("- Create account on billing: Exception {0}".format(str(e)))
                    pass
            except:
                pass
        super(Accounts, self).save(*args, **kwargs)



class Contacts(models.Model):
    """
    One to one relation to the (current) voice api Users table
    which will be moved to the auth api 
    """
    def __str__(self):
        return str("%s %s / %s" % (self.firstname, self.lastname, self.account.name))
    def __unicode__(self):
        return unicode("%s %s / %s" % (self.firstname, self.lastname, self.account.name))
    account = models.ForeignKey('Accounts', default=int(1)  , on_delete=models.CASCADE)
    #user = models.ForeignKey(User, blank=True, null=True)
    firstname = models.CharField(max_length=30, blank=False, null=False)
    lastname = models.CharField(max_length=30, blank=True, null=True)
    title = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=150, blank=False, null=False)
    primary_street = models.CharField(max_length=150, blank=True, null=True)
    primary_city = models.CharField(max_length=100, blank=True, null=True)
    primary_state = models.CharField(max_length=100, blank=True, null=True)
    primary_zipcode = models.CharField(max_length=50, blank=True, null=True)
    primary_country = models.CharField(max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    fax = models.CharField(max_length=15, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    client_portal_login_enabled = models.NullBooleanField(blank=True, null=True, default=False)
    auth_api_id = models.CharField(max_length=150, null=True, blank=True)
    billing_api_id = models.CharField(max_length=150, null=True, blank=True)
    admin_group = models.NullBooleanField(default=False)
    billing_group = models.NullBooleanField(default=False)
    technical_group = models.NullBooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'contacts'

    def save(self, *args, **kwargs):
        self.client_portal_login_enabled = True
        if self.client_portal_login_enabled:
            if not self.auth_api_id:
                if not self.password:
                    self.password = 'user123'
                print ("CREATE Creating user") 
                account_id = None
                #svoip = ServItpvoice.objects.filter(account=self.account)
                #if svoip:
                #    account_id = svoip[0].apiid
                username = self.email
                account_number = self.account.accountnumber

                if not self.email:
                    username = '%s.%s%s' % (self.firstname.replace(' ', '').lower(),
                                          self.lastname.replace(' ', '').lower(),
                                          self.account.pk)
                url = '{0}/users'.format(settings.AUTH_API_URL)
                # url = 'https://api.itpscorp.com/auth/api/users'
                # apikey = '376c72e7-72ad-4d1f-8a81-635e4cc308b8''tec
                roles = []
                if self.admin_group:
                    roles.append('admin')
                if self.billing_group:
                    roles.append('billing')
                if not roles:
                    roles.append('contact')

                d = {
                    "username": username,
                    "password": self.password,
                    "accountnumber": account_number,
                    "roles": roles,
                    "item_type": "crm"
                    #"acl": {}
                }
                # roles = d['acl'].get(account_number)
                """if self.admin_group:
                    if not 'admin' in roles:
                        d['acl'].get(account_number).append('admin')
                else:
                    if not 'contact' in roles:
                        d['acl'].get(account_number).append('contact')"""
                # if account_id:
                #     d['accountID'] = account_id
                headers = {
                    'apikey': settings.AUTH_API_KEY,
                    'content-type': "application/json",
                    'cache-control': "no-cache",
                }
                self.password = 'abc'
                response = requests.request("POST", url, data=json.dumps(d), headers=headers)
                # print ("RESPONSE = %s" % response)

                # IF 200
                if '200' in str(response.status_code):
                    create_ok = response.text
                    jr = json.loads(response.text)
                    self.auth_api_id = jr.get('_id')

        super(Contacts, self).save(*args, **kwargs)


class ProductManufacturer(models.Model):
    # create manufacturer table
    # create a new field on products table for manufactgurer
    # create a new field on products table for model 
    name = models.CharField(max_length=40)
    description = models.TextField(blank=True, null=True)
    website = models.TextField(blank=True, null=True)
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)

class ProductKeyNames(models.Model):
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)
    name = models.CharField(max_length=40)
    default_metadata = JSONField(blank=True, null=True)

class ProductKeyServiceAddons(models.Model):
    #product_key = models.ForeignKey("ProductKeyNames", blank=True, null=True  , on_delete=models.CASCADE)
    product_key = models.CharField(max_length=30, choices=PRODUCT_CATEGORY, 
                                  blank=True, null=True)
    addon_products = models.ManyToManyField("Products", blank=True, null=True)


class Products(models.Model):
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)
    name = models.CharField(max_length=70, default='Change me')
    category = models.CharField(max_length=15, choices=PRODUCT_CATEGORY,
                    verbose_name=('Product Category'))
    product_type = models.CharField(max_length=20, choices=(
            ('Product', 'Product',),('Service', 'Service'),('ServiceAddon', 'ServiceAddon'),))
    currency = models.CharField(max_length=5, choices=CURRENCY_TYPES)
    cost = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    description = models.TextField(blank=True, null=True)
    #part_number = models.CharField(max_length=40, blank=True, null=True, 
    #                            #choices=SERVICE_PART_NUMBER_CHOICE,
    #                            verbose_name=('Key Name'))
    deleted = models.NullBooleanField(blank=True, null=True, default=False)
    metadata = JSONField(blank=True, null=True)
    auto_provision = models.NullBooleanField(blank=True, null=True, default=False)
    product_rel = models.TextField(blank=True, null=True)
    manufacturer = models.ForeignKey('ProductManufacturer', blank=True, null=True , on_delete=models.CASCADE)
    model_number = models.CharField(max_length=40, blank=True, null=True)
    product_key = models.ForeignKey('ProductKeyNames', blank=True, null=True , on_delete=models.CASCADE)   # required!
    taxable = models.NullBooleanField(blank=True, null=True, default=False)
    tax_percent = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    depends_on_service_address = models.NullBooleanField(default=False, blank=True, null=True)

    @property
    def part_number(self):
        return self.product_key.name

    @property
    def item_type(self):
        return self.part_number

    @property
    def quantifiable(self):
        if self.part_number in QUANTIFIABLE_ITEMS:
            return True
        return False

    def get_addons_for_product(self):
        # get all the addons related to a given product
        category = self.category
        logger.debug("category {0}".format(self.category))
        addon_keys = ProductKeyServiceAddons.objects.filter(product_key=category)
        logger.debug(addon_keys)
        products = []
        for addon in addon_keys:
            logger.debug("addon {0}".format(addon))
            for p in addon.addon_products.all():
                logger.debug("append prod {0}".format(p))
                products.append(p)
        return products


    def save(self, *args, **kwargs):
        #if not self.part_number:
        #    self.part_number = self.product_key

        if not self.metadata:
            d = {}
            #self.metadata = self.product_key.default_metadata
            SERVICES_META2 = self.product_key.default_metadata
            for field in SERVICES_META2.get(self.part_number, {}).get('fields', []):
                d[field] = None
            #self.metadata = json.dumps(d, indent=4)
            self.metadata = d

        super(Products, self).save(*args, **kwargs)


class ProductPackages(models.Model):
    def __str__(self):
        return str(self.pk)
    def __unicode__(self):
        return unicode(self.pk)
    name = models.CharField(max_length=30)
    package_type = models.CharField(max_length=15, choices=(
            ('Service','Service'),('Product','Product')))
    phase_type = models.CharField(max_length=15, choices=(
            ('Discount','Discount'),('Trial','Trial')))
    num_of_days = models.IntegerField(default=3)
    discount_percent = models.IntegerField(default=int(0))
    owner = models.ForeignKey(User, blank=True, null=True  , on_delete=models.CASCADE)
    products = models.ManyToManyField('Products')

    def save(self, *args, **kwargs):
        if self.phase_type == 'Trial':
            self.discount_percent = 100
        super(ProductPackages, self).save(*args, **kwargs)



QUOTE_STATUS = ()
for k, v in QUOTE_STATUSES.items():
    QUOTE_STATUS += (v, v),
#QUOTE_STATUSES = QUOTE_STATUS
class Quotes(models.Model):
    """
    DRAFT = 
    MANAGER APPROVAL REQUIRED = 
    PENDING MANAGER APPROVAL =
    MANAGER APPROVED = 
    MANAGER CANCELED = 
    PENDING CUSTOMER PRE-APPROVAL = 
    PENDING CUSTOMER SIGNATURE = 
    CUSTOMER APPROVED = 
    CLOSED ACCEPTED = 
    CLOSED NOT-ACCEPTED = 
    """
    account = models.ForeignKey('Accounts' ,on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=True, null=True)
    date_created = models.DateTimeField(default=datetime.now())
    quote_status = models.CharField(max_length=36, choices=QUOTE_STATUS)
    customer_signed = models.NullBooleanField(default=False)
    customer_signed_ip = models.CharField(max_length=20)
    accepted_date = models.DateTimeField(blank=True, null=True, default=None)
    valid_until = models.DateTimeField(blank=True, null=True, default=None)
    packages = models.ManyToManyField('ProductPackages', blank=True, null=True)
    agent = models.ForeignKey('Agents', blank=True, null=True  ,on_delete=models.CASCADE)
    quote_assignee = models.ForeignKey("Contacts", blank=True, null=True  ,on_delete=models.CASCADE)
    notes = models.TextField(blank=True, null=True)
    created_user = models.ForeignKey(User, blank=True, null=True  ,on_delete=models.CASCADE)
    @property
    def created_by(self):
        pass

    def total_price(self):
        return self.get_quote_grand_total()
        #totals = Decimal('0.0')
        #for i in QuoteItems.objects.filter(quote=self):
        #    totals += i.item_price_total
        # return totals

    def get_contacts_from_account(self):
        pass

    def get_pretty_items(self):
        d = {}
        for ig in self.quoteitemgroup_set.select_related():
            d[ig.name] = {}
            for item in ig.quoteitems_set.select_related():
                if item.upgrade_service:
                    if not d[ig.name].get('service_addons'):
                        d[ig.name]['service_addons'] = {}
                    if not d[ig.name]['service_addons'].get(item.upgrade_service.get_service_label()):
                        d[ig.name]['service_addons'][item.upgrade_service.get_service_label()] = []
                    if not item in d[ig.name]['service_addons'][item.upgrade_service.get_service_label()]:
                        d[ig.name]['service_addons'][item.upgrade_service.get_service_label()].append(item)
                else:
                    if not d[ig.name].get('new_service'):
                        d[ig.name]['new_service'] = []
                    if not item in d[ig.name]['new_service']:
                        d[ig.name]['new_service'].append(item) 
        print (d)
        return d

    def __str__(self):
        try:
            return str('Quote %s for %s' % (self.pk, self.account.name))
        except:
            return str('Quote %s for customer ID %s' % (self.pk, self.account.pk))
    def __unicode__(self):
        try:
            return unicode('Quote %s for %s' % (self.pk, self.account.name))
        except:
            return unicode('Quote %s for customer ID %s' % (self.pk, self.account.pk))

    def get_parent_quote_items(self):
        return QuoteItems.objects.filter(quote=self, parent_quote_item=None)

    def get_quote_items(self):
        l = []
        for qg in self.quoteitemgroup_set.select_related():
            d = {qg.name: []}
            for p in qg.items:
                d[qg.name].append(p)
            l.append(d)
        return l

    @property
    def get_account(self):
        return self.account

    @property
    def quote_items(self):
        return self.get_quote_items()

    @property
    def get_order(self):
        try:
            return self.orders_set.select_related()
        except Exception:
            return None

    def has_agent(self):
        if self.agent:
            return self.agent
        return None

    def add_item(self, item, group):
        # shortcut to add an item
        try:
            q=QuoteItems.objects.get(quote=self, product=item)
            return q
        except:
            q=QuoteItems(
                product=item,
                quote=self,
                account=self.account,
                item_group=group
            )
            q.save()
            return q


    def get_quote_grand_total_old(self):
        #itemgroups = self.quoteitemgroup_set.select_related()
        #amt = Decimal('0.00')
        # first get all item groups
        #for i in itemgroups:
        #    for qi in i.quoteitems_set.select_related():
        #        amt += qi.price
        mrc_subtotal = Decimal('0.0')
        nrc_subtotal = Decimal('0.0')
        excluded_acctserv = []

        for items in QuoteItems.objects.filter(quote=self):
            #print items.item_price_total
            if items.product.product_type == 'Product':
                nrc_subtotal += items.item_price_total
            else:
                mrc_subtotal += items.item_price_total
                if items.upgrade_service and not items.product.product_type == 'ServiceAddon':
                    excluded_acctserv.append(items.upgrade_service)

        for a in AccountServices.objects.filter(account=self.account):
            if not a in excluded_acctserv:
                mrc_subtotal += a.price
            for sa in ServiceAddons.objects.filter(service=a):
                mrc_subtotal += sa.price

        return (mrc_subtotal + nrc_subtotal)


    def get_quote_grand_total(self):
        # first get all account service totals
        acct_serv_totals = self.get_account_service_totals()

        # now get all account service addons totals
        acct_serv_addon_totals = self.get_account_service_addon_totals()
 
        # now subtract the downgrade totals diff 
        downgrade_totals = self.get_downgrade_totals_diff()

        # now add the diff for any upgrades
        upgrade_totals = self.get_upgrade_totals_diff()

        # get the disconnect to subtract
        disconnect_totals = self.get_disconnect_totals_diff()

        # nrc product totals
        nrc_totals = self.get_nrc_totals()

        # finally sum all new services and addons
        new_items_total = self.get_new_orders_and_addons_total()

        t = (acct_serv_totals + \
             upgrade_totals + \
             new_items_total + \
             acct_serv_addon_totals +\
             nrc_totals) -\
                (disconnect_totals + downgrade_totals)
        return t


    def get_account_service_totals(self):
        # return total sum of all live active services.
        t = Decimal('0.0')
        for a in AccountServices.objects.filter(account=self.account):
            t += a.price
        print ("acct serv price %s" % t)
        return t


    def get_account_service_addon_totals(self):
        t = Decimal('0.0')
        for sa in ServiceAddons.objects.filter(account=self.account):
            t += sa.price
        print ("serv addon price %s" % t)
        return t


    def get_nrc_totals(self):
        t = Decimal('0.0')
        for qi in QuoteItems.objects.filter(quote=self, purchase_type='nrc-product'):
            t += qi.item_price_total
        print ("nrc totals price %s" % t)
        return t


    def get_downgrade_totals_diff(self):
        # accumulate totals of downgrades - return how much LESS
        # subtract this value from main total
        t = Decimal('0.0')
        for qi in QuoteItems.objects.filter(quote=self, purchase_type='downgrade'):
            t += (qi.upgrade_service.price - qi.item_price_total)
        print ("downgrade totals price %s" % t)
        return t


    def get_upgrade_totals_diff(self):
        # return the value of how much MORE 
        t = Decimal('0.0')
        for qi in QuoteItems.objects.filter(quote=self, purchase_type='upgrade'):
            t += (qi.item_price_total - qi.upgrade_service.price)
        print ("upgrade totals price %s" % t)
        return t


    def get_disconnect_totals_diff(self):
        # return how much LESS 
        t = Decimal('0.0')
        for qi in QuoteItems.objects.filter(quote=self, purchase_type='disconnect'):
            serv = qi.upgrade_service
            if not serv:
                serv = qi.upgrade_service_addon
            t += serv.price
        print ("disconnect totals price %s" % t)
        return t


    def get_new_orders_and_addons_total(self):
        # get total SUM for everything that is not upgrade/downgrade/disconnects
        t = Decimal('0.0')
        for qi in QuoteItems.objects.filter(quote=self, purchase_type='new-order'):
            t += qi.item_price_total
        for qi in QuoteItems.objects.filter(quote=self, purchase_type='service-addon'):
            t += qi.item_price_total
        print ("new-items/addons totals price %s" % t)
        return t


    def download(self, driver, target_path):
        """Download the currently displayed page to target_path."""
        def execute(script, args):
            driver.execute('executePhantomScript',
                           {'script': script, 'args': args})

        # hack while the python interface lags
        driver.command_executor._commands['executePhantomScript'] = ('POST', '/session/$sessionId/phantom/execute')
        # set page format
        # inside the execution script, webpage is "this"
        page_format = 'this.paperSize = {format: "A4", orientation: "portrait" };'
        execute(page_format, [])

        # render current page
        render = '''this.render("{}")'''.format(target_path)
        execute(render, [])


    def save(self, *args, **kwargs):
        # IC-103 - 8/7/2018 - modified and passed quantity
        super(Quotes, self).save(*args, **kwargs)

        if self.quote_status == QUOTE_STATUSES.get('PENDING_CUSTOMER_SIGNATURE'):
            try:
                quote_approvals = QuoteApprovals.objects.get(
                    quote=self, docusign_envelope_id='waiting'
                )
                docsign = run_esign_on_quote(
                    quote_approval=quote_approvals, quote_id=self.pk
                )
                env_id = None
                if docsign:
                    env_id = docsign.document_hash

                if not env_id:
                    env_id = "waiting"
                quote_approvals.docusign_envelope_id = env_id
                quote_approvals.save()

            except QuoteApprovals.DoesNotExist:
                print ("Error with quote approval")
                pass


        if self.quote_status == QUOTE_STATUSES.get('PENDING_CUSTOMER_PRE_APPROVAL'):
            # do stuff
            #from selenium import webdriver
            #driver = webdriver.PhantomJS('phantomjs')
            #driver.get('http://192.112.255.145:9998/pdf/gen/quote/%s' % self.pk)
            #self.download(driver, "/var/www/html/quotes/quote_%s.pdf" % self.pk)
            #pdfhash = PDFHash(account=self.account, token='ab%s' % uuid.uuid4().__str__())
            #pdfhash.save()
            dt = datetime.now()
            datestr = '__%s%s%s-%s%s%s%s' % (
                dt.year, dt.month, dt.day, dt.hour, dt.min, dt.second, dt.microsecond)
            pdf_fn = 'quote-%s%s.pdf' % (self.pk, datestr)
            # get the pdf link
            # ic-234 - update this to a url from configs (get_pdf_url)
            get_pdf_url = '{0}/{1}'.format(settings.GET_PDF_URL, self.pk)
            r = requests.get(get_pdf_url, allow_redirects=True)

            # write the pdf file out to DICK.... i mean diSk
            # ic-234 - update this to config (get_pdf_save_path)
            get_pdf_save_path = '{0}/{1}'.format(settings.GET_PDF_SAVE_PATH, pdf_fn)
            o = open(get_pdf_save_path, 'wb+')
            o.write(r.content)
            o.close()

            try:
                quote_approvals = QuoteApprovals.objects.get(
                        quote=self, docusign_envelope_id='waiting'
                )
                # UPDATE THE NEW PDFF FILENAME
                quote_approvals.pdf_filename =pdf_fn
                quote_approvals.save()
                # always update the latest filename incase
                # it reruns for some reason
                quote_approvals.pdf_filename = pdf_fn
            except QuoteApprovals.DoesNotExist:
                quote_approvals = QuoteApprovals(
                        token=uuid.uuid4().__str__(),
                        docusign_envelope_id='waiting',
                        quote=self,
                        pdf_filename=pdf_fn
                )
                quote_approvals.save()
                #quote_approvals.add_a_note("Added new quote approval object")

            #docsign = run_docusign_on_quote(quote_id=self.pk)
            #env_id = docsign.envelope_id
            #quote_approvals.docusign_envelope_id = env_id
            email_msg = """
%s %s



A quote has been electronically delivered as a PDF attached to this email.


To accept or reject the quote, please click the following link or copy and paste in your browser:

%s%s

If you have any questions or concerns please contact your sales agent.


Thank you for your interest in ITP!

            """ % (self.quote_assignee.firstname,
                   self.quote_assignee.lastname,
                   settings.QUOTE_APPROVAL_URL, 
                   quote_approvals.token)

            email = EmailMessage(
                'New Quote for %s' %  self.account.name,
                email_msg,
                'donotreply@itpscorp.com',
                [self.quote_assignee.email],
            )
            email.attach_file('/var/www/html/quotes/%s' % pdf_fn)
            email.send(fail_silently=False)


        if self.quote_status == QUOTE_STATUSES.get('CUSTOMER_APPROVED'):
            # when its time to make the ORDER from the quote
            if not self.orders_set.select_related():
                # create the order if customer approved 
                # and not already created
                #self.quote_order_status = 'Pending Order Processing'
                try:
                    orders = Orders.objects.get(status='quote',
                                                account=self.account,
                                                deleted=False,
                                                quote=self,
                                                agent=self.agent,
                                                notes=self.notes)
                except Orders.DoesNotExist:
                    orders = Orders(
                        status='quote',
                        account=self.account,
                        date_created=datetime.now(),
                        deleted=False,
                        quote=self,
                        agent=self.agent,
                    )
                    orders.save()
                    qi_to_oi_mapper = {}
                    for ig in self.quoteitemgroup_set.select_related():
                        oig = OrderItemGroup(name=ig.name, order=orders)
                        oig.save()
                        # IC-43 need to update this to write the groups
                        print(ig.quoteitems_set.select_related())
                        for prd in ig.quoteitems_set.select_related():
                            print("AutoORDER - ITEM {0}".format(prd.pk))
                            total_item_price = prd.item_price_total
                            quantity = prd.quantity
                            repeat_orderitem_creation = int(1)
                            #prdprice = prd.product.price
                            prdmeta = prd.metadata
                            if prd.metadata:
                                if prd.metadata.get('multi_quantity', False):
                                    quantity = int(1)
                                    repeat_orderitem_creation = prd.quantity
                                    total_item_price = prd.price

                            for n in range(0, repeat_orderitem_creation):
                                if not prd.parent_quote_item:
                                    provision_status = 'pending-provision'
                                    if prd.auto_provision:
                                        provision_status='pending-provision'
                                    oi = OrderItems(
                                            order=orders,
                                            quantity=quantity,
                                            product=prd.product,
                                            item_group=oig,
                                            auto_provision=prd.auto_provision,
                                            prov_status=False,
                                            item_type=prd.product.product_key.name,
                                            price_per_unit=prd.price,
                                            price=total_item_price,
                                            next_bill=datetime.now() + monthdelta.monthdelta(1),
                                            metadata=prd.metadata,
                                            provision_status=provision_status,
                                            purchase_type=prd.purchase_type,
                                            upgrade_service=prd.upgrade_service,
                                            service_address=prd.service_address,
                                            req_activation_date=prd.req_activation_date,
                                            record_created=datetime.now(),
                                            contract_terms=prd.contract_terms,
                                            prod_description=prd.prod_description,
                                    )
                                    oi.save()
                                    qi_to_oi_mapper['pk%s' % prd.pk] = oi

                    #print (qi_to_oi_mapper) 
                    # IC-215 - reiterate now so we can onlyu process the ones that have a parent quote item
                    # and map it to the mapper
                    for ig in self.quoteitemgroup_set.select_related():
                        for prd in ig.quoteitems_set.select_related():
                            if prd.product.product_type == 'ServiceAddon':
                                if prd.parent_quote_item:
                                    parent_oi = qi_to_oi_mapper.get('pk%s'%prd.parent_quote_item.pk)
                                    if parent_oi:
                                        quantity = prd.quantity
                                        repeat_orderitem_creation = int(1)
                                        #prdprice = prd.product.price
                                        prdmeta = prd.metadata
                                        if prd.metadata:
                                            if prd.metadata.get('multi_quantity', False):
                                                quantity = int(1)
                                                repeat_orderitem_creation = prd.quantity
                                                total_item_price = prd.price

                                        for n in range(0, repeat_orderitem_creation):
                                            provision_status = 'pending-provision'
                                            if prd.auto_provision:
                                                provision_status='pending-provision'
                                            oi = OrderItems(
                                                    order=orders,
                                                    quantity=quantity,
                                                    product=prd.product,
                                                    item_group=oig,
                                                    auto_provision=prd.auto_provision,
                                                    prov_status=False,
                                                    item_type=prd.product.product_key.name,
                                                    price_per_unit=prd.price,
                                                    price=total_item_price,
                                                    next_bill=datetime.now() + monthdelta.monthdelta(1),
                                                    metadata=prd.metadata,
                                                    provision_status=provision_status,
                                                    purchase_type=prd.purchase_type,
                                                    upgrade_service=prd.upgrade_service,
                                                    service_address=prd.service_address,
                                                    req_activation_date=prd.req_activation_date,
                                                    record_created=datetime.now(),
                                                    contract_terms=prd.contract_terms,
                                                    prod_description=prd.prod_description,
                                                    parent_order_item=parent_oi,
                                                    agent=prd.agent,
                                            )
                                            oi.save()
                                    else:
                                        print ("Error adding related serviceaddon to NEW orderitem from copy quote-to-order")
                                        print ('prd%s'%prd.parent_quote_item.pk)
                                        print (qi_to_oi_mapper)

                    # Order items have been added, time to run provisioner:
                    # Call Provisioner Endpoint to process order immediately
                    print("Running provision on this order:")
                    ois = OrderItems.objects.filter(order=orders)
                    print(ois)
                    print(ois[0].provision_status)
                    apikey = settings.AUTH_API_KEY
                    url = settings.ITP_BASE_API_URL + "/api/provisioner/provision?order-id=%s" % orders.id
                    headers = {
                    'apikey': apikey,
                    'cache-control': "no-cache",
                    }
                    requests.request("GET", url, headers=headers)



class QuoteItems(models.Model):
    quote = models.ForeignKey('Quotes', on_delete=models.CASCADE)
    item_group = models.ForeignKey('QuoteItemGroup', blank=True, null=True  , on_delete=models.CASCADE)
    account = models.ForeignKey('Accounts', blank=True, null=True   , on_delete=models.CASCADE)
    product = models.ForeignKey('Products', blank=True, null=True ,on_delete=models.CASCADE)
    metadata = JSONField(blank=True, null=True)
    price_per_unit = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    price_changed = models.NullBooleanField(default=False, blank=True, null=True)
    auto_provision = models.NullBooleanField(default=False, blank=True, null=True)
    purchase_type = models.CharField(max_length=20, default='new-order', choices=PURCHASE_TYPES)
    upgrade_service = models.ForeignKey("AccountServices", blank=True, null=True ,on_delete=models.CASCADE)
    upgrade_service_addon = models.ForeignKey("ServiceAddons", blank=True, null=True  ,on_delete=models.CASCADE)
    service_address = models.ForeignKey("ServiceAddress", blank=True, null=True  ,on_delete=models.CASCADE)
    quantity = models.IntegerField(blank=True, null=True)
    item_price_total = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    req_activation_date = models.DateTimeField(default=datetime.now())
    # this is for binding a service addon to a NEW (unprovisioned/incomplete) quote item (service)
    parent_quote_item = models.ForeignKey('QuoteItems', blank=True, null=True ,on_delete=models.CASCADE)
    record_created = models.DateTimeField(default=datetime.now())
    prod_description = models.TextField(blank=True, null=True)
    contract_terms = models.IntegerField(null=True)
    taxable = models.NullBooleanField(blank=True, null=True, default=False)
    tax_percent = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))

    @property
    def item_type(self):
        return self.product.product_key.name

    # IC-103 - add quantity column - 8/2018
    @property
    def product_name(self):
        return self.product.name

    def get_related_addons(self):
        # get related quoteitem that are addons and have 
        # this (self) as a parent quote item
        return QuoteItems.objects.filter(parent_quote_item=self)

    def get_quote_description(self):
        # caesar told me to hardcode this :)
        # ic-196 - adding check for self.prod_description which should
        # overwrite description here.
        if self.prod_description:
            return self.prod_description

        if self.product.product_type == 'ServiceAddon':
            if self.product.part_number == 'itpvoice_pbx_did':
                s = ''
                if self.metadata.get('city') and self.metadata.get('state'):
                    s =  '%s - city/state: %s, %s' % (
                            self.product.description, self.metadata.get('city'), self.metadata.get('state'))
                if self.metadata.get('did'):
                    s += ' did: %s' % self.metadata.get('did')
                return s

            if self.product.part_number == 'itpvoice_pbx_did_port':
                # new phone number - rate_center: v
                return '%s - %s' % (self.product.description, self.metadata.get('did'))

        else:
            if self.product.part_number == 'itp_voice':
                desc = 'Hosted PBX - %s users' % (self.quantity)
                return desc
        return self.product.description

    def get_total(self):
        return self.price * self.quantity

    def delete(self, *args, **kwargs):
        for d in QuoteItems.objects.filter(parent_quote_item=self):
            d.delete()
        super(QuoteItems, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        if not self.product:
            if not self.upgrade_service:
                if not self.parent_quote_item:
                    if not self.upgrade_service_addon:
                        raise Exception("Error adding quote item - "
                                    "No product, upgrade service, or parent quote item passed")
            if self.upgrade_service:
                self.product = self.upgrade_service.product
            if self.parent_quote_item:
                self.product = self.parent_quote_item.product
            if self.upgrade_service_addon:
                self.product = self.upgrade_service_addon.product

        
        #if not self.product:
        #    raise Exception("Error adding quote item - "
        #                    "No product, upgrade service, or parent quote item passed")

        if not self.prod_description:
            self.prod_description = self.get_quote_description()

        #if not self.item_price_total:
        self.item_price_total = self.price * int(self.quantity)

        if self.product.product_type in ["Service", "ServiceAddon"]:
            try:
                ig = QuoteItemGroup.objects.get(quote=self.quote, name="mrc")
            except QuoteItemGroup.DoesNotExist:
                ig = QuoteItemGroup(quote=self.quote, name="mrc")
                ig.save()
            self.item_group = ig

        if self.product.product_type in ['Product']:
            try:
                ig = QuoteItemGroup.objects.get(quote=self.quote, name="nrc")
            except QuoteItemGroup.DoesNotExist:
                ig = QuoteItemGroup(quote=self.quote, name="nrc")
                ig.save()
            self.item_group = ig


        p = self.product.auto_provision
        self.auto_provision = p
        print (self.price)
        if not self.price:
            self.price = self.product.price
        
        # use from the account service instead of the latest product object price
        #if self.upgrade_service:
        #    print "USING ACCOUNT SERVICE PRICE"
        #    self.price_per_unit = self.upgrade_service.price_per_unit
        #    self.price = self.upgrade_service.price_per_unit

        # if the price per item changed, then make sure its noted to 
        # force manager approval
        agent = self.quote.has_agent()

        if agent:
            thr = agent.pricing_threshold
            if self.price != self.product.price:
                dif = abs(self.price - self.product.price) / self.product.price * 100
                self.price_changed = True
                if dif > thr:
                    # pending manager approval 
                    self.quote.quote_status = QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED')
                    self.quote.save()
        else:
            # not an agent 
            if self.price != self.product.price:
                self.price_changed = True
                self.quote.quote_status = QUOTE_STATUSES.get('MANAGER_APPROVAL_REQUIRED')
                self.quote.save()
            else:
                self.price_changed = False
        
        if not self.metadata:
            self.metadata = self.product.metadata


        super(QuoteItems, self).save(*args, **kwargs)


class QuoteItemGroup(models.Model):
    name = models.CharField(max_length=35, blank=True, null=True)
    quote = models.ForeignKey('Quotes'  ,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)

    @property
    def items(self):
        return self.quoteitems_set.select_related()

    def get_items_for_template(self):
        d = {}
        for item in self.items:
            d = {}
            if item.upgrade_service:
                d['service_addon'] = True
                d['item'] = item



class Orders(models.Model):
    def __str__(self):
        return str("Order %s" % self.pk)
    def __unicode__(self):
        return unicode("Order %s" % self.pk)
    # cannot add total price here cause sometimes a  product on this order might be a one time charge.
    # the order items will store thsi data. 
    status = models.CharField(max_length=15, choices=ORDER_STATS)
    account = models.ForeignKey('Accounts'  ,on_delete=models.CASCADE)
    date_created = models.DateTimeField(default=datetime.now())
    deleted = models.NullBooleanField(default=False)
    quote = models.ForeignKey('Quotes', blank=True, null=True ,on_delete=models.CASCADE)
    agent = models.ForeignKey('Agents', blank=True, null=True ,on_delete=models.CASCADE)
    assigned_user = models.ForeignKey(User, blank=True, null=True ,on_delete=models.CASCADE)
    order_number = models.CharField(max_length=15, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    # price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00))
    @property
    def unassigned(self):
        if not self.assigned_user:
            return True
        return False
    @property
    def get_items(self):
        return self.orderitems_set.select_related()
    @property
    def db_quote_id(self):
        return self.quote.pk
    @property
    def db_account_id(self):
        return self.account.pk
    @property
    def db_agent_id(self):
        return self.agent.pk

    def get_parent_quote_items(self):
        return OrderItems.objects.filter(order=self, parent_order_item=None)

    def save(self, *args, **kwargs):
        if not self.order_number:
            if not Orders.objects.last():
                self.order_number = 'ORD-1000'
            else:
                olast = Orders.objects.last()
                onum = olast.order_number.split('-')[1]
                onum2 = int(onum) + 1
                ordernum2 = 'ORD-%s' % str(onum2)
                self.order_number = ordernum2

        super(Orders, self).save(*args, **kwargs)


BILL_INTERVALS = (
    ('monthly', 'monthly',),
    ('one-time', 'one-time'),
)
PROV_STATUS = (
    ('provision-in-progress', 'provision-in-progress',),
    ('pending-provision', 'pending-provision',),
    ('unprovisioned', 'unprovisioned',),
    ('provision-err', 'provision-err',),
    ('provisioned', 'provisioned',),
)
class OrderItems(models.Model):
    """
    Order items will belong to an order
    bill_interval - if this is set to one time, check the payment_complete option as well
    and if it is false, then charge the user for this and then set to payment_complete when done
    which will only allow this item to be charged one time. 
    If the bill-inteval is monthly, continue charging this until the status is not active
    """
    def __str__(self):
        return str("Order Item - %s" % (self.product.name))
    def __unicode__(self):
        return unicode("Order Item - %s" % (self.product.name))
    product = models.ForeignKey('Products', blank=True, null=True  ,on_delete=models.CASCADE)
    order = models.ForeignKey('Orders'  ,on_delete=models.CASCADE)
    item_group = models.ForeignKey('OrderItemGroup' ,on_delete=models.CASCADE)
    item_type = models.CharField(max_length=50, blank=True, null=True)
    price_per_unit = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    bill_interval = models.CharField(max_length=10, blank=True, null=True, default='monthly')
    next_bill = models.DateTimeField(default=datetime.now())
    metadata = JSONField(blank=True, null=True)
    # this field will keep track of the payment complete for one-time charges or 
    # any charges that are outside the original monthly service charge. 
    payment_complete = models.NullBooleanField(default=False)
    auto_provision = models.NullBooleanField(default=False, null=True, blank=True)
    # todo delete prov_status when we are not working on three screens at the same time. 
    prov_status = models.NullBooleanField(default=False, null=True, blank=True)
    provision_status = models.CharField(max_length=30, blank=True, null=True, choices=PROV_STATUS)
    provision_result = models.TextField(blank=True, null=True)
    request_activation_date = models.DateTimeField(default=datetime.now() + timedelta(days=10))
    completed = models.NullBooleanField(default=False)
    purchase_type = models.CharField(max_length=20, default='new-order', choices=PURCHASE_TYPES)
    upgrade_service = models.ForeignKey("AccountServices", blank=True, null=True ,on_delete=models.CASCADE)
    service_address = models.ForeignKey("ServiceAddress", blank=True, null=True , on_delete=models.CASCADE)
    quantity = models.IntegerField(blank=True, null=True)
    req_activation_date = models.DateTimeField(default=datetime.now())
    # activation date is set by the second script of the provisoiner
    activation_date = models.DateTimeField(default=datetime.now())
    # date that the record was created in the DB (internal)
    record_created = models.DateTimeField(default=datetime.now())
    prod_description = models.TextField(blank=True, null=True)
    contract_terms = models.IntegerField(null=True)
    taxable = models.NullBooleanField(blank=True, null=True, default=False)
    tax_percent = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    parent_order_item = models.ForeignKey('OrderItems', blank=True, null=True ,on_delete=models.CASCADE)

    def get_related_addons(self):
        return OrderItems.objects.filter(parent_quote_item=self)

    # IC-103 - add quantity column - 8/2018

    def get_quote_description(self):
        # caesar told me to hardcode this :)
        # print self.product.name
        # print self.product.product_type

        if self.product.product_type == 'ServiceAddon':
            # print "STICK IT IN"
            if self.product.part_number == 'itpvoice_pbx_did':
                return '%s - rate center: %s' % (
                        self.product.description, self.metadata.get('rate_center'))
            if self.product.part_number == 'itpvoice_pbx_did_port':
                # new phone number - rate_center: v
                return '%s - %s' % (self.product.description, self.metadata.get('porting_dids'))
        else:
            if self.product.part_number == 'itp_voice':
                desc = 'Hosted PBX - %s users' % (self.quantity)
                return desc
        return self.product.description

    def mark_complete(self):
        # mark complete?
        if self.auto_provision and self.provision_status == 'provisioned' and not self.completed:
            return True
        return False

    def show_provision(self):
        """Determine if we should show the "provision" button which 
        will send this to the provisioner API"""
        if self.provision_status == 'unprovisioned' and self.auto_provision:
            return True
        return False

    def manual_approve_provision(self):
        """Determine if orderitem is elieible for manual provision"""
        try:
            ac = AccountServices.objects.get(oi=self)
        except AccountServices.DoesNotExist:
            ac = None
        manual_approve_provision = False
        if not self.auto_provision:
            if not ac:
                if self.provision_status == 'unprovisioned':
                    manual_approve_provision = True
        return manual_approve_provision
        

    def update_metadata(self, k, v):
        # update the voice api?
        m = self.metadata
        m[k] = v

        self.metadata = m
        return True

    @property
    def notes(self):
        return self.orderitemnotes_set.select_related()

    def save(self, *args, **kwargs):
        if self.quantity:
            try:
                self.metadata['purchased_users'] = int(self.quantity)
            except Exception as e:
                print ("Error save orderitem setting purchased users: %s" % str(e))

        self.item_type = self.product.item_type
        
        super(OrderItems, self).save(*args, **kwargs)
        if self.auto_provision and self.provision_status == 'provisioned' and self.upgrade_service:
            if not self.accountservices_set.select_related():
                acs = ServiceAddons(
                    service=self.upgrade_service,
                    account=self.order.account,
                    product=self.product,
                    order=self.order,
                    oi=self,
                    price_per_unit=self.price_per_unit,
                    price=self.price,
                    bill_interval=self.bill_interval,
                    next_bill=self.next_bill,
                    metadata=self.metadata,
                    payment_complete=self.payment_complete,
                    auto_provision=self.auto_provision,
                    provision_status='provisioned',
                    activation_date=self.activation_date,
                    req_activation_date=self.request_activation_date,
                    api_id=self.metadata.get('api_id'),
                    item_type=self.product.part_number,
                )
                acs.save()



# from-prov, from-agent, from-user
NOTE_TYPES = (
    ('from-prov', 'from-prov',),
    ('from-agent', 'from-agent',),
    ('from-user', 'from-user',),
)
class OrderItemNotes(models.Model):
    orderitem = models.ForeignKey('OrderItems' ,on_delete=models.CASCADE)
    user = models.ForeignKey(User  ,on_delete=models.CASCADE)
    note_type =  models.CharField(max_length=30, choices=NOTE_TYPES)
    msg = models.TextField(blank=True, null=True)
    created = models.DateTimeField(default=datetime.now())



class OrderItemGroup(models.Model):
    name = models.CharField(max_length=36, blank=True, null=True)
    order = models.ForeignKey('Orders'  ,on_delete=models.CASCADE)
    @property
    def items(self):
        return self.orderitems_set.select_related()



class AccountServices(models.Model):
    """
    Order items will belong to an order
    bill_interval - if this is set to one time, check the payment_complete option as well
    and if it is false, then charge the user for this and then set to payment_complete when done
    which will only allow this item to be charged one time. 
    If the bill-inteval is monthly, continue charging this until the status is not active
    """
    account = models.ForeignKey('Accounts', on_delete=models.CASCADE)
    product = models.ForeignKey('Products', on_delete=models.CASCADE)
    order = models.ForeignKey('Orders', blank=True, null=True, on_delete=models.CASCADE)
    oi = models.ForeignKey('OrderItems', blank=True, null=True, on_delete=models.CASCADE)
    service_address = models.ForeignKey("ServiceAddress", blank=True, null=True, on_delete=models.CASCADE)
    item_type = models.CharField(max_length=50, blank=True, null=True)
    price_per_unit = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    bill_interval = models.CharField(max_length=10, blank=True, null=True, default='monthly')
    next_bill = models.DateTimeField(default=datetime.now())
    metadata = JSONField(blank=True, null=True)
    # this field will keep track of the payment complete for one-time charges or 
    # any charges that are outside the original monthly service charge. 
    payment_complete = models.NullBooleanField(default=False)
    auto_provision = models.NullBooleanField(default=False, null=True, blank=True)
    # todo delete prov_status when we are not working on three screens at the same time. 
    prov_status = models.NullBooleanField(default=False, null=True, blank=True)
    provision_status = models.CharField(max_length=30, blank=True, null=True, choices=PROV_STATUS)
    provision_result = models.TextField(blank=True, null=True)
    api_id = models.TextField(blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    req_activation_date = models.DateTimeField(default=datetime.now())
    # this gets copeied from the orderitem.activation_date
    activation_date = models.DateTimeField(default=None, blank=True, null=True)
    disconnect_date = models.DateTimeField(default=None, blank=True, null=True)
    prod_description = models.TextField(blank=True, null=True)
    contract_terms = models.IntegerField(null=True, default=0)

    def __str__(self):
        s = '{0} - {1} @ {2}'.format(self.account.name, self.product.name, self.service_address.get_address_pretty())
        return str(s)

    def __unicode__(self):
        s = '{0} - {1} @ {2}'.format(self.account.name, self.product.name, self.service_address.get_address_pretty())
        return unicode(s)

    @property
    def product_name(self):
        return self.product.name

    def product_type(self):
        return self.product.product_type

    def product_part_number(self):
        return self.product.product_key.name

    def product_description(self):
        return self.product.description

    def get_service_label(self):
        return '#%s %s for %s' % (self.order.order_number, self.product.name, self.account.name)

    def get_service_addons(self):
        return ServiceAddons.objects.filter(service=self)

    def get_service_address(self):
        # ic-140
        # previously from orderitems
        return self.service_address

    def pending_items(self, purchase_type='new-order', quote_id=None):
        filters = {
            'upgrade_service': self,
            'purchase_type': purchase_type
        }
        if quote_id:
            qq = Quotes.objects.get(pk=quote_id)
            filters['quote'] = qq

        q = QuoteItems.objects.filter(**filters)
        if q:
            return q[0]
        return []

    def pending_upgrades(self, quote_id=None):
        if not quote_id:
            return []
        return self.pending_items(purchase_type='upgrade', quote_id=quote_id)

    def pending_downgrades(self, quote_id=None):
        if not quote_id:
            return []
        return self.pending_items(purchase_type='downgrade', quote_id=quote_id)

    def pending_disconnect(self, quote_id=None):
        if not quote_id:
            return []
        return self.pending_items(purchase_type='disconnect', quote_id=quote_id)

    def get_quote_description(self):
        # caesar told me to hardcode this :)
        # print self.product.name
        # print self.product.product_type

        if self.product.product_type == 'ServiceAddon':
            # print "STICK IT IN"
            if self.product.part_number == 'itpvoice_pbx_did':
                return '%s - rate center: %s' % (
                        self.product.description, self.metadata.get('rate_center'))
            if self.product.part_number == 'itpvoice_pbx_did_port':
                # new phone number - rate_center: v
                return '%s - %s' % (self.product.description, self.metadata.get('porting_dids'))
        else:
            if self.product.part_number == 'itp_voice':
                desc = 'Hosted PBX - %s users' % (self.quantity)
                return desc
        return self.product.description

    def save(self, *args, **kwargs):
        if not self.activation_date:
            if self.metadata.get('enabled'):
                self.activation_date = datetime.now()

        if not self.service_address:
            if self.oi.service_address:
                self.service_address = self.oi.service_address

        self.item_type = self.product.item_type
        super(AccountServices, self).save(*args, **kwargs)




class ServiceAddons(models.Model):
    service = models.ForeignKey('AccountServices'  ,on_delete=models.CASCADE)
    account = models.ForeignKey('Accounts', on_delete=models.CASCADE, blank=True, null=True)
    product = models.ForeignKey('Products', on_delete=models.CASCADE)
    order = models.ForeignKey('Orders', blank=True, null=True ,on_delete=models.CASCADE)
    oi = models.ForeignKey('OrderItems', blank=True, null=True, on_delete=models.CASCADE)
    item_type = models.CharField(max_length=50, blank=True, null=True)
    price_per_unit = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    price = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.00'))
    bill_interval = models.CharField(max_length=10, blank=True, null=True, default='monthly')
    next_bill = models.DateTimeField(default=datetime.now())
    metadata = JSONField(blank=True, null=True)
    req_activation_date = models.DateTimeField(default=None, blank=True, null=True)
    activation_date = models.DateTimeField(default=None, blank=True, null=True)
    disconnect_date = models.DateTimeField(default=None, blank=True, null=True)
    # this field will keep track of the payment complete for one-time charges or 
    # any charges that are outside the original monthly service charge. 
    payment_complete = models.NullBooleanField(default=False)
    auto_provision = models.NullBooleanField(default=False, null=True, blank=True)
    # todo delete prov_status when we are not working on three screens at the same time. 
    prov_status = models.NullBooleanField(default=False, null=True, blank=True)
    provision_status = models.CharField(max_length=30, blank=True, null=True, choices=PROV_STATUS)
    provision_result = models.TextField(blank=True, null=True)
    api_id = models.TextField(blank=True, null=True)
    prod_description = models.TextField(blank=True, null=True)

    def __str__(self):
        return str("Service addon pk %s" % self.pk)

    def __unicode__(self):
        return unicode("Service addon pk %s" % self.pk)

    def product_name(self):
        return self.product.name

    def product_description(self):
        return self.product.description

    def get_quote_description(self):
        if self.product.part_number == 'itpvoice_pbx_did': 
            return "DID - %s" % (self.metadata.get('did'))
        if self.product.part_number == 'itvoice_pbx_did_port': 
            return "Number Port"

    def pending_items(self, purchase_type='new-order', quote_id=None):
        """
        Get pending items related to this active service addon
        Similar to the accountservice method except this one
        checks for the upgrade_service_addon field instead of
        the upgrade_service field
        """
        itemlist = []
        q = QuoteItems.objects.filter(
                upgrade_service_addon=self,
                purchase_type=purchase_type)
        for item in q:
            if item.quote.pk == quote_id:
                #if item.quote.quote_status == QUOTE_STATUSES.get('DRAFT'):
                return item
        if not quote_id:
            return q[0]
        return []

    def get_service_addons(self):
        return []

    def pending_disconnect(self, quote_id=None):
        return self.pending_items(purchase_type='disconnect', quote_id=quote_id)

    def save(self, *args, **kwargs):
        self.order = self.service.order
        if not self.account:
            self.account = self.service.account
        self.item_type = self.product.product_key.name
        super(ServiceAddons, self).save(*args, **kwargs)






BLDG_STAT= (
        ('UNAVAILABLE', 'UNAVAILABLE'),
        ('AVAILABLE', 'AVAILABLE'),
        ('LIT', 'LIT'),
        ('LIT REQUIRES CONSTRUTION', 'LIT REQUIRES CONSTRUCTION'),
        ('AVAILABLE REQUIRES CONSTRUCTION', 'AVAILABLE REQUIRES CONSTRUCTION'),
)
class ServiceAddress(models.Model):
    
    lprop_id = models.IntegerField(null=True, blank=True)
    addressid = models.IntegerField(null=True, blank=True)
    folio  = models.TextField(blank=True, null=True)
    point_x = models.FloatField(blank=True, null=True)
    point_y = models.FloatField(blank=True, null=True)
    mailing_mu = models.TextField(blank=True, null=True)

    hse_num = models.IntegerField(blank=True, null=True)
    pre_dir = models.TextField(blank=True, null=True)
    st_name = models.TextField(blank=True, null=True)
    st_type = models.TextField(blank=True, null=True)
    suf_dir = models.TextField(blank=True, null=True)
    zip = models.IntegerField(blank=True, null=True)
    plus4 = models.IntegerField(blank=True, null=True)
    munic = models.IntegerField(blank=True, null=True)
    sname = models.TextField(blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    validated = models.NullBooleanField(default=False, blank=True, null=True)
    suite_num = models.CharField(max_length=10, blank=True, null=True)
    itpfiber_status = models.CharField(max_length=45, default="UNAVAILABLE", blank=True, null=True, choices=BLDG_STAT)
    full_address = models.TextField(blank=True, null=True)
    lat = models.TextField(blank=True, null=True)
    lon = models.TextField(blank=True, null=True)
    # tell if address was updated to resync all geocode shit
    updated_address = models.NullBooleanField(default=False, blank=True, null=True)
    geo_data = JSONField(blank=True, null=True)

    def __str__(self):
        #return str('%s %s %s %s %s %s %s %s' % (
        #    self.hse_num, self.pre_dir, self.st_name, self.st_type,
        #    self.suite_num, self.city, self.state, self.zip))
        #return str(self.pretty_print_address)
        s = '{0}'.format(self.full_address)
        if self.suite_num:
            s += ' {0}'.format(self.suite_num)
        return s

    def __unicode__(self):
        return unicode(self.pretty_print_address)

    def get_long_linear_address(self):
        fulladdr = self.full_address
        s = '{0}'.format(fulladdr)
        if self.suite_num:
            s += ' {0}'.format(self.suite_num)
        s += ' {0}, {1}, {2}'.format(self.city, self.state, self.zip)
        return s

    
    def get_address_pretty(self):
        fulladdr = self.full_address
        s = '{0}'.format(fulladdr)
        if not fulladdr:
            s = self.pretty_print_address()
        else:
            if self.suite_num:
                s += ' {0}'.format(self.suite_num)
        return s

    @property
    def pretty_print_address(self):
        # get fields, print them, be cute.
        s = '{0} {1} {2} {3} {4} {5}, {6} {7}'.format(
            self.hse_num,
            self.pre_dir,
            self.st_name,
            self.st_type,
            str(self.suite_num).upper(),
            self.city,
            str(self.state).upper(),
            self.zip,
        )
        return s.replace('NONE ', '')


    def get_lat(self):
        try:
            return self.geo_data[0].get('lat')
        except Exception:
            return None

    def get_lon(self):
        try:
            return self.geo_data[0].get('lon')
        except Exception:
            return None


    def oldsave(self, *args, **kwargs):
        """ DEPRECATED """
        s = ''
        if self.hse_num:
            s += str(self.hse_num) + ' '
        if self.pre_dir:
            s += str(self.pre_dir) + ' '
        if self.st_name:
            s += str(self.st_name) + ' '
        if self.st_type:
            s += str(self.st_type) + ' '
        if self.suf_dir:
            s += str(self.suf_dir) + ' '
        if self.suite_num:
            s += '#' + str(self.suite_num) + ' '
        if s:
            self.full_address = s

        if not self.geo_data:
            addr = self.full_address
            url = 'http://nominatim.openstreetmap.org/search?format=json&q='
            url += addr
            url += ' %s FL %s' % (self.city, self.zip)
            r = requests.get(url)
            try:
                j = json.loads(r.text)
            except Exception as e:
                j = {"result": "failed", "message": "exception %s" % str(e)}
            self.geo_data = j

        super(ServiceAddress, self).save(*args, **kwargs)

    
    def get_address_geocode_osm(self, addr):
        # get geocode data from openstreet maps (lookups by osm id)
        url = 'http://nominatim.openstreetmap.org/search?format=json&q='
        url += addr
        r = requests.get(url)
        logger.debug("ServiceAddress.get_address_geocode()")
        logger.debug("- search url request: {0}".format(url))
        try:
            logger.debug("Response: {0}".format(r.text))
            j = json.loads(r.text)
            place_id = j[0]['place_id']
            place_url = 'https://nominatim.openstreetmap.org/details?format=json&place_id={0}'.format(place_id)
            logger.debug("- url details: {0}".format(place_url))
            place_response = requests.get(place_url)
            logger.debug("Response: {0}".format(place_response.text))
            place_json = json.loads(place_response.text)
            osm_id = '{0}{1}'.format(place_json.get('osm_type'), place_json.get('osm_id'))
            osm_url = 'https://nominatim.openstreetmap.org/lookup?format=json&osm_ids={0}'.format(osm_id)
            logger.debug("- url osm lookup: {0}".format(osm_url))
            osm_lookup_response = requests.get(osm_url)
            logger.debug("Response: {0}".format(osm_lookup_response))
            osm_response_json = json.loads(osm_lookup_response.text)
            #raise Exception(osm_response_json)

            self.full_address = '{0} {1}'.format(
                place_json.get('addresstags').get('housenumber'),
                place_json.get('addresstags').get('street'),
            )
            #if self.suite_num:
            #    self.full_address += ' #{0}'.format(self.suite_num)
            return place_json, osm_response_json[0]
        except Exception as e:
            logger.error("Error Geocoding {0}".format(addr))
            logger.debug("^ Exception message: {0}".format(str(e)))
            logger.debug("Geocode Response: {0}")
            j = {"result": "failed", "message": "exception %s" % str(e)}
            return j


    def get_address_geocode_google(self, addr):
        gurl = "https://maps.google.com/maps/api/geocode/json?"
        gurl += "key={0}".format(settings.GOOGLE_API_KEY_MAPS)
        gurl += "&address={0}".format(addr)
        r = requests.get(gurl)
        
        j = json.loads(r.text)
        self.geo_data = j
        if j.get('results'):
            jj = j.get('results')[0]
            loc = jj.get('geometry', {}).get('location', {})
            self.lat = loc.get('lat')
            self.lon = loc.get('lng')
            address_components = jj.get('address_components')
            for ac in address_components:
                if 'locality' in ac.get('types') and 'political' in ac.get('types'):
                    self.city = ac.get('long_name')
                if 'postal_code' in ac.get('types'):
                    self.zip = ac.get('long_name')
                if 'administrative_area_level_1' in ac.get('types') and 'political' in ac.get('types'):
                    self.state = ac.get("short_name")
                if 'street_number' in ac.get('types'):
                    self.hse_num = ac.get('short_name')

         

    def save(self, *args, **kwargs):
        
        if not self.geo_data: 
            addr = self.full_address
            #raise Exception(addr)
            self.get_address_geocode_google(addr)
            self.full_address = addr.split(',')[0]
           
            
            #
            """place_result, osm_result = self.get_address_geocode(addr)
            #print(result)
            self.geo_data = {"place": place_result, "osm": osm_result}
            self.city = place_result.get('addresstags').get('city')
            if not place_result.get('addresstags').get('city'):
                self.city = osm_result.get('address').get('suburb')
            self.hse_num = osm_result.get('address').get('house_number')
            self.zip = place_result.get('addresstags').get('postcode')
            self.state = osm_result.get('address').get('state')
            self.lat = osm_result.get('lat')
            self.lon = osm_result.get('lon')"""
        
            
        super(ServiceAddress, self).save(*args, **kwargs)
        # THIS IS GONNA SUCK LATER
        sa = ServiceAddress.objects.filter(full_address=self.full_address)
        for s in sa:
            if not s == self:
                itpserv = ITPServices_Addresses.objects.filter(address=s)
                for i in itpserv:
                    itpservget = ITPServices_Addresses.objects.filter(address=self, product=i.product)
                    if not itpservget:
                        new_sa = ITPServices_Addresses(address=self, product=i.product)
                        new_sa.save()



class ITPServices_Addresses(models.Model):
    # address table id
    # prod id
    address = models.ForeignKey('ServiceAddress'  , on_delete=models.CASCADE)
    product = models.ForeignKey('Products'  ,on_delete=models.CASCADE)


class AgentOwnedBy(models.Model):
    """
    This class model defines which (sub) agents are owned 
    by which (master) agents
    """
    master_agent = models.ForeignKey('Agents', related_name='master_agent'  , on_delete=models.CASCADE)
    sub_agent = models.ForeignKey('Agents', related_name='sub_agent'  ,on_delete=models.CASCADE)


AGENT_TYPES = (
    ('sub', 'sub'),
    ('master', 'master'),)
class Agents(models.Model):
    """
    One to one relation to the (current) voice api Users table
    which will be moved to the auth api 
    """
    def __str__(self):
        return str("%s %s (%s)" % (
            self.firstname, self.lastname, self.agent_type))
    def __unicode__(self):
        return unicode("%s %s (%s)" % (
            self.firstname, self.lastname, self.agent_type))
    #user = models.ForeignKey(User, blank=True, null=True)
    companyname = models.CharField(max_length=50, blank=True, null=True)
    firstname = models.CharField(max_length=30, blank=True, null=True)
    lastname = models.CharField(max_length=30, blank=True, null=True)
    title = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    primary_street = models.CharField(max_length=150, blank=True, null=True)
    primary_city = models.CharField(max_length=100, blank=True, null=True)
    primary_state = models.CharField(max_length=100, blank=True, null=True)
    primary_zipcode = models.CharField(max_length=50, blank=True, null=True)
    primary_country = models.CharField(max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)
    mobile = models.CharField(max_length=15, blank=True, null=True)
    fax = models.CharField(max_length=15, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    client_portal_login_enabled = models.NullBooleanField(blank=True, null=True, default=False)
    auth_api_id = models.CharField(max_length=150, null=True, blank=True)
    agent_type = models.CharField(max_length=10, choices=AGENT_TYPES, default='sub')
    commission_rate = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    pricing_threshold = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal('0.0'))
    active = models.NullBooleanField(default=True, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'agents'

    def role(self):
        return self.agent_type

    def get_sub_agents(self):
        if not self.agent_type == 'master':
            return []
        return AgentOwnedBy.objects.filter(master_agent=self)

    def update_password(self, password):
        self.password = password
        self.save()   # <-- calls save
        pass

    def gen_username(self):
        username = self.email
        if not self.email:
            username = '%s.%s%s' % (self.firstname.replace(' ', '').lower(),
                                  self.lastname.replace(' ', '').lower(),
                                  self.account.pk)
        return username


    def save(self, *args, **kwargs):
        if self.client_portal_login_enabled and self.active:
            # if the account is already created on auth api
            if self.auth_api_id:
                # get the pw
                pw_to_use = self.password 
                # do api calls to update password
                # then reset the password back to not store their pw
                skip_pw_change = False
                if not self.password:
                    self.password = 'not-used99@'
                    skip_pw_change = True
                if self.password == 'not-used99@':
                    skip_pw_change = True
                if not self.active:
                    skip_pw_change = True
                print ("Updating the user password Creating user") 
                account_id = None
                #svoip = ServItpvoice.objects.filter(account=self.account)
                #if svoip:
                #    account_id = svoip[0].apiid
                username = self.gen_username()
                url = "{0}/agent/password".format(settings.AUTH_API_URL)
                # url = 'https://api.itpscorp.com/auth/api/agent/password'
                apikey = settings.AUTH_API_KEY
                d = {
                    "password": self.password,
                    "auth_api_id": self.auth_api_id
                    #"accountnumber": self.account.accountnumber
                }
                if self.agent_type == 'master':
                    d['role'] = 'master-agent'
                headers = {
                    'apikey': apikey,
                    'content-type': "application/json",
                    'cache-control': "no-cache",
                }
                self.password = 'not-used99@'
                if not skip_pw_change:
                    response = requests.request("POST", url, data=json.dumps(d), headers=headers)
                    print ("RESPONSE = %s" % response.text)
                    if '200' in str(response.status_code):
                        create_ok = response.text
                        jr = json.loads(response.text)

                else:
                    print ("Not a password update")
                

            # if the account is not created on the auth api
            if not self.auth_api_id:
                if not self.password:
                    # will have to run update-password after this
                    self.password = 'not-used99@'
                print ("CREATE Creating user")
                account_id = None
                #svoip = ServItpvoice.objects.filter(account=self.account)
                #if svoip:
                #    account_id = svoip[0].apiid
                username = self.gen_username()
                url = "{0}/agent/users".format(settings.AUTH_API_URL)
                # url = 'https://api.itpscorp.com/auth/api/agent/users'
                apikey = settings.AUTH_API_KEY
                d = {
                    "username": username,
                    "password": self.password,
                    "role": "agent"
                    #"accountnumber": self.account.accountnumber
                }
                if self.agent_type == 'master':
                    d['role'] = 'master-agent'
                headers = {
                    'apikey': apikey,
                    'content-type': "application/json",
                    'cache-control': "no-cache",
                }
                self.password = 'not-used99@'
                response = requests.request("POST", url, data=json.dumps(d), headers=headers)
                print ("RESPONSE = %s" % response)

                # IF 200
                if '200' in str(response.status_code):
                    create_ok = response.text
                    jr = json.loads(response.text)
                    self.auth_api_id = jr.get('_id')
                if '400' in str(response.status_code):
                    create_ok = False
                    jr = json.loads(response.text)
                    if jr.get('result', 'x') == 'dupe-email':
                        raise Exception("Duplicate Email...")
        super(Agents, self).save(*args, **kwargs)



class US_States(models.Model):
    name = models.CharField(max_length=100)
    shortcode = models.CharField(max_length=5)
    def formal_name(self):
        return self.name.title()
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)



class US_Cities(models.Model):
    us_state = models.ForeignKey('US_States'  ,on_delete=models.CASCADE)
    name = models.TextField()
    county = models.CharField(max_length=35)
    city_alias = models.CharField(max_length=35)
    def __str__(self):
        return str(self.name)
    def __unicode__(self):
        return unicode(self.name)



class QuoteApprovals(models.Model):
    # this table keeps track of quotes that are 
    # ready to be approved by the customer
    # when done it will set the quote status to 
    # customer approved, which will trigger a docusign send 
    # yay.
    def __str__(self):
        return str("Quote %s approval object" % (self.quote.pk))
    def __unicode__(self):
        return unicode("Quote %s approval object" % (self.quote.pk))
    quote = models.ForeignKey('Quotes'  ,on_delete=models.CASCADE)
    # this is the token sent in the confirmation email.....
    token = models.TextField(default='abc99@@')
    pdf_filename = models.TextField(blank=True, null=True)
    docusign_envelope_id = models.TextField()
    notes = models.ManyToManyField('QuoteApprovalNotes', blank=True, null=True)
    def add_a_note(self, msg):
        n = QuoteApprovalNotes(
                o=self,
                created=datetime.now(),
                txt = msg)
        n.save()
        return True

class QuoteApprovalNotes(models.Model):
    o = models.ForeignKey('QuoteApprovals'  ,on_delete=models.CASCADE)
    created = models.DateTimeField(default=datetime.now())
    txt = models.TextField(blank=True, null=True)


class PDFHash(models.Model):
    # not used right now
    account = models.ForeignKey('Accounts'  ,on_delete=models.CASCADE)
    token = models.TextField(default='abc99@@')


ACTIVITY_TYPES = (
        ('email', 'email'),
        ('sms', 'sms'),
        ('docusign', 'docusign'),
)
class ActivityLogs(models.Model):
    account = models.ForeignKey('Accounts'  ,on_delete=models.CASCADE)
    contact = models.ForeignKey('Contacts'  ,on_delete=models.CASCADE)
    activity_type = models.CharField(max_length=20, choices=ACTIVITY_TYPES)

