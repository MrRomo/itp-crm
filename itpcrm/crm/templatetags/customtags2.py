from django import template

register = template.Library()


@register.filter('pending_upgrades')
def get_pending_upgrades(acctserv, quote_id):
    #print "quote id from pending_upgradeds template tag %s" % quote_id
    caca = acctserv.pending_upgrades(quote_id=quote_id)
    return caca


@register.filter('pending_downgrades')
def get_pending_downgrades(acctserv, quote_id):
    caca = acctserv.pending_downgrades(quote_id=quote_id)
    return caca


@register.filter('pending_disconnect')
def get_pending_disconnect(acctserv, quote_id):
    caca = acctserv.pending_disconnect(quote_id=quote_id)
    return caca


@register.tag('sum')
def do_sum(n1, n2):
    return n1 + n2

