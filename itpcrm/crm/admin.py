# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from crm.models import (
    Accounts,
    Contacts,
    Products,
    Quotes,
    Orders,
    OrderItems,
    QuoteItemGroup,
    QuoteItems,
    ServiceAddress,
    Agents,
    ITPServices_Addresses,
    AgentOwnedBy,
    AccountServices,
    ServiceAddons,
    OrderItemNotes,
    US_States,
    US_Cities,
    QuoteApprovals,
    QuoteApprovalNotes,
    ProductManufacturer,
    ProductKeyNames,
    ProductKeyServiceAddons,
)

# Register your models here.
class AccountsAdmin(admin.ModelAdmin):
    list_display = ['name','account_type', 'accountnumber','agent','crm_login_user']

class ContactsAdmin(admin.ModelAdmin):
    list_display = ['account', 'firstname', 'lastname', 'auth_api_id',]
    list_filter = ('admin_group','billing_group','technical_group',)


class ProductsAdmin(admin.ModelAdmin):
    list_display = ['name',
                    'category',
                    'product_type',
                    'price',
                    'part_number',
                    'manufacturer', 'model_number',
                    'product_key',
                    ]
    list_filter = ('category',  'product_type',)

class ProductKeyNamesAdmin(admin.ModelAdmin):
    list_display = ['name', 'default_metadata']

class ProductKeyServiceAddonsAdmin(admin.ModelAdmin):
    list_display = ['product_key',]
    list_filter = ('product_key',)
    def render_change_form(self, request, context, *args, **kwargs):
        context['adminform'].form.fields['addon_products'].queryset = \
            Products.objects.filter(product_type='ServiceAddon')
        return super(ProductKeyServiceAddonsAdmin, self).render_change_form(request, context, *args, **kwargs)

class QuotesAdmin(admin.ModelAdmin):
    list_display = ['account', 'quote_status', 'date_created', 'agent', 'quote_assignee',]
    list_filter = ('account', 'quote_status', 'agent',)

class QuoteItemGroupsAdmin(admin.ModelAdmin):
    list_display = ['name', 'quote']


class QuoteItemsAdmin(admin.ModelAdmin):
    list_display = ['quote', 'purchase_type', 'service_address', 'item_group', 'product', 'price', 'price_changed', 'quantity', 'contract_terms']
    list_filter = ('purchase_type',)

class OrdersAdmin(admin.ModelAdmin):
    list_display = ['order_number', 'account', 'status', 'date_created', 'deleted', 'quote']
    list_filter = ('account',)

class OrderItemsAdmin(admin.ModelAdmin):
    list_display = ['order', 'service_address', 'completed', 'item_type', 
            'purchase_type', 'upgrade_service', 
            'provision_status', 'price_per_unit', 'price', 'quantity', 'payment_complete', 'contract_terms',]
    list_filter = ('completed', 'purchase_type', 
            'provision_status', 'payment_complete',)


class ServiceAddressAdmin(admin.ModelAdmin):
    list_display = ['full_address', 'hse_num', 'pre_dir', 'st_name', 'st_type', 'suite_num', 'zip', 'city', 'state',]
    list_filter = ('validated', 'state', 'city', 'zip', 'st_type',)

class AgentsAdmin(admin.ModelAdmin):
    list_display = ['firstname', 'lastname', 'agent_type', 'email', 'auth_api_id']
    list_filter = ('agent_type',)


class ITPServices_AddressesAdmin(admin.ModelAdmin):
    list_display = ['product', 'address']

class AgentOwnedByAdmin(admin.ModelAdmin):
    list_display = ['master_agent', 'sub_agent']

class AccountServicesAdmin(admin.ModelAdmin):
    list_display = ['order', 'item_type', 'provision_status', 'price_per_unit', 'price', 'quantity', 'payment_complete', 'api_id']
    list_filter = ('payment_complete',)

class ServiceAddonsAdmin(admin.ModelAdmin):
    list_display = ['service', 'account', 'order', 'item_type', 'provision_status', 'price_per_unit', 'price', 'payment_complete']
    list_filter = ('payment_complete',)


class OrderItemNotesAdmin(admin.ModelAdmin):
    list_display = ['orderitem', 'user', 'note_type', 'msg']
    list_filter = ('note_type', 'user',)

class US_CitiesAdmin(admin.ModelAdmin):
    list_display = ['us_state', 'name', 'county', 'city_alias']
    list_filter = ('us_state',)

class US_StatesAdmin(admin.ModelAdmin):
    list_display = ['name', 'shortcode']


class QuoteApprovalsAdmin(admin.ModelAdmin):
    list_display = ['quote', 'token', 'docusign_envelope_id']

class QuoteApprovalNotesAdmin(admin.ModelAdmin):
    list_display = ['o', 'created', 'txt']

class ProductManufacturerAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'website']

admin.site.register(Accounts, AccountsAdmin)
admin.site.register(Contacts, ContactsAdmin)
admin.site.register(Products, ProductsAdmin)
admin.site.register(Quotes, QuotesAdmin)
admin.site.register(Orders, OrdersAdmin)
admin.site.register(OrderItems, OrderItemsAdmin)
admin.site.register(QuoteItemGroup, QuoteItemGroupsAdmin)
admin.site.register(QuoteItems, QuoteItemsAdmin)
admin.site.register(ServiceAddress, ServiceAddressAdmin)
admin.site.register(ITPServices_Addresses, ITPServices_AddressesAdmin)
admin.site.register(Agents, AgentsAdmin)
admin.site.register(AgentOwnedBy, AgentOwnedByAdmin)
admin.site.register(AccountServices, AccountServicesAdmin)
admin.site.register(ServiceAddons, ServiceAddonsAdmin)
admin.site.register(OrderItemNotes, OrderItemNotesAdmin)
admin.site.register(US_Cities, US_CitiesAdmin)
admin.site.register(US_States, US_StatesAdmin)
admin.site.register(QuoteApprovals, QuoteApprovalsAdmin)
admin.site.register(QuoteApprovalNotes, QuoteApprovalNotesAdmin)
admin.site.register(ProductManufacturer, ProductManufacturerAdmin)
admin.site.register(ProductKeyNames, ProductKeyNamesAdmin)
admin.site.register(ProductKeyServiceAddons, ProductKeyServiceAddonsAdmin)
