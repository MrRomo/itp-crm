from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views
from django.urls import path


urlpatterns = [
    path('accounts/', views.AccountsList.as_view(), name='accounts_list'),
    path('accounts/new', views.AccountsCreate.as_view(), name='accounts_new'),
    path('accounts/edit/<int:pk>/', views.AccountsUpdate.as_view(), name='accounts_edit'),
    path('accounts/delete/<int:pk>/', views.AccountsDelete.as_view(), name='accounts_delete'),
    path('accounts/<int:pk>/', views.AccountsDetail.as_view(), name='accounts_detail'),

    path('accountservices/new/', views.AccountServicesCreate.as_view(), name='accountservice_new'),
    path('accountservices/<int:pk>/', views.AccountServicesDetail.as_view(), name='accountservice_detail'),

    path('serviceaddons/new/', views.ServiceAddonsCreate.as_view(), name='serviceaddon_new'),

    path('contacts/', views.ContactsList.as_view(), name='contacts_list'),
    path('contacts/new', views.ContactsCreate.as_view(), name='contacts_new'),
    path('contacts/edit/<int:pk>/', views.ContactsUpdate.as_view(), name='contacts_edit'),
    path('contacts/delete/<int:pk>/', views.ContactsDelete.as_view(), name='contacts_delete'),
    path('contacts/<int:pk>/', views.ContactsDetail.as_view(), name='contacts_detail'),

    path('products/', views.ProductsList.as_view(), name='products_list'),
    path('products/new/', views.ProductsCreate.as_view(), name='products_new'),
    path('products/edit/<int:pk>/', views.ProductsUpdate.as_view(), name='products_edit'),
    path('products/delete/<int:pk>/', views.ProductsDelete.as_view(), name='products_delete'),
    path('products/<int:pk>/', views.ProductsDetail.as_view(), name='products_detail'),

    # service address
    path('service-address', views.ServiceAddressList.as_view(), name='serviceaddress_list'),
    path('service-address/new/', views.ServiceAddressCreate.as_view(), name='serviceaddress_new'),
    path('service-address/formalize/', views.service_address_formalize, name='serviceaddress_formalize'),
    path('service-address/search/', views.service_address_search, name='serviceaddress_search'),
    path('service-address/edit/<int:pk>/', views.ServiceAddressUpdate.as_view(), name='serviceaddress_edit'),
    path('service-address/delete/<int:pk>/', views.ServiceAddressDelete.as_view(), name='serviceaddress_delete'),
    path('service-address/<int:pk>/', views.ServiceAddressDetail.as_view(), name='serviceaddress_detail'),

    path('productpackages/', views.ProductPackagesList.as_view(), name='productpackages_list'),
    path('productpackages/new', views.ProductPackagesCreate.as_view(), name='productpackages_new'),
    path('productpackages/edit/<int:pk>/', views.ProductPackagesUpdate.as_view(), name='productpackages_edit'),
    path('productpackages/delete/<int:pk>/', views.ProductsDelete.as_view(), name='productpackages_delete'),
    path('productpackages/<int:pk>/', views.ProductsDetail.as_view(), name='productpackages_detail'),

    path('quotes/', views.QuotesList.as_view(), name='quotes_list'),
    path('quotes/by-draft', views.QuotesDraftList.as_view(), name='quotes_by_draft'),
    path('quotes/by-pending', views.QuotesPendingList.as_view(), name='quotes_by_pending'),
    path('quotes/new', views.QuotesCreate.as_view(), name='quotes_new'),
    path('quotes/manager-approval', views.manager_approval, name='manager_approval_quote'),
    path('quotes/manager-approval/<int:pk>/details', views.manager_view_order, name='manager_approval_quote_view'),
    path('quotes/approve-quote/<int:pk>/', views.manager_approve_the_quote, name='manager_approval_quote_approve'),
    path('quotes/cancel-order/<int:pk>/', views.manager_decline_the_quote, name='manager_approval_quote_cancel'),
    path('quotes/customer-approval', views.customer_approve_quote, name='customer_approve_quote'),
    path('quotes/edit/<int:pk>/', views.QuotesUpdate.as_view(), name='quotes_edit'),
    path('quotes/delete/<int:pk>/', views.QuotesDelete.as_view(), name='quotes_delete'),
    path('quotes/<int:pk>/', views.QuotesDetail.as_view(), name='quotes_detail'),
 
    path('orders/', views.OrdersList.as_view(), name='orders_list'),
    path('order-claim/', views.claim_order, name='orders_claim'),
    path('orders/new', views.OrdersCreate.as_view(), name='orders_new'),
    path('orders/edit/<int:pk>/', views.OrdersUpdate.as_view(), name='orders_edit'),
    path('orders/delete/<int:pk>/', views.OrdersDelete.as_view(), name='orders_delete'),
    path('orders/<int:pk>/', views.OrdersDetail.as_view(), name='orders_detail'),

    path('orderitems/', views.OrderItemsList.as_view(), name='orderitems_list'),
    path('order-item-provision/', views.orderitem_provision, name='orderitem_provision'),
    path('order-item-completed/', views.orderitem_mark_complete, name='orderitem_mark_complete'),
    path('order-item-completed-fiber/', views.orderitem_fiber_mark_complete, name='orderitem_fiber_mark_complete'),
    path('order-item-approval/', views.orderitem_manual_approve, name='orderitem_manual_approve'),
    path('orderitems/new', views.OrderItemsCreate.as_view(), name='orderitems_new'),
    path('orderitems/edit/<int:pk>/', views.OrderItemsUpdate.as_view(), name='orderitems_edit'),
    path('orderitems/delete/<int:pk>/', views.OrderItemsDelete.as_view(), name='orderitems_delete'),
    path('orderitems/<int:pk>/', views.OrderItemsDetail.as_view(), name='orderitems_detail'),

    # wizards - quotes
    path('quoteswizard/2/<int:pk>/', views.wizard_quotes_step2, name='wizard_quotes_step2'),
    path('quoteswizard/3ro/<int:pk>/', views.wizard_quotes_step3_readonly, name='wizard_quotes_step3_ro'),
    path('quoteswizard/3/<int:pk>/', views.wizard_quotes_step3, name='wizard_quotes_step3'),
    path('quoteswizard/1', views.wizard_quotes_step1, name='wizard_quotes_step1'),
    path('quoteswizard/account-service/<int:pk>/<int:apk>', views.wizard_addons_for_account_service, name='wizard_addons_for_account_service'),
    path('quoteswizard/service-quote-item/<int:pk>/<int:qipk>/', views.wizard_addons_for_quote_item, name='wizard_addons_for_quote_item'),
    path('quoteswizard/auto-order/(?P<pk>\d+)$', views.wizard_quotes_convert_to_order, name='wizard_quotes_convert_to_order'),
    path('quoteswizard/submit/<int:pk>/', views.wizard_quotes_submit, name='wizard_quotes_submit'),
    path('quoteswizard/add-new-service-addon/quoteitem/<int:pk>/<int:apk>/', views.wizard_add_new_service_addon_qi, name='wizard_add_new_service_addon_qi'),
    path('quoteswizard/add-new-service-addon/<int:pk>/<int:apk>/', views.wizard_add_new_service_addon, name='wizard_add_new_service_addon'),
    path('quoteswizard/add-new-nrc-product/<int:pk>/', views.wizard_add_new_nrc_product, name='wizard_add_new_nrc_product'),
    path('quoteswizard/add-new-service/<int:pk>/', views.wizard_add_new_service, name='wizard_add_new_service'),
    

    url(r'^quoteswizard/upgrade-service/(?P<pk>\d+)/(?P<apk>\d+)$', views.wizard_upgrade_service, name='wizard_upgrade_service'),
    url(r'^quoteswizard/downgrade-service/(?P<pk>\d+)/(?P<apk>\d+)$', views.wizard_downgrade_service, name='wizard_downgrade_service'),

    # SEARCH GLOBAL
    path('global-search/', views.global_search, name='global_search'),

    url(r'^ (?P<pk>\d+)/(?P<apk>\d+)$', views.wizard_disconnect_service, name='wizard_disconnect_service'),

    # ajax
    path('ajax/autocomplete/accounts/', views.ajax_autocomplete_accounts, name='ajax_autocomplete_accounts'),
    path('ajax/autocomplete/products/', views.ajax_autocomplete_products, name='ajax_autocomplete_products'),
    path('ajax/accounts/', views.ajax_account_detail, name='ajax_account_detail'),
    path('ajax/city-by-state/', views.ajax_city_by_state, name='ajax_city_by_state'),
    path('ajax/autocomplete/service-address/', views.wizard_autocomplete_serviceaddress, name='ajax_service_address'),
    path('ajax/prod-by-service-address/', views.wizard_products_by_address, name='ajax_wizard_products_by_address'),
    path('ajax/get-product-by-id/', views.ajax_products_by_id, name='ajax_products_by_id'),


    #quick save methods to help wizard/flow
    path('wizard/post/quotes/itemgroup/', views.wizard_add_itemgroup, name='wizard_add_itemgroup'),
    path('wizard/post/quotes/item/delete/', views.wizard_del_item_from_group, name='wizard_del_item_from_group'),
    path('wizard/post/quotes/item/change-price/', views.wizard_change_item_price, name='wizard_change_item_price'),
    path('wizard/post/quotes/item/change-quantity/', views.wizard_change_item_quantity, name='wizard_change_item_quantity'),
    url(r'^wizard/post/quotes/item/description/$', views.wizard_change_item_description, name='wizard_change_item_description'),
    # wizard add/remove services and service addons

    path('wizard/post/quotes/add-service/', views.add_service, name='post_wizard_add_service'),
    path('wizard/post/quotes/add-service/', views.add_service, name='wizard_add_service'),   # --- (legacy) remove this one after cleanup

    path('wizard/post/quotes/upgrade-service/', views.upgrade_service, name='post_wizard_upgrade_service'),
    path('wizard/post/quotes/downgrade-service/', views.downgrade_service, name='post_wizard_downgrade_service'),

    path('wizard/post/quotes/remove-service/', views.remove_service, name='wizard_remove_service'),
    path('wizard/post/quotes/add-service-addon/', views.add_service_addon, name='post_add_service_addon'),

    path('wizard/post/quotes/add-nrc-product/', views.add_nrc_product, name='post_wizard_add_nrc_product'),


    # Admin Tools Urls

    path('admintools/billing/international-rates', views.international_rates, name='admintools_international_rates'),

    # other urls
    url(r'^login_success/$', views.login_success, name='login_success'),

    #url('pdf/render/send-quote', views.Pdf.as_view(), name="pdf_send_quote"),
    #url('pdf/render/send-quote', views.HelloPDFView.as_view(), name="pdf_send_quote"),
    # this renders html for quote so the phantomjs can use it and 
    # make a PDF out of it..
    # url('pdf/quote/(?P<pk>\d+)$', views.print_quote_html, name="print_quote_html"),
    url('pdf/gen/quote/(?P<pk>\d+)$', views.gen_pdf, name="gen_pdf"),

    # indexp - default url
    url(r'^$', views.index, name="index"),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
