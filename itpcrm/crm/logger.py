from datetime import datetime
import logging

from django.conf import settings


class CRMLogger(object):
    """
    Sample usage:

    logger = BillingLogger()
    logger.info('fk')

    """
    logger = None
    logfile = None
    loglevel = None    
    def __init__(self, *args, **kwargs):
        # Detect log level and pass to log opts
        self.loglevel = settings.LOG_LEVEL
        self.logfile = settings.LOG_FILE

        # override cause were nice
        if kwargs.get('logfile'):
            self.logfile = kwargs.get('logfile')
        if kwargs.get('loglevel'):
            self.loglevel = kwargs.get('loglevel')

        # log level map for config simplicity
        lm = {
            "debug": logging.DEBUG,
            "info": logging.INFO,
            "critical": logging.CRITICAL,
            "warning": logging.WARNING,
            "error": logging.ERROR
        }
        if self.loglevel:
            _loglevel = lm[self.loglevel]

        logopts = {
            'filename': self.logfile,
            'level': _loglevel,
            'format': '%(asctime)s %(levelname)s %(name)s %(message)s'
        }
        logging.basicConfig(**logopts)
        self.logger = logging.getLogger(__name__)

    def info(self, msg):
        self.logger.info(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def error(self, msg):
        self.logger.error(msg)

    def critical(self, msg):
        self.logger.critical(msg)

    def debug(self, msg):
        self.logger.debug(msg)

