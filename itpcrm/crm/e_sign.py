from  __future__ import absolute_import, print_function
from pprint import pprint
import base64
import unittest
import webbrowser
import eversign

from PyPDF2 import PdfFileReader
from django.conf import settings
from random import randint

PDF_PATH = settings.GET_PDF_SAVE_PATH



def run_esign_on_quote(quote_approval=None, quote_id=None, num_pages=None):
    # ENABLE DEBUG
    eversign.debug = False

    if not quote_id:
        raise Exception("NAH MAN WE NEED A QUOTE ID OK?")
    if not quote_approval:
        raise Exception("We need a quote approval object")


    client = eversign.Client(settings.ES_ACCESS_KEY)

    # document init..
    document = eversign.Document()
    document.title = "ITP Quote"
    document.message = "miamiweblab@gmail.com"

    # set teh recipient
    recipient_name = '%s %s' % (
        quote_approval.quote.quote_assignee.firstname,
        quote_approval.quote.quote_assignee.lastname,
    )
    recipient = eversign.Recipient(name=recipient_name,
            email=quote_approval.quote.quote_assignee.email)

    # PDF 
    pdffile_path = '%s/%s' % (PDF_PATH,
                              quote_approval.pdf_filename)

    pdffile = eversign.File(name='Test99-%s' % randint(1111,999999))

    with open(pdffile_path, "rb") as p:
        pdffile.file_base64 = base64.b64encode(p.read())

    print("The File path is: " + pdffile_path)

    with open(pdffile_path, "rb") as pdf_file:
        pdf = PdfFileReader(pdf_file)
        num_pdf_pages = pdf.numPages


    # signer stuffs
    signer = eversign.Signer()
    signer.id = "1"
    signer.name = recipient_name
    signer.email = quote_approval.quote.quote_assignee.email


    if settings.CONFIG_MODE == 'dev':
        document.sandbox = True
    document.add_file(pdffile)
    document.add_signer(signer)
    #document.add_recipient(recipient)

    #initials field
    #initial_heres = []
    esign_fields = []
    for n in range(1, num_pdf_pages):
        init1 = eversign.InitialsField()
        init1.identifier = 'initialz%s' % n
        init1.x = "520"
        init1.y = "785"
        init1.page = n
        init1.signer = 1
        init1.width = 60
        init1.height = 25
        init1.required = 1
        #initial_heres.append(init1)
        esign_fields.append(init1)

    # signature field
    field = eversign.SignatureField()
    field.identifier = "sig99"
    field.x = "140"
    field.y = "350"
    field.page = num_pdf_pages
    field.signer = 1
    field.width = 120
    field.height = 25
    field.required = 1
    esign_fields.append(field)


    #fullname
    fname = eversign.TextField()
    fname.identifier = "name99"
    fname.x = "140"
    fname.y = "272"
    fname.page = num_pdf_pages
    fname.signer = 1
    fname.width = 120
    fname.height = 25
    fname.required = 1
    fname.value = recipient_name
    fname.readonly = 1
    esign_fields.append(fname)



    #date
    dt = eversign.DateSignedField()
    dt.identifier = "datesigned99"
    dt.x = "140"
    dt.y = "330"
    dt.page = num_pdf_pages
    dt.signer = 1
    dt.width = 120
    dt.height = 25
    dt.required = 1
    esign_fields.append(dt)

    #Title/Position Field
    field_title_position = eversign.TextField()
    field_title_position.identifier = "title_position_field_1"
    field_title_position.x = "140"
    field_title_position.y = "295"
    field_title_position.page = num_pdf_pages
    field_title_position.signer = 1
    field_title_position.width = 150
    field_title_position.height = 25
    field_title_position.required = 0
    field_title_position.readonly = 1
    field_title_position.value = quote_approval.quote.quote_assignee.title
    esign_fields.append(field_title_position)


    ### add the fields
    document.add_field_list(esign_fields)


    ## send that shit bro
    try:
        finished_doc = client.create_document(document)
        return finished_doc

    except Exception as e:
        print("Exception returned %s" % str(e))
