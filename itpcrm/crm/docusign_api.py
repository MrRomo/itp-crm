from  __future__ import absolute_import, print_function
from pprint import pprint
import base64
import unittest
import webbrowser

import docusign_esign as docusign
from docusign_esign import AuthenticationApi, TemplatesApi, EnvelopesApi
from docusign_esign.rest import ApiException
from PyPDF2 import PdfFileReader

from django.conf import settings


#docusign config / ds = docusign

def run_esign_on_quote(quote_approval=None, quote_id=None, num_pages=None):
    if not quote_id:
        raise Exception("NAH MAN WE NEED A QUOTE ID OK?")
    if not quote_approval:
        raise Exception("We need a quote approval object")

    #user_name = "docusign@itpfiber.com"
    user_name = settings.DOCUS_USER_NAME
    #integrator_key = "8a256bde-405b-4032-bf24-be0245631f03"
    integrator_key = settings.DOCUS_INTEGRATOR_KEY
    #base_url = "https://demo.docusign.net/restapi"
    base_url = settings.DOCUS_BASE_URL
    #oauth_base_url = "account-d.docusign.com" # use account.docusign.com for Live/Production
    oauth_base_url = settings.DOCUS_OAUTH_BASE_URL
    #redirect_uri = "http://192.112.255.145:9998/api/callbacks/docusign"
    redirect_uri = settings.DOCUS_REDIRECT_URI
    #private_key_filename = "/home/itp/docusign-examples/keys/docusign_private_key.txt"
    private_key_filename = settings.DOCUS_PRIV_KEY_FILENAME
    #user_id = "7f26f6bb-8a39-444f-ae99-54922fec68f6"
    user_id = settings.DOCUS_USER_ID

    # template_id = "[TEMPLATE_ID]"

    api_client = docusign.ApiClient(base_url)

    # IMPORTANT NOTE:
    # the first time you ask for a JWT access token, you should grant access by making the following call
    # get DocuSign OAuth authorization url:
    oauth_login_url = api_client.get_jwt_uri(integrator_key, redirect_uri, oauth_base_url)
    # open DocuSign OAuth authorization url in the browser, login and grant access
    # webbrowser.open_new_tab(oauth_login_url)
    print(oauth_login_url)

    # END OF NOTE

    # configure the ApiClient to asynchronously get an access token and store it
    api_client.configure_jwt_authorization_flow(private_key_filename, oauth_base_url, integrator_key, user_id, 3600)

    docusign.configuration.api_client = api_client

    template_role_name = 'Needs to sign'

    # create an envelope to be signed
    envelope_definition = docusign.EnvelopeDefinition()
    envelope_definition.email_subject = 'ITP - Please Review And Sign'
    envelope_definition.email_blurb = 'Hello, Please Review the Quote and E-Sign to accept.'

    # create a template role with a valid template_id and role_name and assign signer info
    t_role = docusign.TemplateRole()
    t_role.role_name = template_role_name
    t_role.name = 'Your Name'
    t_role.email = 'dev@scidentify.info'

    # create a list of template roles and add our newly created role
    # assign template role(s) to the envelope
    #envelope_definition.template_roles = [t_role]

    # send the envelope by setting |status| to "sent". To save as a draft set to "created"
    envelope_definition.status = 'sent'

    auth_api = AuthenticationApi()
    envelopes_api = EnvelopesApi()


    # here
    # DETRERMINE THE FILENAME FROM THE QUOTE ID
    pdffn = '/var/www/html/quotes/%s' % quote_approval.pdf_filename
    # get pdf num of pages 
    pdf=PdfFileReader(open(pdffn,'rb'))
    num_pdf_pages = pdf.numPages
    # end get pdf numpages
    doc = docusign.Document()
    base64_doc = base64.b64encode(open(pdffn, 'rb').read()).decode("utf-8")
    doc.document_base64 = base64_doc
    doc.name = "ITP-Quote"
    doc.document_id = str(quote_id)
    doc.document_id = '1'
    print('docsign debug: doc = %s' % dir(doc))
    #doc.document_fields = [{"hi": "hi99"}]
    #envelope_template.documents = [doc]

    envelope_definition.documents = [doc]
    print('docusign debug pages %s' % dir(envelope_definition))
    #print('docusign debug: doc pages len %s' % len(envelope_definition.pages))

     # Add a recipient to sign the document
    signer = docusign.Signer()
    #signer.email = user_namea
    signer.name = 'Caesar Engroba'
    signer.recipient_id = '1'
    signer.email = 'caesar@itpscorp.com'

    signer2 = docusign.Signer()
    #signer2.email = user_name
    #signer2.name = 'Mike Lopez'
    signer2.name = '%s %s' % (
        quote_approval.quote.quote_assignee.firstname,
        quote_approval.quote.quote_assignee.lastname,
    )
    signer2.recipient_id = '1'
    #signer2.email = 'miamiweblab@gmail.com'
    signer2.email = quote_approval.quote.quote_assignee.email

    # Create a SignHere tab somewhere on the document for the signer to sign
    # raise Exception(dir(docusign))
    # initial for each listing item page 

    # event notificatoins (NOT USED RIGHT NOW...HERE FOR REFERENCE)
    ev = docusign.EventNotification()
    ev.url = redirect_uri
    ev.envelope_events = [
        {"envelopeEventStatusCode": "sent"},
        {"envelopeEventStatusCode": "delivered"},
        {"envelopeEventStatusCode": "completed"},
        {"envelopeEventStatusCode": "declined"},
        {"envelopeEventStatusCode": "voided"}
    ]
    ev.recipient_events = [
        {"recipientEventStatusCode": "Sent"},
        {"recipientEventStatusCode": "Delivered"},
        {"recipientEventStatusCode": "Completed"},
        {"recipientEventStatusCode": "Declined"},
        {"recipientEventStatusCode": "AuthenticationFailed"},
        {"recipientEventStatusCode": "AutoResponded"}
    ]


    # custom fields
    cf = docusign.CustomFieldV2()
    cf.name = "quoteid99"
    cf.value = "yoooo99"
    cf.show = True
    #cf.list_custom_fields = {"quoteid": "quoteid"}
    #cf.list_custom_fields = ['hiquote', 'hi99']
    #envelope_definition.custom_fields = [cf]

    #initials
    initial_heres = []
    for n in range(0, num_pdf_pages-1):
        initial_here = docusign.InitialHere()
        initial_here.document_id = '1'
        initial_here.page_number = str(n + 1)
        initial_here.recipient_id = '1'
        initial_here.x_position = '550'
        initial_here.y_position =  '775'
        initial_here.scale_value = '0.5'
        #append
        initial_heres.append(initial_here)


    #signature
    sign_here = docusign.SignHere()
    sign_here.document_id = '1'
    sign_here.page_number = num_pdf_pages
    sign_here.recipient_id = '1'
    sign_here.x_position = '150'
    sign_here.y_position =  '335'
    sign_here.scale_value = '0.5'

    #title 
    title_here = docusign.Title()
    title_here.document_id = '1'
    title_here.page_number = num_pdf_pages
    title_here.recipient_id = '1'
    title_here.x_position = '150'
    title_here.y_position =  '300'
    title_here.scale_value = '0.5'


    #fullname
    name_here = docusign.FullName()
    name_here.document_id = '1'
    name_here.page_number = num_pdf_pages
    name_here.recipient_id = '1'
    name_here.x_position = '150'
    name_here.y_position =  '280'
    name_here.scale_value = '0.5'

    #date
    date_here = docusign.DateSigned()
    date_here.document_id = '1'
    date_here.page_number = num_pdf_pages
    date_here.recipient_id = '1'
    date_here.x_position = '150'
    date_here.y_position =  '330'
    date_here.scale_value = '0.5'


    tabs = docusign.Tabs()
    #raise Exception(tabs)
    #tabs.initial_here_tabs = [initial_here]
    tabs.initial_here_tabs = []
    for i in initial_heres:
        tabs.initial_here_tabs.append(i)

    tabs.sign_here_tabs = [sign_here]
    tabs.title_tabs = [title_here]
    tabs.date_signed_tabs = [date_here]
    tabs.full_name_tabs = [name_here]
    #signer.tabs = tabs
    signer2.tabs = tabs

    recipients = docusign.Recipients()
    recipients.signers = [signer2]
    #raise Exception(dir(recipients))
    #envelope_template.recipients = recipients
    envelope_definition.recipients = recipients

    ###

    try:
        login_info = auth_api.login(api_password='true', include_account_id_guid='true')
        #assert login_info is not None
        #assert len(login_info.login_accounts) > 0
        login_accounts = login_info.login_accounts
        #assert login_accounts[0].account_id is not None

        base_url, _ = login_accounts[0].base_url.split('/v2')
        api_client.host = base_url
        docusign.configuration.api_client = api_client

        envelope_summary = envelopes_api.create_envelope(login_accounts[0].account_id, envelope_definition=envelope_definition)
        return envelope_summary

        """print(dir(envelope_summary))
        assert envelope_summary is not None
        assert envelope_summary.envelope_id is not None
        assert envelope_summary.status == 'sent'

        print("EnvelopeSummary: ", end="")
        pprint(envelope_summary)"""

    except ApiException as e:
        print("\nException when calling DocuSign API: %s" % e)
        #assert e is None # make the test case fail in case of an API exception
