[
	{
		"field_name": "status",
		"field_type": "dropdown",
		"field_values": ["enabled", "disabled"]
	},
	{
		"field_name": "date_modified",
		"field_type": "datetime_now",
		"field_values": null
	},
	{
		"field_name": "notes",
		"field_type": "text",
		"field_values": null,
	},
	{
		"field_name": "enabled",
		"field_type": "text_readonly",
		"field_values": false
	},
	{
		"field_name": "toll_free_rate",
		"field_type": "dropdown",
		"field_values": ["0.022", "0.021", "0.020", "0.019", "0.018"]
	},
	{
		"field_name": "cdr_billable",
		"field_type": "dropdown",
		"field_values": [true, false]
	},
	{
		"field_name": "purchased_users",
		"field_type": "price_calc_field",
		"field_values": ["1", "2", "3", "4"]
	},
	{
		"field_name": "date_added",
		"field_type": "text_readonly",
		"field_values": null,
	},
	{
		"field_name": "create_results",
		"field_type": "text_readonly",
		"field_values": null
	},
	{
		"field_name": "international_enabled",
		"field_type": "dropdown",
		"field_values": [true, false]
	}

]
