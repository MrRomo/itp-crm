import json
from random import randint
from datetime import datetime, timedelta
from crm.models import *

names = open('/tmp/companynames.txt', 'r').readlines()

biz_name_suffix = [
    'Transportation',
    'Flowers',
    'Furniture',
    'Repairs',
    'Technologies',
    'Industries',
    'Checkpoints',
    'Consulting',
    'Law Firm',
    'Affiliations',
    'Marketing',
    'IT Services',
    'Plumbing',
    'Services',
    'Architects',
    'Realtors',
    'Group',
    'Holdings',
    'Operations',
    'Futures',
    'Entertainment',
    'TV',
    'News',
    'Credit Union',
    'Casinos',
    'Professionals'
]
addr_directions = ['SW', 'W', 'E', 'SE', 'NW', 'NE', 'N']
addr_streets = ['Ave', 'Street', 'Blvd', 'Ct', 'Way', 'Lane', 'Drive']
addr_cities = ['Miami', 'Fort Lauderdale', 'Doral', 'Coral Gables']

phone_area_codes = ['305','786','954', '561']



def gen_company_name():
    try:
        company_name = "%s %s" % (
            names[randint(0, 4923)].replace('\n', '').replace('\r', ''),
            biz_name_suffix[randint(0, len(biz_name_suffix)-1)]

        )
        return str(company_name)
    except Exception:
        company_name = "%s %s" % (
            names[randint(0, 4923)].replace('\n', '').replace('\r', ''),
            biz_name_suffix[randint(0, len(biz_name_suffix)-1)]
        )
        return str(company_name)


def gen_address():
    addr_num = randint(100, 99999)
    direction = addr_directions[randint(0, len(addr_directions)-1)]
    street = addr_streets[randint(0, len(addr_streets)-1)]
    return {
        'address': '%s %s %s %s' % (addr_num, direction, randint(10,170), street),
        'city': 'Miami',
        'state': "FL",
        'zipcode': randint(33010, 33333)
    }


def gen_phone():
    phone = '%s-%s-%s' % (
        phone_area_codes[randint(0, len(phone_area_codes)-1)],
        randint(202, 985),
        randint(1021, 8793)
    )
    return phone


def gen_fname():
    o = open('/tmp/firstnames.txt', 'r').readlines()
    n = o[randint(0, len(o)-1)]
    return n.replace('\n', '').replace('\r', '')


def gen_lname():
    o = open('/tmp/lastnames.txt', 'r').readlines()
    n = o[randint(0, len(o)-1)]
    return n.replace('\n', '').replace('\r', '')


def test_print():
    print gen_company_name()
    address = gen_address()
    addr = address.get('address')
    city = address.get('city')
    state = address.get('state')
    zipcode = address.get('zipcode')

    print addr
    print "%s, %s %s" % (city, state, zipcode)


def get_random_from(x):
    return x[randint(0, len(x)-1)]

####
#########
##################
##################
#########
####



def run():
    run_times = 1000
    for runtimes in range(0, run_times):
        dice = [True, False]
        stat_dice = ['Active', 'Suspended']
        contacts_count_dice = [1, 5, 7, 3, 10, 22, 12, 18, 20]

        #STEP1 - create the Accounts object
        company_name = gen_company_name()
        website_domain = ['com', 'net', 'org']
        website = '%s.%s' % (
            company_name.replace(' ','').replace('.', '').replace(',',''),
            website_domain[randint(0, len(website_domain)-1)]
        )
        
        bill_address = gen_address()
        service_address = bill_address
        # roll the dice, see if we should add same billing/service address
        bill_diff = dice[randint(0, 1)]

        if bill_diff:
            # gen a new one for service address
            service_address = gen_address()
        
        
        phone = gen_phone()
        automatic_payments = dice[randint(0, 1)]
        ebilling = dice[randint(0,1)]
        status = 'Active'

        accounts = {
            'name': company_name,
            'status': 'Active',
            'website': website.lower(),
            'phone': phone,
            'billing_address': bill_address.get('address'),
            'billing_city': bill_address.get('city'),
            'billing_state': bill_address.get('state'),
            'billing_zipcode': bill_address.get('zipcode'),
            'billing_country': 'US',
            'service_address': service_address.get('address'),
            'service_city': service_address.get('city'),
            'service_state': service_address.get('state'),
            'service_zipcode': service_address.get('zipcode'),
            'service_country': 'US',
            'industry': 'Other',
            'net_terms': '30',
        }
        print json.dumps(accounts, indent=4)
        acc_object = Accounts(**accounts)
        acc_object.save()


        # Step2 - Contacts
        num_contacts = contacts_count_dice[randint(0, len(contacts_count_dice)-1)]
        for contact in range(0, num_contacts):
            fname = gen_fname()
            lname = gen_lname()

            email = '%s.%s@%s' % (fname.replace(' ',''), lname.replace(' ', ''), website.lower())
            contact_addr = gen_address()
            contact_phone = gen_phone()
            contact_mobile = gen_phone()
            contacts = {
                'client_portal_login_enabled': False,
                'auth_api_id': 'nay99',
                'firstname': fname,
                'lastname': lname,
                'email': email,
                'primary_street': contact_addr.get('address'),
                'primary_city': contact_addr.get('city'),
                'primary_state': contact_addr.get('state'),
                'primary_zipcode': contact_addr.get('zipcode'),
                'primary_country': 'US',
                'phone': contact_phone,
                'mobile': contact_mobile,
                'password': 'user99123'
            }
            print json.dumps(contacts, indent=4)
            contact_object = Contacts(**contacts)
            contact_object.account = acc_object
            contact_object.save()



        # packages and products
        packages = [
            {
                'groups': {
                    'ITP Fiber Internet Service': [
                        Products.objects.get(product_type='Service', part_number='itp_fiber'),
                        Products.objects.get(product_type="Product", part_number='router_14'),
                    ],
                    'ITP Voice Services': [
                        Products.objects.get(product_type='Service', part_number='itp_voice'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                    ]
                }
            },
            {
                'groups': {
                    'ITP Managed IT Service': [
                        Products.objects.get(product_type="Service", part_number="it_managed_services"),
                        Products.objects.get(product_type="Service", part_number="itp_cloud"),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='router_14'),
                        Products.objects.get(product_type="Product", part_number='switch_17'),
                    ],
                }
            },
            {
                'groups': {
                    'ITP Managed IT Service': [
                        Products.objects.get(product_type="Service", part_number="it_managed_services"),
                        Products.objects.get(product_type="Service", part_number="google_apps"),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_10'),
                        Products.objects.get(product_type="Product", part_number='pc_15'),
                        Products.objects.get(product_type="Product", part_number='pc_10'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                    ],
                }
            },
            {
                'groups': {
                    'ITP Voice Service': [
                        Products.objects.get(product_type="Service", part_number="itp_voice"),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                        Products.objects.get(product_type="Product", part_number='voipphone_16'),
                        Products.objects.get(product_type="Product", part_number='voipphone_16'),
                    ],
                    'IT Managed Service': [
                        Products.objects.get(product_type="Service", part_number="it_managed_services"),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_10'),
                        Products.objects.get(product_type="Product", part_number='pc_15'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                    ],
                }
            },
            {
                'groups': {
                    'ITP - Managed IT Services Mid': [
                        Products.objects.get(product_type="Service", part_number="it_managed_services"),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                        Products.objects.get(product_type="Product", part_number='pc_18'),
                    ]
                }
            },
            {
                'groups': {
                    'ITP Fiber Internet Service': [
                        Products.objects.get(product_type="Service", part_number="itp_fiber"),
                        Products.objects.get(product_type="Product", part_number='router_14'),
                        Products.objects.get(product_type="Product", part_number='switch_17'),
                    ],
                },
            },
            {
                'groups': {
                    'ITP Voice Service': [
                        Products.objects.get(product_type="Service", part_number="itp_voice"),
                        Products.objects.get(product_type="Product", part_number='voipphone_16'),
                        Products.objects.get(product_type="Product", part_number='voipphone_16'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                        Products.objects.get(product_type="Product", part_number='voipphone_13'),
                    ]
                    },
            },
        ]

        for nq in range(0, 1):
            quotes = {
                'quote_status': 'Pending Customer Acceptance',
                'customer_signed': True,
                'customer_signed_ip': '10.%s.%s.%s' % (randint(10,250), randint(10,250),randint(10,250)),
            }
            print json.dumps(quotes, indent=4)
            quote_object = Quotes(**quotes)
            quote_object.account = acc_object
            quote_object.valid_until = datetime.now()+timedelta(days=30)
            quote_object.accepted_date = datetime.now()
            quote_object.save()

            random_service = packages[randint(0, len(packages)-1)]
            for k, v in random_service.get('groups').items():
                ig = QuoteItemGroup(name=k, quote=quote_object)
                ig.save()
                for prod in v:
                    quote_object.add_item(prod, ig)

            # after adding the products, now well approve the quote
            quote_object.quote_status = 'Customer Approved'
            # this will create the order object.
            quote_object.save()
            teh_order = quote_object.get_order


















        # max tabout here



