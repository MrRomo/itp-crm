import json
from decimal import Decimal
from crm.models import (
        SERVICES_META,
        Products,
        ServiceAddress,
        ITPServices_Addresses
)
from random import randint


def run():

    for addr in ServiceAddress.objects.all():
        for prod in Products.objects.filter(product_type='Service'):
            print "Adding product %s to address %s" % (prod, addr)
            try:
                ITPServices_Addresses.objects.get(address=addr, product=prod)
            except ITPServices_Addresses.DoesNotExist:
                # add the record if it doesnt exist
                i = ITPServices_Addresses()
                i.address = addr
                i.product = prod
                i.save()


