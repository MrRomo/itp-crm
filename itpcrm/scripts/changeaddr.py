from crm.models import *

def run():
    for i in ServiceAddress.objects.all():
        full_addr = ''
        if i.hse_num:
            full_addr += str(i.hse_num) + ' '
    
        if i.pre_dir:
            full_addr += str(i.pre_dir) + ' '
        
        if i.st_name:
            full_addr += str(i.st_name) + ' '
    
        if i.st_type:
            full_addr += str(i.st_type) + ' '
    
        if i.suf_dir:
            full_addr += str(i.suf_dir) + ' '
    
        i.full_address = full_addr.strip()
        i.save()


