from crm.models import *

def run():
    for i in Products.objects.all():
        try:
            pk = ProductKeyNames.objects.get(name=i.part_number)
        except ProductKeyNames.DoesNotExist:
            pk = ProductKeyNames(name=i.part_number, default_metadata={'hello': 'World'})
            pk.save()

        if not i.product_key:
            i.product_key = pk
            i.save()


