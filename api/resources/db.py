from sqlalchemy import create_engine
from config import DB



class DBHandler(object):
    db_user = None
    db_name = None 
    db_host = None
    db_pass = None
    config = DB

    def __init__(self, **kwargs):
        if kwargs.get('config'):
            setattr('config',  kwargs.get('config'))
        
        self.db_user = DB.get('USER')
        self.db_name = DB.get('NAME')
        self.db_host = DB.get('HOST')
        self.db_pass = DB.get('PASSWORD')

        self._create_engine(**kwargs)
        if not self.engine:
            raise Exception("Failed to create engine")


    def _create_engine(self, **kwargs):
        # user password host db
        self.engine = create_engine('mysql://%s:%s@%s/%s' % (
            self.db_user, self.db_pass, self.db_host, self.db_name
        ))

    def get_one(self, q):
        r = self.run_query(q)
        if r:
            return r[0]

    def get_multi(self, q):
        r = self.run_query(q)
        return r

    def run_query(self, q):
        con = self.engine.connect()
        result = self.engine.execute(q)
        if result:
            result = self.iterate_result(result)
        con.close()
        return result


    def iterate_result(self, res):
        results = []
        if res:
            for i in res:
                try:
                    keys = i.keys()
                    d = {}
                    for k in keys:
                        d[str(k)] = i[k]
                    results.append(d)
                except Exception, e:
                    raise Exception(e)
                    results.append(i)
        return results
        
        
