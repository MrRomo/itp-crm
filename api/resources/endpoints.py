import jwt
import requests
import json
from flask_restful import Resource
from flask import request
from db import DBHandler
from config import (
    JWT_KEY,
    VOICE_API_KEY,
    VOICE_API_URL)

def do_auth(token, get_full=False, testing=False):
    validated = False
    if not token:
        return None
    token = str(token).replace('Bearer ', '').replace('bearer ', '')
    #print "TOKEN %s" % token
    print token
    try:
        j = jwt.decode(token, JWT_KEY, algorithms=['HS256'], verify=True)
        print j
        #print j
        validated = True
    except Exception, e:
        return None
    if testing:
        if validated:
            j['validated'] = validated
        return j
    if j:
        user_id = j.get('id')
        if get_full:
            return j
        return user_id
    return None


class BaseResource(Resource):
    def auth(self, get_full=False):
        uid = do_auth(request.headers.get('Authorization'), get_full=get_full)
        if not uid:
            return None
        return uid

    def get_account_id(self, uid):
        if not uid:
            return None
        db = DBHandler()
        q = "select account_id from contacts where auth_api_id = '%s'" % uid
        print q
        qr = db.get_one(q)
        if not qr:
            return None
        try:
            acc_id = qr['account_id']
        except Exception, e:
            acc_id = None
            return None
        q2 = "select id from accounts where id = '%s' and status = 'Active'" % acc_id
        print "q2 = %s" % q2
        acc_qr = db.get_one(q2)
        if acc_qr:
            return acc_qr['id']
        return None



class Accounts(BaseResource):
    def __init__(self, *args, **kwargs):
        super(GetAccounts, self).__init__(*args, **kwargs)

    def get(self):
        uid = self.auth()
        if not uid:
            return "Unauthorized", 401
        acc_id = self.get_account_id(uid)
        if not acc_id:
            return {"result": None, "user_id": None, "account_id": None}
        db = DBHandler()
        q = "select * from accounts where id = '%s' and status = 'Active'" % acc_id
        qr = db.get_one(q)
        return {"result": qr, 'user_id': uid, 'account_id': acc_id}

    def post(self):
        # create account




class GetAccount(BaseResource):
    """This is a test class, deprecate soon"""
    def __init__(self, *args, **kwargs):
        super(GetAccount, self).__init__(*args, **kwargs)

    def get(self):
        uid = self.auth()
        if not uid:
            return "Unauthorized", 401
        acc_id = self.get_account_id(uid)
        db = DBHandler()
        q = "select * from accounts where id = '%s' and status = 'Active'" % acc_id
        qr = db.get_one(q)
        return {"result": qr, 'user_id': uid, 'account_id': acc_id}



class GetServices(BaseResource):
    list_of_services = [
        'serv_googleapps',
        'serv_it_managed_services',
        'serv_itpcloud',
        'serv_itpfiber',
        'serv_itpvoice',
        'serv_itpvoice_siptrunking'
    ]

    def __init__(self, *args, **kwargs):
        super(GetServices, self).__init__(*args, **kwargs)

    def get(self):
        uid = self.auth()
        acc_id = self.get_account_id(uid)
        if not acc_id:
            return "Unauthorized", 401
        response = {}
        db = DBHandler()
        q = 'select o.id as order_id, o.account_id, oi.id as orderitem_id, '
        q += 'p.name, p.product_type, '
        q += 'p.part_number, oi.metadata '
        q += 'from crm_orders as o '
        q += 'inner join crm_orderitems as oi on oi.order_id = o.id '
        q += 'inner join crm_products as p on p.id = oi.product_id  '
        q += 'where o.account_id = %s and p.product_type = "Service"' % (acc_id)

        qr = db.get_multi(q)
        d = {}

        return {"result": qr, 'user_id': uid}



class GetContacts(BaseResource):
    def __init__(self, *args, **kwargs):
        (GetContacts, self).__init__(*args, **kwargs)

    def get(self):
        auth_obj = self.auth(get_full=True)
        uid = auth_obj.get('id')
        role = auth_obj.get('role')
        if not 'admin' in str(role):
            return "Unauthorized role", 401
        acc_id = self.get_account_id(uid)
        if not acc_id:
            return "Unauthorized", 401
        db = DBHandler()
        q = "select * from contacts where account_id = %s" % acc_id
        qr = db.get_multi(q)
        return {"result": qr}



class Authorize(Resource):
    # test the authorization
    # url = "https://auth.itpscorp.com/portal/users/58bef19515affc5809f4f0c5"
    def get(self):
        token = request.headers.get('Authorization')
        if not token:
            return "Unauthorized", 401
        uid = do_auth(token, testing=True)
        return uid
        if uid:
            q = "select * from contacts where auth_api_id = '%s'" % uid
            db = DBHandler()
            qr = db.run_query(q)
            if qr:
                qr = qr[0]
            if uid:
                return {"result": 'ok', 'account': qr, 'user_id': uid}
        return j, 200


class Devices(BaseResource):
    def get(self):
        token = request.headers.get('Authorization')
        if not token:
            return "Unauthorized", 401
        uid = do_auth(token, testing=True)
        # get account id from the user id
        #return uid
        acc_id = self.get_account_id(uid)
        if not acc_id:
            return {"result": "error", "errors": True, "message": "Invalid account ID"}, 401
        url = '%s/v1/devices?Authorization=%s&account_id=%s' %\
            (VOICE_API_URL, VOICE_API_KEY, acc_id)
        return json.loads(requests.get(url).text)

    def get_voice_account_id(self, accid):
        # todo - fix this cause it can return two of em...
        q = 'select o.account_id, oi.metadata from crm_orderitems as oi '
        q += 'left join crm_orders as o on o.id = oi.order_id where oi.item_type = "itp_voice" '
        q += 'and o.account_id = %s' % accid
        #print "LONG ASS Q: %s" % q
        db = DBHandler()
        qr = db.run_query(q)
        if qr:
            qr = qr[0]
            j = qr['metadata']
            jmeta = json.loads(j)
            #print dir(q)
            #print "JSON META %s" % str(j)
            return jmeta.get('api_id')

    def _easy_auth(self, req):
        token = req.headers.get('Authorization')
        #print "TOKEN = %s" % token
        if not token:
            return "Unauthorized", 401
        uid = do_auth(token, get_full=False, testing=False)
        #print "UID %s" % uid
        if not uid:
            return "Unauthorized", 401
        acc_id = self.get_account_id(uid)
        api_acc_id = self.get_voice_account_id(acc_id)
        return api_acc_id, 200


    def get(self):
        api_acc_id, code = self._easy_auth(request)
        if code == 401:
            return "Unauthorized", 401
        url = '%s/v1/devices?Authorization=%s&account_id=%s' % \
                (VOICE_API_URL, VOICE_API_KEY,api_acc_id)
        print 'URL %s' % url
        return json.loads(requests.get(url).text)


    def post(self):
        api_acc_id, code = self._easy_auth(request)
        if code == 401:
            return "Unauthorized", 401
        url = '%s/v1/devices?Authorization=%s' % \
            (VOICE_API_URL, VOICE_API_KEY)
        # get post data
        j = json.loads(request.get_data())
        j['account'] = api_acc_id
        print "api_cc_id %s" % api_acc_id
        r = requests.post(url, json=j)
        if r.status_code == 200:
            return {"result": "ok", "errors": None, "message": "Device added successfully"}, 200
        else:
            return {"result": "error", "errors": True, "message": str(r.text)}, r.status_code


    def patch(self, dev_id):
        api_acc_id, code = self._easy_auth(request)
        if code == 401:
            return "Unauthorized", 401
        j = json.loads(request.get_data())
        j['account_id'] = api_acc_id
        j['device_id'] = dev_id
        url = '%s/v1/devices?Authorization=%s' % \
            (VOICE_API_URL,VOICE_API_KEY)
        r = requests.patch(url, json=j)
        if r.status_code == 200:
            return {"result": "ok", "errors": None, "message": "Device Updated successfully"}, 200
        else:
            return {"result": "error", "errors": True, "message": str(r.text)}, r.status_code

    def delete(self, dev_id):
        api_acc_id, code = self._easy_auth(request)
        if code == 401:
            return "Unauthorized", 401
        j = {}
        j['account_id'] = api_acc_id
        j['device_id'] = dev_id
        url = '%s/v1/devices?Authorization=%s' % \
            (VOICE_API_URL, VOICE_API_KEY)
        r = requests.delete(url, json=j)
        if r.status_code == 200:
            return {"result": "ok", "errors": None, "message": "Device Updated successfully"}, 200
        else:
            return {"result": "error", "errors": True, "message": str(r.text)}, r.status_code



class AutoProvisionServices(BaseResource):
    def get(self):
        """
        Get the new auto provision servjces
        that have not been provisioned

        status
        product
        order
        item_group
        item_type
        price_per_unit
        price
        bill_interval
        next_bill
        metadata
        payment_complete
        auto_provision
        prov_status

        1) Gets a list of services and things to do
        2) Provisioner makes decision based on product/service
        3) (ITPVOICE) ->
          - Create the account via itpvoice-api.
          - Get the account object code response and add it to crm.
          - Update the order to prov_status = 1

        """
        auth = request.args.get('Authorization')
        if not auth:
            return {}, 401
        if not auth == VOICE_API_KEY:
            return {}, 401

        q = "select * from crm_orderitems where "
        q += "auto_provision = 1 and prov_status = 0"
        db = DBHandler()
        qr = db.run_query(q)

        fields = [
            'status',
            'product_id',
            'order_id',
            'item_group_id',
            'item_type',
            'price_per_unit',
            'price',
            'bill_interval',
            'next_bill',
            'metadata',
            'payment_complete',
            'auto_provision',
            'prov_status'
        ]
        
        results = []
        if qr:
            qr = qr[0]
            d = {}
            for field in fields:
                d[field] = str(qr[field])
            results.append(d)
            #jmeta = json.loads(j)
        return {"result": results}


class SearchForAddress(BaseResource):
    def post(self):
        data = json.loads(request.get_data())
        q = "select * from crm_serviceaddress where "
        n = len(data.keys())
        c = 1
        for k, v in data.items():
            if c == n:
                q += k + " LIKE '%%" + v + "%%';"
            else:
                q += k + " LIKE '%%" + v + "%%' and "
            c += 1

        fields = [
            'lprop_id',
            'addressid',
            'folio',
            'point_x',
            'point_y',
            'mailing_mu',
            'hse_num',
            'pre_dir',
            'st_name',
            'st_type',
            'suf_dir',
            'zip',
            'plus4',
            'munic',
            'sname',
            'city',
        ]
        db = DBHandler()
        qr = db.run_query(q)
        results = []
        if qr:
            for qresult in qr:
                d = {}
                for field in fields:
                    d[field] = str(qresult[field])
                results.append(d)
        return {"result": results}
        

