import json

fcfg = '/etc/itp-crm/config.json'
mcfg = json.loads(open(fcfg, 'r').read())

cfg = mcfg.get(mcfg.get('mode'))

DB = cfg.get('db')
JWT_KEY = cfg.get('jwt_key')
VOICE_API_KEY = cfg.get('voice_api_key')
VOICE_API_URL = cfg.get('voice_api_url')


