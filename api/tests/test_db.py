# test a raw query for the db

import unittest
import sys
import json
sys.path.append('../')

from resources.db import DBHandler



class TestDBRawQuery(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def test_db_raw_q(self):
        q = "select * from accounts"
        db = DBHandler()
        qr = db.run_query(q)
        print qr
        print json.dumps(qr, indent=4)
        self.assertTrue(qr)
        self.assertTrue(json.dumps(qr))



if __name__ == "__main__":
    unittest.main()