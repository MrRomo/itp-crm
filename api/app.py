from flask import Flask
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from resources.endpoints import (
        Accounts,
        GetAccount,
        Authorize,
        GetServices,
        GetContacts,
        Devices,
        SearchForAddress,
)


# init the app
app = Flask(__name__)
api = Api(app)


# mysql (refactor and remove this to its own config 
app.config.from_pyfile('resources/config.py')
db = SQLAlchemy(app)


# endpoint routes 
api.add_resource(Accounts, '/v1/accounts')
api.add_resource(GetContacts, '/v1/contacts')
api.add_resource(GetAccount, '/v1/account', '/v1/account/')
api.add_resource(GetServices, '/v1/services', '/v1/services/')
api.add_resource(Authorize, '/v1/auth')

api.add_resource(Devices, '/v1/services/itpvoice/devices', '/v1/services/itpvoice/devices/<string:dev_id>')

api.add_resource(SearchForAddress, '/v1/services/search-by-address')

# when calling via cli, run test server
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5420)
