#!/usr/bin/env python
import pika

import sys

credentials = pika.PlainCredentials('admin', 'p@ssw0rd71$')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.3.172', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='itp-api-events',
                                 exchange_type='direct', durable=True)

rkey = 'billing'
message = "fkme?"

channel.basic_publish(exchange='itpapievents',
                              routing_key=rkey,
                            body=message)
print(" [x] Sent %r:%r" % (rkey, message))
connection.close()
